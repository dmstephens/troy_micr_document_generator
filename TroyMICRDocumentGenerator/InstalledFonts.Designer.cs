﻿
namespace TroyMICRDocumentGenerator
{
    partial class InstalledFonts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.adminPasswordTextBox = new System.Windows.Forms.TextBox();
            this.adminPasswordLabel = new System.Windows.Forms.Label();
            this.deleteInstalledFontsButton = new System.Windows.Forms.Button();
            this.printFontsReportButton = new System.Windows.Forms.Button();
            this.updatePrinterButton = new System.Windows.Forms.Button();
            this.startingIdGroupBox = new System.Windows.Forms.GroupBox();
            this.copyToCheckDetailsButton = new System.Windows.Forms.Button();
            this.numStartingId = new System.Windows.Forms.NumericUpDown();
            this.startingIdLabel = new System.Windows.Forms.Label();
            this.lstFiles = new System.Windows.Forms.ListBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.labelFile = new System.Windows.Forms.Label();
            this.startingIdGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStartingId)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // adminPasswordTextBox
            // 
            this.adminPasswordTextBox.Location = new System.Drawing.Point(610, 248);
            this.adminPasswordTextBox.Name = "adminPasswordTextBox";
            this.adminPasswordTextBox.Size = new System.Drawing.Size(128, 20);
            this.adminPasswordTextBox.TabIndex = 19;
            this.adminPasswordTextBox.Visible = false;
            this.adminPasswordTextBox.TextChanged += new System.EventHandler(this.adminPasswordTextBox_TextChanged);
            // 
            // adminPasswordLabel
            // 
            this.adminPasswordLabel.AutoSize = true;
            this.adminPasswordLabel.Location = new System.Drawing.Point(519, 251);
            this.adminPasswordLabel.Name = "adminPasswordLabel";
            this.adminPasswordLabel.Size = new System.Drawing.Size(85, 13);
            this.adminPasswordLabel.TabIndex = 18;
            this.adminPasswordLabel.Text = "Admin Password";
            this.adminPasswordLabel.Visible = false;
            // 
            // deleteInstalledFontsButton
            // 
            this.deleteInstalledFontsButton.Location = new System.Drawing.Point(336, 245);
            this.deleteInstalledFontsButton.Name = "deleteInstalledFontsButton";
            this.deleteInstalledFontsButton.Size = new System.Drawing.Size(133, 23);
            this.deleteInstalledFontsButton.TabIndex = 17;
            this.deleteInstalledFontsButton.Text = "Delete Installed Fonts";
            this.deleteInstalledFontsButton.UseVisualStyleBackColor = true;
            this.deleteInstalledFontsButton.Visible = false;
            this.deleteInstalledFontsButton.Click += new System.EventHandler(this.deleteInstalledFontsButton_Click);
            // 
            // printFontsReportButton
            // 
            this.printFontsReportButton.Location = new System.Drawing.Point(182, 245);
            this.printFontsReportButton.Name = "printFontsReportButton";
            this.printFontsReportButton.Size = new System.Drawing.Size(133, 23);
            this.printFontsReportButton.TabIndex = 16;
            this.printFontsReportButton.Text = "Print Fonts Report";
            this.printFontsReportButton.UseVisualStyleBackColor = true;
            this.printFontsReportButton.Visible = false;
            this.printFontsReportButton.Click += new System.EventHandler(this.printFontsReportButton_Click);
            // 
            // updatePrinterButton
            // 
            this.updatePrinterButton.Location = new System.Drawing.Point(28, 245);
            this.updatePrinterButton.Name = "updatePrinterButton";
            this.updatePrinterButton.Size = new System.Drawing.Size(133, 23);
            this.updatePrinterButton.TabIndex = 15;
            this.updatePrinterButton.Text = "Update Printer";
            this.updatePrinterButton.UseVisualStyleBackColor = true;
            this.updatePrinterButton.Click += new System.EventHandler(this.updatePrinterButton_Click);
            // 
            // startingIdGroupBox
            // 
            this.startingIdGroupBox.Controls.Add(this.copyToCheckDetailsButton);
            this.startingIdGroupBox.Controls.Add(this.numStartingId);
            this.startingIdGroupBox.Controls.Add(this.startingIdLabel);
            this.startingIdGroupBox.Location = new System.Drawing.Point(754, 41);
            this.startingIdGroupBox.Name = "startingIdGroupBox";
            this.startingIdGroupBox.Size = new System.Drawing.Size(218, 164);
            this.startingIdGroupBox.TabIndex = 14;
            this.startingIdGroupBox.TabStop = false;
            // 
            // copyToCheckDetailsButton
            // 
            this.copyToCheckDetailsButton.Location = new System.Drawing.Point(58, 92);
            this.copyToCheckDetailsButton.Name = "copyToCheckDetailsButton";
            this.copyToCheckDetailsButton.Size = new System.Drawing.Size(108, 39);
            this.copyToCheckDetailsButton.TabIndex = 2;
            this.copyToCheckDetailsButton.Text = "Copy To Check Details";
            this.copyToCheckDetailsButton.UseVisualStyleBackColor = true;
            this.copyToCheckDetailsButton.Click += new System.EventHandler(this.copyCheckDetailsButton_Click);
            // 
            // numStartingId
            // 
            this.numStartingId.Location = new System.Drawing.Point(58, 65);
            this.numStartingId.Maximum = new decimal(new int[] {
            19999,
            0,
            0,
            0});
            this.numStartingId.Name = "numStartingId";
            this.numStartingId.Size = new System.Drawing.Size(108, 20);
            this.numStartingId.TabIndex = 1;
            // 
            // startingIdLabel
            // 
            this.startingIdLabel.AutoSize = true;
            this.startingIdLabel.Location = new System.Drawing.Point(60, 49);
            this.startingIdLabel.Name = "startingIdLabel";
            this.startingIdLabel.Size = new System.Drawing.Size(97, 13);
            this.startingIdLabel.TabIndex = 0;
            this.startingIdLabel.Text = "Starting ID Number";
            // 
            // lstFiles
            // 
            this.lstFiles.FormattingEnabled = true;
            this.lstFiles.Location = new System.Drawing.Point(9, 71);
            this.lstFiles.Name = "lstFiles";
            this.lstFiles.Size = new System.Drawing.Size(726, 134);
            this.lstFiles.TabIndex = 13;
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(704, 41);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(31, 23);
            this.browseButton.TabIndex = 12;
            this.browseButton.Text = "...";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(9, 41);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(688, 20);
            this.txtFilePath.TabIndex = 11;
            // 
            // labelFile
            // 
            this.labelFile.AutoSize = true;
            this.labelFile.Location = new System.Drawing.Point(6, 10);
            this.labelFile.Name = "labelFile";
            this.labelFile.Size = new System.Drawing.Size(26, 13);
            this.labelFile.TabIndex = 10;
            this.labelFile.Text = "File:";
            // 
            // InstalledFonts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.adminPasswordTextBox);
            this.Controls.Add(this.adminPasswordLabel);
            this.Controls.Add(this.deleteInstalledFontsButton);
            this.Controls.Add(this.printFontsReportButton);
            this.Controls.Add(this.updatePrinterButton);
            this.Controls.Add(this.startingIdGroupBox);
            this.Controls.Add(this.lstFiles);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.labelFile);
            this.Name = "InstalledFonts";
            this.Size = new System.Drawing.Size(982, 313);
            this.Load += new System.EventHandler(this.InstalledFonts_Load);
            this.startingIdGroupBox.ResumeLayout(false);
            this.startingIdGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStartingId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox adminPasswordTextBox;
        private System.Windows.Forms.Label adminPasswordLabel;
        private System.Windows.Forms.Button deleteInstalledFontsButton;
        private System.Windows.Forms.Button printFontsReportButton;
        private System.Windows.Forms.Button updatePrinterButton;
        private System.Windows.Forms.GroupBox startingIdGroupBox;
        private System.Windows.Forms.Label startingIdLabel;
        private System.Windows.Forms.ListBox lstFiles;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Label labelFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.NumericUpDown numStartingId;
        private System.Windows.Forms.Button copyToCheckDetailsButton;
    }
}
