﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Security.Permissions;

namespace System.ComponentModel
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class ComponentResourceManager : ResourceManager
    {
        private Hashtable _resourceSets;
        private CultureInfo _neutralResourcesCulture;

        public ComponentResourceManager()
        {
        }

        public ComponentResourceManager(Type t)
          : base(t)
        {
        }

        private CultureInfo NeutralResourcesCulture
        {
            get
            {
                if (this._neutralResourcesCulture == null && this.MainAssembly != (Assembly)null)
                    this._neutralResourcesCulture = ResourceManager.GetNeutralResourcesLanguage(this.MainAssembly);
                return this._neutralResourcesCulture;
            }
        }

        public void ApplyResources(object value, string objectName) => this.ApplyResources(value, objectName, (CultureInfo)null);

        public virtual void ApplyResources(object value, string objectName, CultureInfo culture)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            if (objectName == null)
                throw new ArgumentNullException(nameof(objectName));
            if (culture == null)
                culture = CultureInfo.CurrentUICulture;
            SortedList<string, object> sortedList;
            if (this._resourceSets == null)
            {
                this._resourceSets = new Hashtable();
                sortedList = this.FillResources(culture, out ResourceSet _);
                this._resourceSets[(object)culture] = (object)sortedList;
            }
            else
            {
                sortedList = (SortedList<string, object>)this._resourceSets[(object)culture];
                if (sortedList == null || sortedList.Comparer.Equals((object)StringComparer.OrdinalIgnoreCase) != this.IgnoreCase)
                {
                    sortedList = this.FillResources(culture, out ResourceSet _);
                    this._resourceSets[(object)culture] = (object)sortedList;
                }
            }
            BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty;
            if (this.IgnoreCase)
                bindingAttr |= BindingFlags.IgnoreCase;
            bool flag = false;
            if (value is IComponent)
            {
                ISite site = ((IComponent)value).Site;
                if (site != null && site.DesignMode)
                    flag = true;
            }
            foreach (KeyValuePair<string, object> keyValuePair in sortedList)
            {
                string key = keyValuePair.Key;
                if (key != null)
                {
                    if (this.IgnoreCase)
                    {
                        if (string.Compare(key, 0, objectName, 0, objectName.Length, StringComparison.OrdinalIgnoreCase) != 0)
                            continue;
                    }
                    else if (string.CompareOrdinal(key, 0, objectName, 0, objectName.Length) != 0)
                        continue;
                    int length = objectName.Length;
                    if (key.Length > length && key[length] == '.')
                    {
                        string name = key.Substring(length + 1);
                        if (flag)
                        {
                            PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(value).Find(name, this.IgnoreCase);
                            if (propertyDescriptor != null && !propertyDescriptor.IsReadOnly && (keyValuePair.Value == null || propertyDescriptor.PropertyType.IsInstanceOfType(keyValuePair.Value)))
                                propertyDescriptor.SetValue(value, keyValuePair.Value);
                        }
                        else
                        {
                            PropertyInfo property;
                            try
                            {
                                property = value.GetType().GetProperty(name, bindingAttr);
                            }
                            catch (AmbiguousMatchException ex)
                            {
                                Type type = value.GetType();
                                do
                                {
                                    property = type.GetProperty(name, bindingAttr | BindingFlags.DeclaredOnly);
                                    type = type.BaseType;
                                    if (property == (PropertyInfo)null)
                                    {
                                        if (!(type != (Type)null))
                                            break;
                                    }
                                    else
                                        break;
                                }
                                while (type != typeof(object));
                            }
                            if (property != (PropertyInfo)null && property.CanWrite && (keyValuePair.Value == null || property.PropertyType.IsInstanceOfType(keyValuePair.Value)))
                                property.SetValue(value, keyValuePair.Value, (object[])null);
                        }
                    }
                }
            }
        }

        private SortedList<string, object> FillResources(
          CultureInfo culture,
          out ResourceSet resourceSet)
        {
            ResourceSet resourceSet1 = (ResourceSet)null;
            SortedList<string, object> sortedList = culture.Equals((object)CultureInfo.InvariantCulture) || culture.Equals((object)this.NeutralResourcesCulture) ? (!this.IgnoreCase ? new SortedList<string, object>((IComparer<string>)StringComparer.Ordinal) : new SortedList<string, object>((IComparer<string>)StringComparer.OrdinalIgnoreCase)) : this.FillResources(culture.Parent, out resourceSet1);
            resourceSet = this.GetResourceSet(culture, true, true);
            if (resourceSet != null && resourceSet != resourceSet1)
            {
                foreach (DictionaryEntry dictionaryEntry in resourceSet)
                    sortedList[(string)dictionaryEntry.Key] = dictionaryEntry.Value;
            }
            return sortedList;
        }
    }
}
