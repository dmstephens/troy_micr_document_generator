﻿using System;
using System.Runtime.InteropServices;

namespace TroyMICRDocumentGenerator
{
    public class WIN32
    {
        public static int GETTECHNOLOGY = 20;
        public static int QUERYESCSUPPORT = 8;
        public static int POSTSCRIPT_PASSTHROUGH = 4115;
        public static int ENCAPSULATED_POSTSCRIPT = 4116;
        public static int POSTSCRIPT_IDENTIFY = 4117;
        public static int POSTSCRIPT_INJECTION = 4118;
        public static int POSTSCRIPT_DATA = 37;
        public static int POSTSCRIPT_IGNORE = 38;
        public static int PASSTHROUGH = 19;

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, int level, [MarshalAs(UnmanagedType.LPStruct), In] WIN32.DOCINFOA di);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool WritePrinter(
          IntPtr hPrinter,
          IntPtr pBytes,
          int dwCount,
          out int dwWritten);

        [DllImport("gdi32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int ExtEscape(
          IntPtr hdc,
          int nEscape,
          int cbInput,
          byte[] lpvInData,
          int cbOutput,
          IntPtr lpszOutData);

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr CreateDC(
          string lpszDriver,
          string lpszDevice,
          string lpszOutput,
          IntPtr lpInitData);

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DeleteDC(IntPtr hDc);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void SetLastError(int errCode);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int DeviceCapabilities(
          string device,
          string port,
          WIN32.DeviceCapabilitiesFlags capability,
          IntPtr outputBuffer,
          IntPtr deviceMode);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int GetPrinterDriver(
          IntPtr hPrinter,
          string pEnvironment,
          uint Level,
          IntPtr pDriverInfo,
          uint cbBuf,
          out uint pcbNeeded);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern IntPtr FindFirstPrinterChangeNotification(
          IntPtr hPrinter,
          int fwFlags,
          int fwOptions,
          [MarshalAs(UnmanagedType.LPStruct)] WIN32.PRINTER_NOTIFY_OPTIONS pPrinterNotifyOptions);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool FindNextPrinterChangeNotification(
          IntPtr hPrinter,
          out uint pdwChange,
          IntPtr pPrinterNotifyOptions,
          ref IntPtr ppPrinterNotifyInfo);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        internal static extern bool FindClosePrinterChangeNotification(IntPtr hChange);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        internal static extern int FreePrinterNotifyInfo(IntPtr pPrinterNotifyInfo);

        [StructLayout(LayoutKind.Sequential)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }

        public enum DeviceCapabilitiesFlags : short
        {
            DC_FIELDS = 1,
            DC_PAPERS = 2,
            DC_PAPERSIZE = 3,
            DC_MINEXTENT = 4,
            DC_MAXEXTENT = 5,
            DC_BINS = 6,
            DC_DUPLEX = 7,
            DC_SIZE = 8,
            DC_EXTRA = 9,
            DC_VERSION = 10, // 0x000A
            DC_DRIVER = 11, // 0x000B
            DC_BINNAMES = 12, // 0x000C
            DC_ENUMRESOLUTIONS = 13, // 0x000D
            DC_FILEDEPENDENCIES = 14, // 0x000E
            DC_TRUETYPE = 15, // 0x000F
            DC_PAPERNAMES = 16, // 0x0010
            DC_ORIENTATION = 17, // 0x0011
            DC_COPIES = 18, // 0x0012
            DC_BINADJUST = 19, // 0x0013
            DC_EMF_COMPLIANT = 20, // 0x0014
            DC_DATATYPE_PRODUCED = 21, // 0x0015
            DC_COLLATE = 22, // 0x0016
            DC_MANUFACTURER = 23, // 0x0017
            DC_MODEL = 24, // 0x0018
            DC_PERSONALITY = 25, // 0x0019
            DC_PRINTRATE = 26, // 0x001A
            DC_PRINTRATEUNIT = 27, // 0x001B
            DC_PRINTERMEM = 28, // 0x001C
            DC_MEDIAREADY = 29, // 0x001D
            DC_STAPLE = 30, // 0x001E
            DC_PRINTRATEPPM = 31, // 0x001F
            DC_COLORDEVICE = 32, // 0x0020
            DC_NUP = 33, // 0x0021
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public class DriverInfo2
        {
            [MarshalAs(UnmanagedType.I4)]
            public int cVersion;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string pName;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string pEnvironment;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string pDriverPath;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string pDataFile;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string pConfigFile;
        }

        public enum Printer_Change_Notification_Flags
        {
            PRINTER_CHANGE_ADD_JOB = 256, // 0x00000100
            PRINTER_CHANGE_SET_JOB = 512, // 0x00000200
            PRINTER_CHANGE_DELETE_JOB = 1024, // 0x00000400
            PRINTER_CHANGE_WRITE_JOB = 2048, // 0x00000800
            PRINTER_CHANGE_JOB = 65280, // 0x0000FF00
            PRINTER_CHANGE_FORM = 458752, // 0x00070000
            PRINTER_CHANGE_PORT = 7340032, // 0x00700000
            PRINTER_CHANGE_ALL = 2004353023, // 0x7777FFFF
        }

        public enum Job_Notify_Field_Indexes
        {
            JOB_NOTIFY_FIELD_PRINTER_NAME,
            JOB_NOTIFY_FIELD_MACHINE_NAME,
            JOB_NOTIFY_FIELD_PORT_NAME,
            JOB_NOTIFY_FIELD_USER_NAME,
            JOB_NOTIFY_FIELD_NOTIFY_NAME,
            JOB_NOTIFY_FIELD_DATATYPE,
            JOB_NOTIFY_FIELD_PRINT_PROCESSOR,
            JOB_NOTIFY_FIELD_PARAMETERS,
            JOB_NOTIFY_FIELD_DRIVER_NAME,
            JOB_NOTIFY_FIELD_DEVMODE,
            JOB_NOTIFY_FIELD_STATUS,
            JOB_NOTIFY_FIELD_STATUS_STRING,
        }

        [StructLayout(LayoutKind.Sequential)]
        public class PRINTER_NOTIFY_OPTIONS_TYPE
        {
            public short wType;
            public short wReserved0;
            public int dwReserved1;
            public int dwReserved2;
            public int FieldCount;
            public IntPtr pFields;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class PRINTER_NOTIFY_OPTIONS
        {
            public int dwVersion;
            public int dwFlags;
            public int Count;
            public IntPtr lpTypes;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class PRINTER_NOTIFY_INFO_DATA
        {
            public short wType;
            public short wField;
            public int dwReserved;
            public int dwId;
            public int cbBuff;
            public int pBuff;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class PRINTER_NOTIFY_INFO
        {
            public int version;
            public int Flags;
            public int Count;
        }

        [Flags]
        public enum Job_Status
        {
            BLOCKED_DEVICEQUEUE = 512, // 0x00000200
            DELETED = 256, // 0x00000100
            DELETING = 4,
            ERROR = 2,
            OFFLINE = 32, // 0x00000020
            PAPEROUT = 64, // 0x00000040
            PAUSED = 1,
            PRINTED = 128, // 0x00000080
            PRINTING = 16, // 0x00000010
            RESTART = 2048, // 0x00000800
            SPOOLING = 8,
            INTERVENTION = 1024, // 0x00000400
        }
    }
}
