﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace TroyMICRDocumentGenerator
{
    public partial class About : Form
    {
        public About()
        {
            this.InitializeComponent();
            this.lblVersion.Text = "Version " + Assembly.GetExecutingAssembly().GetName().Version;
        }

        private void OKButtonClickedEvent(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(OKButtonClickedEvent), nameof(About));
            }
        } 
    }
}
