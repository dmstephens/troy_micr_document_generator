﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace TroyMICRDocumentGenerator
{
    public partial class InstalledFonts : UserControl
    {
        Random rnd = new Random();

        public InstalledFonts()
        {
            InitializeComponent();
        }

        private void ReadConfigFile()
        {
            try
            {
                XmlSerializer dser = new XmlSerializer(typeof(TroyPrinterUtilityConfiguration));
                FileStream fs = new FileStream(Path.Combine(Application.StartupPath, Globals.MainConfigurationFileName), FileMode.Open);
                Globals.tpuc = (TroyPrinterUtilityConfiguration)dser.Deserialize(fs);
                fs.Close();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ReadConfigFile), nameof(InstalledFonts));
            }
        }

        private void OpenFileDialog()
        {
            try
            {
                openFileDialog1.Multiselect = true;
                openFileDialog1.Filter = "Font Files (*.sfp;*.sfl;*.t15;)|*.sfp;*.sfl;*.t15;|All files (*.*)|*.*";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    lstFiles.Items.Clear();
                    foreach (string filename in openFileDialog1.FileNames)
                    {
                        int index = filename.LastIndexOf('\\');
                        if (index > -1)
                        {
                            string name = filename.Substring(index + 1);
                            lstFiles.Items.Add(name);
                            txtFilePath.Text = filename.Substring(0, index);
                        }
                        else
                        {
                            lstFiles.Items.Add(filename);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(OpenFileDialog), nameof(InstalledFonts));
            }

        }
        private void SendFontDownload()
        {
            try
            {
                int fontidvalue = (int)numStartingId.Value;
                foreach (string str in lstFiles.Items)
                {
                    string fontid = "\u001B*c" + fontidvalue.ToString() + "D";
                    string leadstr = fontid;
                    string endingstr = "";
                    bool sendAdmin = true, sendDown = true, sendJobBegEnd = true;
                    endingstr = fontid + "\u001B*c5F";
                    sendAdmin = false;
                    sendDown = false;
                    sendJobBegEnd = false;

                    Globals.DownloadFileToPrinter(Path.Combine(txtFilePath.Text, str), leadstr, endingstr, sendAdmin, sendDown, sendJobBegEnd);

                    if (++fontidvalue > (int)numStartingId.Maximum) fontidvalue = (int)numStartingId.Minimum;
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SendFontDownload), nameof(InstalledFonts));
            }
        }

        private void SendDataDownload()
        {
            try
            {
                int dataidvalue = (int)numStartingId.Value;
                string filenameprefix = "\u0000\u0000\u0000\u0003\u0000\u0001\u0000\u0000";
                foreach (string str in lstFiles.Items)
                {
                    FileInfo fi = new FileInfo(Path.Combine(txtFilePath.Text, str));
                    string fontid = "\u001B%v" + dataidvalue.ToString() + "I";
                    string leadstr = fontid + "\u001B%v" + fi.Length.ToString() + "W";
                    string endingstr = "";
                    bool sendAdmin = true, sendDown = true, sendJobBegEnd = true;
                    int len = fi.Name.Length;
                    string completeName = len.ToString() + fi.Name;
                    int totallength = filenameprefix.Length + completeName.Length;

                    Globals.DownloadFileToPrinter(Path.Combine(txtFilePath.Text, str), leadstr, endingstr, sendAdmin, sendDown, sendJobBegEnd);

                    if (++dataidvalue > (int)numStartingId.Maximum) dataidvalue = (int)numStartingId.Minimum;

                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SendDataDownload), nameof(InstalledFonts));
            }
        }

        private void updatePrinterButton_Click(object sender, EventArgs e)
        {
            try
            {
                SendFontDownload();
                MessageBox.Show("File was downloaded to the printer.", "Success");
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(updatePrinterButton_Click), nameof(InstalledFonts));
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog();
                updateFontId();
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(updatePrinterButton_Click), nameof(InstalledFonts));
            }
        }

        private void adminPasswordTextBox_TextChanged(object sender, EventArgs e)
        {
            Globals.SelectedPrinter = adminPasswordTextBox.Text;
        }

        private void printFontsReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] download = new byte[] { 0x1B, 0x25, 0x76, 0x32, 0x44 };
                Globals.DownloadBytesToPrinter(download, true, true, true);
                MessageBox.Show("The print resources command has been sent to the printer.");
            }
            catch (Exception ex) 
            {
                Logger.LogMessage(ex, nameof(printFontsReportButton_Click), nameof(InstalledFonts)); 
            } 
        }

        private void InstalledFonts_Load(object sender, EventArgs e)
        {
            try
            {
                ReadConfigFile();
                updateFontId();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(InstalledFonts_Load), nameof(InstalledFonts));
            }
        }

        private void updateFontId()
        {
            try
            {
                numStartingId.Value = rnd.Next(10000, 19999);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(updateFontId), nameof(InstalledFonts));
            }
        }

        private void deleteInstalledFontsButton_Click(object sender, EventArgs e)
        {
            try
            {
                string output = "\u001B%v1D\u001B%v12D\u001B%v0D";
                byte[] outputbytes = new UTF8Encoding().GetBytes(output);
                Globals.DownloadBytesToPrinter(outputbytes, true, true, true);
                MessageBox.Show("The delete resource options has been sent to the selected printer.");
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(deleteInstalledFontsButton_Click), nameof(InstalledFonts));
            }
        }

        private void copyCheckDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                Globals.StartingFontId = (int)numStartingId.Value;
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(copyCheckDetailsButton_Click), nameof(InstalledFonts));
            }
        }
    }
}
