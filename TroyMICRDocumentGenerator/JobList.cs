﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace TroyMICRDocumentGenerator
{
    public partial class JobList : Form
    {
        private IContainer components;
        private ListBox lstJobList;
        private Button btnOk;
        private Button btnCancel;
        private Hashtable _jobList = new Hashtable();
        public string jobFile;       

        public JobList()
        {
            this.InitializeComponent();
            this.PopulateJobList();
        }

        private void PopulateJobList()
        {
            try
            {
                string empty = string.Empty;
                string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                string[] files = Directory.GetFiles((!directoryName.StartsWith("file:\\") ? directoryName : directoryName.Substring(6)) + "\\JOBS");
                for (int index = 0; index < files.Length; ++index)
                {
                    if (files[index].EndsWith(".JOB", StringComparison.CurrentCultureIgnoreCase) || files[index].EndsWith(".XML", StringComparison.CurrentCultureIgnoreCase))
                    {
                        string fileName = Path.GetFileName(files[index]);
                        this.lstJobList.Items.Add((object)fileName);
                        this._jobList.Add((object)fileName, (object)files[index]);
                    }
                }
                if (this.lstJobList.Items.Count > 0)
                {
                    this.lstJobList.SelectedIndex = 0;
                    this.btnOk.Enabled = true;
                }
                else
                    this.btnOk.Enabled = false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PopulateJobList), nameof(JobList));
            }
        }

        private void OKButtonClickedEvent(object sender, EventArgs e)
        {
            try
            {
                if (this.lstJobList.SelectedItem == null || this.lstJobList.SelectedIndex == -1)
                {
                    int num = (int)MessageBox.Show("Please Select a Job", nameof(JobList));
                }
                else
                {
                    this.jobFile = (string)this._jobList[(object)this.lstJobList.GetItemText(this.lstJobList.SelectedItem)];
                    this._jobList.Clear();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "PopulateJobList", nameof(JobList));
            }
        }

        private void CancelButtonClickedEvent(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this._jobList.Clear();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(CancelButtonClickedEvent), nameof(JobList));
            }
        }
    }
}
