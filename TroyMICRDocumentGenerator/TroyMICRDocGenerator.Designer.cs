﻿using System.ComponentModel;
using System.Windows.Forms;

namespace TroyMICRDocumentGenerator
{
    partial class TroyMICRDocGenerator
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabPrinter = new System.Windows.Forms.TabPage();
            this.ignoreSourceCheckBox = new System.Windows.Forms.CheckBox();
            this.txtPrinterSerialNo = new System.Windows.Forms.TextBox();
            this.btnGetSerialNumber = new System.Windows.Forms.Button();
            this.grpPrintOptions = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.lblYHght = new System.Windows.Forms.Label();
            this.lblCheckHdr = new System.Windows.Forms.Label();
            this.txtHeight3 = new System.Windows.Forms.TextBox();
            this.txtHeight1 = new System.Windows.Forms.TextBox();
            this.lblCheck3 = new System.Windows.Forms.Label();
            this.lblCheck1 = new System.Windows.Forms.Label();
            this.lblCheck4 = new System.Windows.Forms.Label();
            this.txtHeight4 = new System.Windows.Forms.TextBox();
            this.lblCheck2 = new System.Windows.Forms.Label();
            this.txtHeight2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rd4UpPrinting = new System.Windows.Forms.RadioButton();
            this.rd3UpPrinting = new System.Windows.Forms.RadioButton();
            this.txtBatchId = new System.Windows.Forms.TextBox();
            this.lblDensity = new System.Windows.Forms.Label();
            this.txtDensity = new System.Windows.Forms.TextBox();
            this.lblPrinterSerialNo = new System.Windows.Forms.Label();
            this.lblBatchId = new System.Windows.Forms.Label();
            this.txtJobName = new System.Windows.Forms.TextBox();
            this.lblJobName = new System.Windows.Forms.Label();
            this.cbPaperSource = new System.Windows.Forms.ComboBox();
            this.lblPaperSource = new System.Windows.Forms.Label();
            this.cbPaperOutput = new System.Windows.Forms.ComboBox();
            this.lblPaperOutput = new System.Windows.Forms.Label();
            this.cbPaperSize = new System.Windows.Forms.ComboBox();
            this.lblPaperSize = new System.Windows.Forms.Label();
            this.cbPrinterName = new System.Windows.Forms.ComboBox();
            this.txtTonerType = new System.Windows.Forms.TextBox();
            this.lblTonerType = new System.Windows.Forms.Label();
            this.txtPrinterModel = new System.Windows.Forms.TextBox();
            this.lblPrinterModel = new System.Windows.Forms.Label();
            this.lblPrinterName = new System.Windows.Forms.Label();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabCheck = new System.Windows.Forms.TabPage();
            this.checkDetailsDelayLabel = new System.Windows.Forms.Label();
            this.checkDetailsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.checkDetailsStopPrinting = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.micrIDCtrl = new TroyMICRDocumentGenerator.MICRIDControl();
            this.txtCheckNo = new System.Windows.Forms.TextBox();
            this.txtNumberOfPages = new System.Windows.Forms.TextBox();
            this.checkDetailsPrintContinuously = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblNumberOfPages = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.numericUpDownLeftMargin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTopMargin = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.printToryFontListButton = new System.Windows.Forms.Button();
            this.cbMICRSourceFontEscapeSequence = new System.Windows.Forms.ComboBox();
            this.rdEscapeSequence = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rdSoftFont = new System.Windows.Forms.RadioButton();
            this.rdInstalledFont = new System.Windows.Forms.RadioButton();
            this.rdResidentFont = new System.Windows.Forms.RadioButton();
            this.txtResidentFontID = new System.Windows.Forms.TextBox();
            this.cbMICRSourceFont = new System.Windows.Forms.ComboBox();
            this.tabTestDocument = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.milisecondsDelayNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.delayLabel = new System.Windows.Forms.Label();
            this.secondDelayNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.numberOfPrintsLabel = new System.Windows.Forms.Label();
            this.delayLable = new System.Windows.Forms.Label();
            this.numberOfPrintsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.printCountTextBox = new System.Windows.Forms.TextBox();
            this.continuePrintingCheckBox = new System.Windows.Forms.CheckBox();
            this.printCountLabel = new System.Windows.Forms.Label();
            this.stopPrintingButton = new System.Windows.Forms.Button();
            this.printOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.testPatternRadioButton = new System.Windows.Forms.RadioButton();
            this.whitePageRadioButton = new System.Windows.Forms.RadioButton();
            this.gradientRadioButton = new System.Windows.Forms.RadioButton();
            this.blackPage = new System.Windows.Forms.RadioButton();
            this.gray50RadioButton = new System.Windows.Forms.RadioButton();
            this.gray35RadioButton = new System.Windows.Forms.RadioButton();
            this.gray25RadioButton = new System.Windows.Forms.RadioButton();
            this.gray15RadioButton = new System.Windows.Forms.RadioButton();
            this.fontsTabPage = new System.Windows.Forms.TabPage();
            this.installedFonts = new TroyMICRDocumentGenerator.InstalledFonts();
            this.toolBtnNew = new System.Windows.Forms.ToolStripButton();
            this.toolBtnOpen = new System.Windows.Forms.ToolStripButton();
            this.toolBtnSave = new System.Windows.Forms.ToolStripButton();
            this.toolBtnPrint = new System.Windows.Forms.ToolStripButton();
            this.toolBtnHelp = new System.Windows.Forms.ToolStripButton();
            this.toolBtnPrintTestPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripMainMenu = new System.Windows.Forms.ToolStrip();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabPrinter.SuspendLayout();
            this.grpPrintOptions.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabCheck.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkDetailsNumericUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLeftMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTopMargin)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabTestDocument.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.milisecondsDelayNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondDelayNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfPrintsNumericUpDown)).BeginInit();
            this.printOptionsGroupBox.SuspendLayout();
            this.fontsTabPage.SuspendLayout();
            this.toolStripMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // tabPrinter
            // 
            this.tabPrinter.BackColor = System.Drawing.Color.Transparent;
            this.tabPrinter.Controls.Add(this.ignoreSourceCheckBox);
            this.tabPrinter.Controls.Add(this.txtPrinterSerialNo);
            this.tabPrinter.Controls.Add(this.btnGetSerialNumber);
            this.tabPrinter.Controls.Add(this.grpPrintOptions);
            this.tabPrinter.Controls.Add(this.txtBatchId);
            this.tabPrinter.Controls.Add(this.lblDensity);
            this.tabPrinter.Controls.Add(this.txtDensity);
            this.tabPrinter.Controls.Add(this.lblPrinterSerialNo);
            this.tabPrinter.Controls.Add(this.lblBatchId);
            this.tabPrinter.Controls.Add(this.txtJobName);
            this.tabPrinter.Controls.Add(this.lblJobName);
            this.tabPrinter.Controls.Add(this.cbPaperSource);
            this.tabPrinter.Controls.Add(this.lblPaperSource);
            this.tabPrinter.Controls.Add(this.cbPaperOutput);
            this.tabPrinter.Controls.Add(this.lblPaperOutput);
            this.tabPrinter.Controls.Add(this.cbPaperSize);
            this.tabPrinter.Controls.Add(this.lblPaperSize);
            this.tabPrinter.Controls.Add(this.cbPrinterName);
            this.tabPrinter.Controls.Add(this.txtTonerType);
            this.tabPrinter.Controls.Add(this.lblTonerType);
            this.tabPrinter.Controls.Add(this.txtPrinterModel);
            this.tabPrinter.Controls.Add(this.lblPrinterModel);
            this.tabPrinter.Controls.Add(this.lblPrinterName);
            this.tabPrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPrinter.Location = new System.Drawing.Point(4, 22);
            this.tabPrinter.Name = "tabPrinter";
            this.tabPrinter.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrinter.Size = new System.Drawing.Size(1003, 314);
            this.tabPrinter.TabIndex = 1;
            this.tabPrinter.Text = "Printer Details";
            this.tabPrinter.UseVisualStyleBackColor = true;
            // 
            // ignoreSourceCheckBox
            // 
            this.ignoreSourceCheckBox.AutoSize = true;
            this.ignoreSourceCheckBox.Location = new System.Drawing.Point(21, 289);
            this.ignoreSourceCheckBox.Name = "ignoreSourceCheckBox";
            this.ignoreSourceCheckBox.Size = new System.Drawing.Size(163, 22);
            this.ignoreSourceCheckBox.TabIndex = 23;
            this.ignoreSourceCheckBox.Text = "Ignore Paper Source";
            this.ignoreSourceCheckBox.UseVisualStyleBackColor = true;
            this.ignoreSourceCheckBox.CheckedChanged += new System.EventHandler(this.ignoreSourceCheckBox_CheckedChanged);
            // 
            // txtPrinterSerialNo
            // 
            this.txtPrinterSerialNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrinterSerialNo.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtPrinterSerialNo.Location = new System.Drawing.Point(306, 90);
            this.txtPrinterSerialNo.MaxLength = 26;
            this.txtPrinterSerialNo.Name = "txtPrinterSerialNo";
            this.txtPrinterSerialNo.Size = new System.Drawing.Size(233, 24);
            this.txtPrinterSerialNo.TabIndex = 7;
            this.txtPrinterSerialNo.TextChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // btnGetSerialNumber
            // 
            this.btnGetSerialNumber.Location = new System.Drawing.Point(541, 90);
            this.btnGetSerialNumber.Name = "btnGetSerialNumber";
            this.btnGetSerialNumber.Size = new System.Drawing.Size(27, 23);
            this.btnGetSerialNumber.TabIndex = 1;
            this.btnGetSerialNumber.Text = "...";
            this.btnGetSerialNumber.UseVisualStyleBackColor = true;
            this.btnGetSerialNumber.Click += new System.EventHandler(this.getSerialNumber);
            // 
            // grpPrintOptions
            // 
            this.grpPrintOptions.Controls.Add(this.tableLayoutPanel1);
            this.grpPrintOptions.Controls.Add(this.rd4UpPrinting);
            this.grpPrintOptions.Controls.Add(this.rd3UpPrinting);
            this.grpPrintOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPrintOptions.ForeColor = System.Drawing.SystemColors.Desktop;
            this.grpPrintOptions.Location = new System.Drawing.Point(602, 26);
            this.grpPrintOptions.Name = "grpPrintOptions";
            this.grpPrintOptions.Size = new System.Drawing.Size(398, 257);
            this.grpPrintOptions.TabIndex = 21;
            this.grpPrintOptions.TabStop = false;
            this.grpPrintOptions.Text = "Print Options";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 862F));
            this.tableLayoutPanel1.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblYHght, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblCheckHdr, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtHeight3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtHeight1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCheck3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblCheck1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCheck4, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtHeight4, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblCheck2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtHeight2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(15, 104);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(371, 93);
            this.tableLayoutPanel1.TabIndex = 41;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(277, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 16);
            this.label9.TabIndex = 43;
            this.label9.Text = "Y Offset (in)";
            // 
            // lblYHght
            // 
            this.lblYHght.AutoSize = true;
            this.lblYHght.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblYHght.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYHght.Location = new System.Drawing.Point(91, 3);
            this.lblYHght.Name = "lblYHght";
            this.lblYHght.Size = new System.Drawing.Size(88, 16);
            this.lblYHght.TabIndex = 42;
            this.lblYHght.Text = "Y Offset (in) ";
            // 
            // lblCheckHdr
            // 
            this.lblCheckHdr.AutoSize = true;
            this.lblCheckHdr.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblCheckHdr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckHdr.Location = new System.Drawing.Point(6, 3);
            this.lblCheckHdr.Name = "lblCheckHdr";
            this.lblCheckHdr.Size = new System.Drawing.Size(75, 16);
            this.lblCheckHdr.TabIndex = 10;
            this.lblCheckHdr.Text = "Check No";
            // 
            // txtHeight3
            // 
            this.txtHeight3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeight3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtHeight3.Location = new System.Drawing.Point(91, 62);
            this.txtHeight3.Name = "txtHeight3";
            this.txtHeight3.Size = new System.Drawing.Size(59, 24);
            this.txtHeight3.TabIndex = 15;
            this.txtHeight3.TextChanged += new System.EventHandler(this.JobChangedEvent);
            this.txtHeight3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericTextBoxKeyDownEvent);
            this.txtHeight3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBoxKeyPressEvent);
            // 
            // txtHeight1
            // 
            this.txtHeight1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeight1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtHeight1.Location = new System.Drawing.Point(91, 30);
            this.txtHeight1.Name = "txtHeight1";
            this.txtHeight1.Size = new System.Drawing.Size(59, 24);
            this.txtHeight1.TabIndex = 13;
            this.txtHeight1.TextChanged += new System.EventHandler(this.JobChangedEvent);
            this.txtHeight1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericTextBoxKeyDownEvent);
            this.txtHeight1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBoxKeyPressEvent);
            // 
            // lblCheck3
            // 
            this.lblCheck3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCheck3.AutoSize = true;
            this.lblCheck3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheck3.Location = new System.Drawing.Point(9, 65);
            this.lblCheck3.Name = "lblCheck3";
            this.lblCheck3.Size = new System.Drawing.Size(70, 18);
            this.lblCheck3.TabIndex = 1;
            this.lblCheck3.Text = "Check 3";
            // 
            // lblCheck1
            // 
            this.lblCheck1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCheck1.AutoSize = true;
            this.lblCheck1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheck1.Location = new System.Drawing.Point(9, 32);
            this.lblCheck1.Name = "lblCheck1";
            this.lblCheck1.Size = new System.Drawing.Size(70, 18);
            this.lblCheck1.TabIndex = 2;
            this.lblCheck1.Text = "Check 1";
            // 
            // lblCheck4
            // 
            this.lblCheck4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCheck4.AutoSize = true;
            this.lblCheck4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheck4.Location = new System.Drawing.Point(193, 65);
            this.lblCheck4.Name = "lblCheck4";
            this.lblCheck4.Size = new System.Drawing.Size(70, 18);
            this.lblCheck4.TabIndex = 4;
            this.lblCheck4.Text = "Check 4";
            // 
            // txtHeight4
            // 
            this.txtHeight4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeight4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtHeight4.Location = new System.Drawing.Point(277, 62);
            this.txtHeight4.Name = "txtHeight4";
            this.txtHeight4.Size = new System.Drawing.Size(59, 24);
            this.txtHeight4.TabIndex = 16;
            this.txtHeight4.TextChanged += new System.EventHandler(this.JobChangedEvent);
            this.txtHeight4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericTextBoxKeyDownEvent);
            this.txtHeight4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBoxKeyPressEvent);
            // 
            // lblCheck2
            // 
            this.lblCheck2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCheck2.AutoSize = true;
            this.lblCheck2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheck2.Location = new System.Drawing.Point(193, 32);
            this.lblCheck2.Name = "lblCheck2";
            this.lblCheck2.Size = new System.Drawing.Size(70, 18);
            this.lblCheck2.TabIndex = 0;
            this.lblCheck2.Text = "Check 2";
            // 
            // txtHeight2
            // 
            this.txtHeight2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeight2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtHeight2.Location = new System.Drawing.Point(277, 30);
            this.txtHeight2.Name = "txtHeight2";
            this.txtHeight2.Size = new System.Drawing.Size(59, 24);
            this.txtHeight2.TabIndex = 14;
            this.txtHeight2.TextChanged += new System.EventHandler(this.JobChangedEvent);
            this.txtHeight2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericTextBoxKeyDownEvent);
            this.txtHeight2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBoxKeyPressEvent);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(188, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "Check No  ";
            // 
            // rd4UpPrinting
            // 
            this.rd4UpPrinting.AutoSize = true;
            this.rd4UpPrinting.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd4UpPrinting.ForeColor = System.Drawing.SystemColors.Desktop;
            this.rd4UpPrinting.Location = new System.Drawing.Point(195, 50);
            this.rd4UpPrinting.Name = "rd4UpPrinting";
            this.rd4UpPrinting.Size = new System.Drawing.Size(123, 22);
            this.rd4UpPrinting.TabIndex = 12;
            this.rd4UpPrinting.TabStop = true;
            this.rd4UpPrinting.Text = "4 Up Printing";
            this.rd4UpPrinting.UseVisualStyleBackColor = true;
            this.rd4UpPrinting.CheckedChanged += new System.EventHandler(this.PrintOptionChangedEvent);
            // 
            // rd3UpPrinting
            // 
            this.rd3UpPrinting.AutoSize = true;
            this.rd3UpPrinting.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd3UpPrinting.ForeColor = System.Drawing.SystemColors.Desktop;
            this.rd3UpPrinting.Location = new System.Drawing.Point(44, 50);
            this.rd3UpPrinting.Name = "rd3UpPrinting";
            this.rd3UpPrinting.Size = new System.Drawing.Size(123, 22);
            this.rd3UpPrinting.TabIndex = 11;
            this.rd3UpPrinting.TabStop = true;
            this.rd3UpPrinting.Text = "3 Up Printing";
            this.rd3UpPrinting.UseVisualStyleBackColor = true;
            this.rd3UpPrinting.CheckedChanged += new System.EventHandler(this.PrintOptionChangedEvent);
            // 
            // txtBatchId
            // 
            this.txtBatchId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatchId.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBatchId.Location = new System.Drawing.Point(306, 37);
            this.txtBatchId.MaxLength = 26;
            this.txtBatchId.Name = "txtBatchId";
            this.txtBatchId.Size = new System.Drawing.Size(263, 24);
            this.txtBatchId.TabIndex = 6;
            this.txtBatchId.Text = " ";
            this.txtBatchId.TextChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // lblDensity
            // 
            this.lblDensity.AutoSize = true;
            this.lblDensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDensity.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblDensity.Location = new System.Drawing.Point(303, 125);
            this.lblDensity.Name = "lblDensity";
            this.lblDensity.Size = new System.Drawing.Size(129, 18);
            this.lblDensity.TabIndex = 7;
            this.lblDensity.Text = "Printer Settings:";
            // 
            // txtDensity
            // 
            this.txtDensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDensity.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtDensity.Location = new System.Drawing.Point(306, 145);
            this.txtDensity.MaxLength = 26;
            this.txtDensity.Name = "txtDensity";
            this.txtDensity.Size = new System.Drawing.Size(263, 24);
            this.txtDensity.TabIndex = 8;
            this.txtDensity.TextChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // lblPrinterSerialNo
            // 
            this.lblPrinterSerialNo.AutoSize = true;
            this.lblPrinterSerialNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinterSerialNo.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblPrinterSerialNo.Location = new System.Drawing.Point(303, 70);
            this.lblPrinterSerialNo.Name = "lblPrinterSerialNo";
            this.lblPrinterSerialNo.Size = new System.Drawing.Size(138, 18);
            this.lblPrinterSerialNo.TabIndex = 22;
            this.lblPrinterSerialNo.Text = "Printer Serial No:";
            // 
            // lblBatchId
            // 
            this.lblBatchId.AutoSize = true;
            this.lblBatchId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatchId.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBatchId.Location = new System.Drawing.Point(303, 17);
            this.lblBatchId.Name = "lblBatchId";
            this.lblBatchId.Size = new System.Drawing.Size(77, 18);
            this.lblBatchId.TabIndex = 19;
            this.lblBatchId.Text = "Batch ID:";
            // 
            // txtJobName
            // 
            this.txtJobName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJobName.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtJobName.Location = new System.Drawing.Point(12, 37);
            this.txtJobName.MaxLength = 26;
            this.txtJobName.Name = "txtJobName";
            this.txtJobName.Size = new System.Drawing.Size(263, 24);
            this.txtJobName.TabIndex = 0;
            this.txtJobName.TextChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // lblJobName
            // 
            this.lblJobName.AutoSize = true;
            this.lblJobName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJobName.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblJobName.Location = new System.Drawing.Point(8, 17);
            this.lblJobName.Name = "lblJobName";
            this.lblJobName.Size = new System.Drawing.Size(90, 18);
            this.lblJobName.TabIndex = 17;
            this.lblJobName.Text = "Job Name:";
            // 
            // cbPaperSource
            // 
            this.cbPaperSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPaperSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPaperSource.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbPaperSource.FormattingEnabled = true;
            this.cbPaperSource.Location = new System.Drawing.Point(13, 257);
            this.cbPaperSource.Name = "cbPaperSource";
            this.cbPaperSource.Size = new System.Drawing.Size(263, 26);
            this.cbPaperSource.TabIndex = 5;
            this.cbPaperSource.SelectedIndexChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // lblPaperSource
            // 
            this.lblPaperSource.AutoSize = true;
            this.lblPaperSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaperSource.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblPaperSource.Location = new System.Drawing.Point(10, 236);
            this.lblPaperSource.Name = "lblPaperSource";
            this.lblPaperSource.Size = new System.Drawing.Size(116, 18);
            this.lblPaperSource.TabIndex = 13;
            this.lblPaperSource.Text = "Paper Source:";
            // 
            // cbPaperOutput
            // 
            this.cbPaperOutput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPaperOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPaperOutput.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbPaperOutput.FormattingEnabled = true;
            this.cbPaperOutput.Location = new System.Drawing.Point(306, 198);
            this.cbPaperOutput.Name = "cbPaperOutput";
            this.cbPaperOutput.Size = new System.Drawing.Size(263, 26);
            this.cbPaperOutput.TabIndex = 10;
            this.cbPaperOutput.SelectedIndexChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // lblPaperOutput
            // 
            this.lblPaperOutput.AutoSize = true;
            this.lblPaperOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaperOutput.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblPaperOutput.Location = new System.Drawing.Point(303, 177);
            this.lblPaperOutput.Name = "lblPaperOutput";
            this.lblPaperOutput.Size = new System.Drawing.Size(112, 18);
            this.lblPaperOutput.TabIndex = 11;
            this.lblPaperOutput.Text = "Paper Output:";
            // 
            // cbPaperSize
            // 
            this.cbPaperSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPaperSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPaperSize.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbPaperSize.FormattingEnabled = true;
            this.cbPaperSize.Location = new System.Drawing.Point(13, 198);
            this.cbPaperSize.Name = "cbPaperSize";
            this.cbPaperSize.Size = new System.Drawing.Size(263, 26);
            this.cbPaperSize.TabIndex = 4;
            this.cbPaperSize.SelectedIndexChanged += new System.EventHandler(this.PrintOptionChangedEvent);
            // 
            // lblPaperSize
            // 
            this.lblPaperSize.AutoSize = true;
            this.lblPaperSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaperSize.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblPaperSize.Location = new System.Drawing.Point(10, 177);
            this.lblPaperSize.Name = "lblPaperSize";
            this.lblPaperSize.Size = new System.Drawing.Size(95, 18);
            this.lblPaperSize.TabIndex = 9;
            this.lblPaperSize.Text = "Paper Size:";
            // 
            // cbPrinterName
            // 
            this.cbPrinterName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPrinterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPrinterName.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbPrinterName.FormattingEnabled = true;
            this.cbPrinterName.Location = new System.Drawing.Point(12, 89);
            this.cbPrinterName.Name = "cbPrinterName";
            this.cbPrinterName.Size = new System.Drawing.Size(263, 26);
            this.cbPrinterName.TabIndex = 2;
            this.cbPrinterName.SelectedIndexChanged += new System.EventHandler(this.PrinterSelectionChangedEvent);
            // 
            // txtTonerType
            // 
            this.txtTonerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTonerType.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtTonerType.Location = new System.Drawing.Point(306, 257);
            this.txtTonerType.MaxLength = 26;
            this.txtTonerType.Name = "txtTonerType";
            this.txtTonerType.Size = new System.Drawing.Size(263, 24);
            this.txtTonerType.TabIndex = 9;
            this.txtTonerType.TextChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // lblTonerType
            // 
            this.lblTonerType.AutoSize = true;
            this.lblTonerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTonerType.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblTonerType.Location = new System.Drawing.Point(303, 236);
            this.lblTonerType.Name = "lblTonerType";
            this.lblTonerType.Size = new System.Drawing.Size(98, 18);
            this.lblTonerType.TabIndex = 4;
            this.lblTonerType.Text = "Toner Type:";
            // 
            // txtPrinterModel
            // 
            this.txtPrinterModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrinterModel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtPrinterModel.Location = new System.Drawing.Point(12, 145);
            this.txtPrinterModel.MaxLength = 45;
            this.txtPrinterModel.Name = "txtPrinterModel";
            this.txtPrinterModel.Size = new System.Drawing.Size(263, 24);
            this.txtPrinterModel.TabIndex = 3;
            this.txtPrinterModel.TextChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // lblPrinterModel
            // 
            this.lblPrinterModel.AutoSize = true;
            this.lblPrinterModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinterModel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblPrinterModel.Location = new System.Drawing.Point(9, 125);
            this.lblPrinterModel.Name = "lblPrinterModel";
            this.lblPrinterModel.Size = new System.Drawing.Size(114, 18);
            this.lblPrinterModel.TabIndex = 2;
            this.lblPrinterModel.Text = "Printer Model:";
            // 
            // lblPrinterName
            // 
            this.lblPrinterName.AutoSize = true;
            this.lblPrinterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinterName.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblPrinterName.Location = new System.Drawing.Point(10, 69);
            this.lblPrinterName.Name = "lblPrinterName";
            this.lblPrinterName.Size = new System.Drawing.Size(112, 18);
            this.lblPrinterName.TabIndex = 0;
            this.lblPrinterName.Text = "Printer Name:";
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPrinter);
            this.tabMain.Controls.Add(this.tabCheck);
            this.tabMain.Controls.Add(this.tabTestDocument);
            this.tabMain.Controls.Add(this.fontsTabPage);
            this.tabMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMain.Location = new System.Drawing.Point(-1, 46);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1011, 340);
            this.tabMain.TabIndex = 2;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabCheck
            // 
            this.tabCheck.BackColor = System.Drawing.Color.Transparent;
            this.tabCheck.Controls.Add(this.checkDetailsDelayLabel);
            this.tabCheck.Controls.Add(this.checkDetailsNumericUpDown);
            this.tabCheck.Controls.Add(this.checkDetailsStopPrinting);
            this.tabCheck.Controls.Add(this.groupBox2);
            this.tabCheck.Controls.Add(this.txtCheckNo);
            this.tabCheck.Controls.Add(this.txtNumberOfPages);
            this.tabCheck.Controls.Add(this.checkDetailsPrintContinuously);
            this.tabCheck.Controls.Add(this.label6);
            this.tabCheck.Controls.Add(this.lblNumberOfPages);
            this.tabCheck.Controls.Add(this.label8);
            this.tabCheck.Controls.Add(this.groupBox5);
            this.tabCheck.Controls.Add(this.groupBox1);
            this.tabCheck.Location = new System.Drawing.Point(4, 22);
            this.tabCheck.Name = "tabCheck";
            this.tabCheck.Size = new System.Drawing.Size(1003, 314);
            this.tabCheck.TabIndex = 3;
            this.tabCheck.Text = "Check Details";
            this.tabCheck.UseVisualStyleBackColor = true;
            // 
            // checkDetailsDelayLabel
            // 
            this.checkDetailsDelayLabel.AutoSize = true;
            this.checkDetailsDelayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkDetailsDelayLabel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.checkDetailsDelayLabel.Location = new System.Drawing.Point(751, 95);
            this.checkDetailsDelayLabel.Name = "checkDetailsDelayLabel";
            this.checkDetailsDelayLabel.Size = new System.Drawing.Size(76, 18);
            this.checkDetailsDelayLabel.TabIndex = 47;
            this.checkDetailsDelayLabel.Text = "Delay(s):";
            this.checkDetailsDelayLabel.Visible = false;
            // 
            // checkDetailsNumericUpDown
            // 
            this.checkDetailsNumericUpDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.checkDetailsNumericUpDown.Location = new System.Drawing.Point(828, 93);
            this.checkDetailsNumericUpDown.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.checkDetailsNumericUpDown.Name = "checkDetailsNumericUpDown";
            this.checkDetailsNumericUpDown.Size = new System.Drawing.Size(72, 23);
            this.checkDetailsNumericUpDown.TabIndex = 46;
            this.checkDetailsNumericUpDown.UseWaitCursor = true;
            this.checkDetailsNumericUpDown.Visible = false;
            // 
            // checkDetailsStopPrinting
            // 
            this.checkDetailsStopPrinting.Enabled = false;
            this.checkDetailsStopPrinting.Location = new System.Drawing.Point(907, 93);
            this.checkDetailsStopPrinting.Name = "checkDetailsStopPrinting";
            this.checkDetailsStopPrinting.Size = new System.Drawing.Size(87, 23);
            this.checkDetailsStopPrinting.TabIndex = 42;
            this.checkDetailsStopPrinting.Text = "Stop";
            this.checkDetailsStopPrinting.UseVisualStyleBackColor = true;
            this.checkDetailsStopPrinting.Visible = false;
            this.checkDetailsStopPrinting.Click += new System.EventHandler(this.checkDetailsStopPrintingButtonClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.micrIDCtrl);
            this.groupBox2.Location = new System.Drawing.Point(-2, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1005, 143);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // micrIDCtrl
            // 
            this.micrIDCtrl.AmountField = "           A";
            this.micrIDCtrl.AuxOnUSField = "               ";
            this.micrIDCtrl.Location = new System.Drawing.Point(2, 17);
            this.micrIDCtrl.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.micrIDCtrl.Name = "micrIDCtrl";
            this.micrIDCtrl.OnUSField = "                    ";
            this.micrIDCtrl.Size = new System.Drawing.Size(1005, 123);
            this.micrIDCtrl.StartAutoIncrAmountAt = -1;
            this.micrIDCtrl.StartAutoIncrAuxOnUSAt = -1;
            this.micrIDCtrl.StartAutoIncrOnUSAt = -1;
            this.micrIDCtrl.StartAutoIncrTransitAt = -1;
            this.micrIDCtrl.TabIndex = 0;
            this.micrIDCtrl.TransitField = "           ";
            // 
            // txtCheckNo
            // 
            this.txtCheckNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckNo.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtCheckNo.Location = new System.Drawing.Point(737, 106);
            this.txtCheckNo.Name = "txtCheckNo";
            this.txtCheckNo.Size = new System.Drawing.Size(78, 24);
            this.txtCheckNo.TabIndex = 3;
            this.txtCheckNo.TextChanged += new System.EventHandler(this.checkNumberTextBoxLeave);
            this.txtCheckNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericTextBoxKeyDownEvent);
            this.txtCheckNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBoxKeyPressEvent);
            // 
            // txtNumberOfPages
            // 
            this.txtNumberOfPages.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumberOfPages.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtNumberOfPages.Location = new System.Drawing.Point(916, 106);
            this.txtNumberOfPages.Name = "txtNumberOfPages";
            this.txtNumberOfPages.Size = new System.Drawing.Size(78, 24);
            this.txtNumberOfPages.TabIndex = 3;
            this.txtNumberOfPages.TextChanged += new System.EventHandler(this.numberOfPagesLeave);
            this.txtNumberOfPages.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericTextBoxKeyDownEvent);
            this.txtNumberOfPages.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBoxKeyPressEvent);
            // 
            // checkDetailsPrintContinuously
            // 
            this.checkDetailsPrintContinuously.AutoSize = true;
            this.checkDetailsPrintContinuously.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkDetailsPrintContinuously.Location = new System.Drawing.Point(624, 97);
            this.checkDetailsPrintContinuously.Name = "checkDetailsPrintContinuously";
            this.checkDetailsPrintContinuously.Size = new System.Drawing.Size(128, 17);
            this.checkDetailsPrintContinuously.TabIndex = 4;
            this.checkDetailsPrintContinuously.Text = "Print Continousoly";
            this.checkDetailsPrintContinuously.UseVisualStyleBackColor = true;
            this.checkDetailsPrintContinuously.Visible = false;
            this.checkDetailsPrintContinuously.CheckedChanged += new System.EventHandler(this.checkDetailsPrintContinuously_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label6.Location = new System.Drawing.Point(621, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 18);
            this.label6.TabIndex = 3;
            this.label6.Text = "No of Checks:";
            // 
            // lblNumberOfPages
            // 
            this.lblNumberOfPages.AutoSize = true;
            this.lblNumberOfPages.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberOfPages.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblNumberOfPages.Location = new System.Drawing.Point(827, 109);
            this.lblNumberOfPages.Name = "lblNumberOfPages";
            this.lblNumberOfPages.Size = new System.Drawing.Size(80, 18);
            this.lblNumberOfPages.TabIndex = 3;
            this.lblNumberOfPages.Text = "No of Pg:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(623, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 18);
            this.label8.TabIndex = 3;
            this.label8.Text = "No of Checks:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.numericUpDownLeftMargin);
            this.groupBox5.Controls.Add(this.numericUpDownTopMargin);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox5.Location = new System.Drawing.Point(623, 14);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(375, 65);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "MICR Offset (0.1mm)";
            // 
            // numericUpDownLeftMargin
            // 
            this.numericUpDownLeftMargin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownLeftMargin.ForeColor = System.Drawing.SystemColors.Desktop;
            this.numericUpDownLeftMargin.Location = new System.Drawing.Point(284, 34);
            this.numericUpDownLeftMargin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLeftMargin.Name = "numericUpDownLeftMargin";
            this.numericUpDownLeftMargin.Size = new System.Drawing.Size(48, 24);
            this.numericUpDownLeftMargin.TabIndex = 6;
            this.numericUpDownLeftMargin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLeftMargin.ValueChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // numericUpDownTopMargin
            // 
            this.numericUpDownTopMargin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownTopMargin.ForeColor = System.Drawing.SystemColors.Desktop;
            this.numericUpDownTopMargin.Location = new System.Drawing.Point(115, 34);
            this.numericUpDownTopMargin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTopMargin.Name = "numericUpDownTopMargin";
            this.numericUpDownTopMargin.Size = new System.Drawing.Size(48, 24);
            this.numericUpDownTopMargin.TabIndex = 5;
            this.numericUpDownTopMargin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTopMargin.ValueChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(179, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 18);
            this.label4.TabIndex = 17;
            this.label4.Text = "Left Margin:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label5.Location = new System.Drawing.Point(9, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "Top Margin:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.printToryFontListButton);
            this.groupBox1.Controls.Add(this.cbMICRSourceFontEscapeSequence);
            this.groupBox1.Controls.Add(this.rdEscapeSequence);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.rdSoftFont);
            this.groupBox1.Controls.Add(this.rdInstalledFont);
            this.groupBox1.Controls.Add(this.rdResidentFont);
            this.groupBox1.Controls.Add(this.txtResidentFontID);
            this.groupBox1.Controls.Add(this.cbMICRSourceFont);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox1.Location = new System.Drawing.Point(-1, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(605, 178);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MICR Source Font";
            // 
            // printToryFontListButton
            // 
            this.printToryFontListButton.Location = new System.Drawing.Point(234, 139);
            this.printToryFontListButton.Name = "printToryFontListButton";
            this.printToryFontListButton.Size = new System.Drawing.Size(160, 23);
            this.printToryFontListButton.TabIndex = 14;
            this.printToryFontListButton.Text = "Print Troy Font List";
            this.printToryFontListButton.UseVisualStyleBackColor = true;
            this.printToryFontListButton.Click += new System.EventHandler(this.printToryFontListButton_Click);
            // 
            // cbMICRSourceFontEscapeSequence
            // 
            this.cbMICRSourceFontEscapeSequence.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMICRSourceFontEscapeSequence.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbMICRSourceFontEscapeSequence.FormattingEnabled = true;
            this.cbMICRSourceFontEscapeSequence.Items.AddRange(new object[] {
            "<esc>(0Q<esc>(s1p12.0v0s0b0T",
            "<esc>(0Q<esc>(s1p12.0v0s0b1T"});
            this.cbMICRSourceFontEscapeSequence.Location = new System.Drawing.Point(234, 100);
            this.cbMICRSourceFontEscapeSequence.Name = "cbMICRSourceFontEscapeSequence";
            this.cbMICRSourceFontEscapeSequence.Size = new System.Drawing.Size(350, 26);
            this.cbMICRSourceFontEscapeSequence.TabIndex = 13;
            // 
            // rdEscapeSequence
            // 
            this.rdEscapeSequence.AutoSize = true;
            this.rdEscapeSequence.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdEscapeSequence.ForeColor = System.Drawing.SystemColors.Desktop;
            this.rdEscapeSequence.Location = new System.Drawing.Point(23, 101);
            this.rdEscapeSequence.Name = "rdEscapeSequence";
            this.rdEscapeSequence.Size = new System.Drawing.Size(161, 22);
            this.rdEscapeSequence.TabIndex = 12;
            this.rdEscapeSequence.TabStop = true;
            this.rdEscapeSequence.Text = "Escape Sequence";
            this.rdEscapeSequence.UseVisualStyleBackColor = true;
            this.rdEscapeSequence.CheckedChanged += new System.EventHandler(this.FontOptionSelectionChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(520, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "(Name)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(520, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 18);
            this.label3.TabIndex = 10;
            this.label3.Text = "(Font ID)";
            // 
            // rdSoftFont
            // 
            this.rdSoftFont.AutoSize = true;
            this.rdSoftFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSoftFont.ForeColor = System.Drawing.SystemColors.Desktop;
            this.rdSoftFont.Location = new System.Drawing.Point(23, 64);
            this.rdSoftFont.Name = "rdSoftFont";
            this.rdSoftFont.Size = new System.Drawing.Size(96, 22);
            this.rdSoftFont.TabIndex = 1;
            this.rdSoftFont.TabStop = true;
            this.rdSoftFont.Text = "Soft Font";
            this.rdSoftFont.UseVisualStyleBackColor = true;
            this.rdSoftFont.CheckedChanged += new System.EventHandler(this.FontOptionSelectionChanged);
            // 
            // rdInstalledFont
            // 
            this.rdInstalledFont.AutoSize = true;
            this.rdInstalledFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdInstalledFont.ForeColor = System.Drawing.SystemColors.Desktop;
            this.rdInstalledFont.Location = new System.Drawing.Point(23, 139);
            this.rdInstalledFont.Name = "rdInstalledFont";
            this.rdInstalledFont.Size = new System.Drawing.Size(188, 22);
            this.rdInstalledFont.TabIndex = 2;
            this.rdInstalledFont.TabStop = true;
            this.rdInstalledFont.Text = "System Installed Font";
            this.rdInstalledFont.UseVisualStyleBackColor = true;
            this.rdInstalledFont.CheckedChanged += new System.EventHandler(this.FontOptionSelectionChanged);
            // 
            // rdResidentFont
            // 
            this.rdResidentFont.AutoSize = true;
            this.rdResidentFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdResidentFont.ForeColor = System.Drawing.SystemColors.Desktop;
            this.rdResidentFont.Location = new System.Drawing.Point(23, 27);
            this.rdResidentFont.Name = "rdResidentFont";
            this.rdResidentFont.Size = new System.Drawing.Size(131, 22);
            this.rdResidentFont.TabIndex = 0;
            this.rdResidentFont.TabStop = true;
            this.rdResidentFont.Text = "Resident Font";
            this.rdResidentFont.UseVisualStyleBackColor = true;
            this.rdResidentFont.CheckedChanged += new System.EventHandler(this.FontOptionSelectionChanged);
            // 
            // txtResidentFontID
            // 
            this.txtResidentFontID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResidentFontID.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtResidentFontID.Location = new System.Drawing.Point(234, 24);
            this.txtResidentFontID.Name = "txtResidentFontID";
            this.txtResidentFontID.Size = new System.Drawing.Size(275, 24);
            this.txtResidentFontID.TabIndex = 3;
            this.txtResidentFontID.TextChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // cbMICRSourceFont
            // 
            this.cbMICRSourceFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMICRSourceFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMICRSourceFont.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbMICRSourceFont.FormattingEnabled = true;
            this.cbMICRSourceFont.Location = new System.Drawing.Point(234, 61);
            this.cbMICRSourceFont.Name = "cbMICRSourceFont";
            this.cbMICRSourceFont.Size = new System.Drawing.Size(275, 26);
            this.cbMICRSourceFont.TabIndex = 4;
            this.cbMICRSourceFont.SelectedIndexChanged += new System.EventHandler(this.JobChangedEvent);
            // 
            // tabTestDocument
            // 
            this.tabTestDocument.BackColor = System.Drawing.Color.Transparent;
            this.tabTestDocument.Controls.Add(this.groupBox3);
            this.tabTestDocument.Controls.Add(this.printOptionsGroupBox);
            this.tabTestDocument.Location = new System.Drawing.Point(4, 22);
            this.tabTestDocument.Name = "tabTestDocument";
            this.tabTestDocument.Size = new System.Drawing.Size(1003, 314);
            this.tabTestDocument.TabIndex = 4;
            this.tabTestDocument.Text = "Print Test Patterns";
            this.tabTestDocument.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.milisecondsDelayNumericUpDown);
            this.groupBox3.Controls.Add(this.delayLabel);
            this.groupBox3.Controls.Add(this.secondDelayNumericUpDown);
            this.groupBox3.Controls.Add(this.numberOfPrintsLabel);
            this.groupBox3.Controls.Add(this.delayLable);
            this.groupBox3.Controls.Add(this.numberOfPrintsNumericUpDown);
            this.groupBox3.Controls.Add(this.printCountTextBox);
            this.groupBox3.Controls.Add(this.continuePrintingCheckBox);
            this.groupBox3.Controls.Add(this.printCountLabel);
            this.groupBox3.Controls.Add(this.stopPrintingButton);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox3.Location = new System.Drawing.Point(457, 34);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(270, 222);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            // 
            // milisecondsDelayNumericUpDown
            // 
            this.milisecondsDelayNumericUpDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.milisecondsDelayNumericUpDown.Location = new System.Drawing.Point(116, 24);
            this.milisecondsDelayNumericUpDown.Maximum = new decimal(new int[] {
            600000,
            0,
            0,
            0});
            this.milisecondsDelayNumericUpDown.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.milisecondsDelayNumericUpDown.Name = "milisecondsDelayNumericUpDown";
            this.milisecondsDelayNumericUpDown.Size = new System.Drawing.Size(120, 23);
            this.milisecondsDelayNumericUpDown.TabIndex = 37;
            this.milisecondsDelayNumericUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.milisecondsDelayNumericUpDown.ValueChanged += new System.EventHandler(this.milisecondUpDown_ValueChanged);
            // 
            // delayLabel
            // 
            this.delayLabel.AutoSize = true;
            this.delayLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.delayLabel.Location = new System.Drawing.Point(7, 28);
            this.delayLabel.Name = "delayLabel";
            this.delayLabel.Size = new System.Drawing.Size(68, 15);
            this.delayLabel.TabIndex = 36;
            this.delayLabel.Text = "Delay (ms):";
            // 
            // secondDelayNumericUpDown
            // 
            this.secondDelayNumericUpDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.secondDelayNumericUpDown.Location = new System.Drawing.Point(116, 59);
            this.secondDelayNumericUpDown.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.secondDelayNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.secondDelayNumericUpDown.Name = "secondDelayNumericUpDown";
            this.secondDelayNumericUpDown.Size = new System.Drawing.Size(120, 23);
            this.secondDelayNumericUpDown.TabIndex = 45;
            this.secondDelayNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.secondDelayNumericUpDown.ValueChanged += new System.EventHandler(this.secondsUpDown_ValueChanged);
            // 
            // numberOfPrintsLabel
            // 
            this.numberOfPrintsLabel.AutoSize = true;
            this.numberOfPrintsLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.numberOfPrintsLabel.Location = new System.Drawing.Point(8, 98);
            this.numberOfPrintsLabel.Name = "numberOfPrintsLabel";
            this.numberOfPrintsLabel.Size = new System.Drawing.Size(106, 15);
            this.numberOfPrintsLabel.TabIndex = 38;
            this.numberOfPrintsLabel.Text = "Number of Prints:";
            // 
            // delayLable
            // 
            this.delayLable.AutoSize = true;
            this.delayLable.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.delayLable.Location = new System.Drawing.Point(7, 63);
            this.delayLable.Name = "delayLable";
            this.delayLable.Size = new System.Drawing.Size(70, 15);
            this.delayLable.TabIndex = 44;
            this.delayLable.Text = "Delay (sec):";
            // 
            // numberOfPrintsNumericUpDown
            // 
            this.numberOfPrintsNumericUpDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.numberOfPrintsNumericUpDown.Location = new System.Drawing.Point(116, 94);
            this.numberOfPrintsNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numberOfPrintsNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberOfPrintsNumericUpDown.Name = "numberOfPrintsNumericUpDown";
            this.numberOfPrintsNumericUpDown.Size = new System.Drawing.Size(120, 23);
            this.numberOfPrintsNumericUpDown.TabIndex = 39;
            this.numberOfPrintsNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // printCountTextBox
            // 
            this.printCountTextBox.Enabled = false;
            this.printCountTextBox.Location = new System.Drawing.Point(116, 129);
            this.printCountTextBox.Name = "printCountTextBox";
            this.printCountTextBox.Size = new System.Drawing.Size(120, 22);
            this.printCountTextBox.TabIndex = 43;
            // 
            // continuePrintingCheckBox
            // 
            this.continuePrintingCheckBox.AutoSize = true;
            this.continuePrintingCheckBox.Location = new System.Drawing.Point(13, 174);
            this.continuePrintingCheckBox.Name = "continuePrintingCheckBox";
            this.continuePrintingCheckBox.Size = new System.Drawing.Size(143, 20);
            this.continuePrintingCheckBox.TabIndex = 40;
            this.continuePrintingCheckBox.Text = "Continue Printing";
            this.continuePrintingCheckBox.UseVisualStyleBackColor = true;
            this.continuePrintingCheckBox.CheckedChanged += new System.EventHandler(this.chkContinue_CheckedChanged);
            // 
            // printCountLabel
            // 
            this.printCountLabel.AutoSize = true;
            this.printCountLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.printCountLabel.Location = new System.Drawing.Point(8, 133);
            this.printCountLabel.Name = "printCountLabel";
            this.printCountLabel.Size = new System.Drawing.Size(73, 15);
            this.printCountLabel.TabIndex = 42;
            this.printCountLabel.Text = "Print Count:";
            // 
            // stopPrintingButton
            // 
            this.stopPrintingButton.Enabled = false;
            this.stopPrintingButton.Location = new System.Drawing.Point(162, 171);
            this.stopPrintingButton.Name = "stopPrintingButton";
            this.stopPrintingButton.Size = new System.Drawing.Size(87, 23);
            this.stopPrintingButton.TabIndex = 41;
            this.stopPrintingButton.Text = "Stop Printing";
            this.stopPrintingButton.UseVisualStyleBackColor = true;
            this.stopPrintingButton.Click += new System.EventHandler(this.btnStopPrinting_Click);
            // 
            // printOptionsGroupBox
            // 
            this.printOptionsGroupBox.Controls.Add(this.testPatternRadioButton);
            this.printOptionsGroupBox.Controls.Add(this.whitePageRadioButton);
            this.printOptionsGroupBox.Controls.Add(this.gradientRadioButton);
            this.printOptionsGroupBox.Controls.Add(this.blackPage);
            this.printOptionsGroupBox.Controls.Add(this.gray50RadioButton);
            this.printOptionsGroupBox.Controls.Add(this.gray35RadioButton);
            this.printOptionsGroupBox.Controls.Add(this.gray25RadioButton);
            this.printOptionsGroupBox.Controls.Add(this.gray15RadioButton);
            this.printOptionsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printOptionsGroupBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.printOptionsGroupBox.Location = new System.Drawing.Point(276, 34);
            this.printOptionsGroupBox.Name = "printOptionsGroupBox";
            this.printOptionsGroupBox.Size = new System.Drawing.Size(143, 266);
            this.printOptionsGroupBox.TabIndex = 46;
            this.printOptionsGroupBox.TabStop = false;
            this.printOptionsGroupBox.Text = "Print Options";
            // 
            // testPatternRadioButton
            // 
            this.testPatternRadioButton.AutoSize = true;
            this.testPatternRadioButton.Checked = true;
            this.testPatternRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testPatternRadioButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.testPatternRadioButton.Location = new System.Drawing.Point(6, 24);
            this.testPatternRadioButton.Name = "testPatternRadioButton";
            this.testPatternRadioButton.Size = new System.Drawing.Size(118, 22);
            this.testPatternRadioButton.TabIndex = 19;
            this.testPatternRadioButton.TabStop = true;
            this.testPatternRadioButton.Text = "Test Pattern";
            this.testPatternRadioButton.UseVisualStyleBackColor = true;
            // 
            // whitePageRadioButton
            // 
            this.whitePageRadioButton.AutoSize = true;
            this.whitePageRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whitePageRadioButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.whitePageRadioButton.Location = new System.Drawing.Point(4, 218);
            this.whitePageRadioButton.Name = "whitePageRadioButton";
            this.whitePageRadioButton.Size = new System.Drawing.Size(112, 22);
            this.whitePageRadioButton.TabIndex = 17;
            this.whitePageRadioButton.Text = "White Page";
            this.whitePageRadioButton.UseVisualStyleBackColor = true;
            // 
            // gradientRadioButton
            // 
            this.gradientRadioButton.AutoSize = true;
            this.gradientRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gradientRadioButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.gradientRadioButton.Location = new System.Drawing.Point(6, 190);
            this.gradientRadioButton.Name = "gradientRadioButton";
            this.gradientRadioButton.Size = new System.Drawing.Size(133, 22);
            this.gradientRadioButton.TabIndex = 16;
            this.gradientRadioButton.Text = "Gradient Page";
            this.gradientRadioButton.UseVisualStyleBackColor = true;
            // 
            // blackPage
            // 
            this.blackPage.AutoSize = true;
            this.blackPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blackPage.ForeColor = System.Drawing.SystemColors.Desktop;
            this.blackPage.Location = new System.Drawing.Point(6, 162);
            this.blackPage.Name = "blackPage";
            this.blackPage.Size = new System.Drawing.Size(111, 22);
            this.blackPage.TabIndex = 15;
            this.blackPage.Text = "Black Page";
            this.blackPage.UseVisualStyleBackColor = true;
            // 
            // gray50RadioButton
            // 
            this.gray50RadioButton.AutoSize = true;
            this.gray50RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gray50RadioButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.gray50RadioButton.Location = new System.Drawing.Point(6, 134);
            this.gray50RadioButton.Name = "gray50RadioButton";
            this.gray50RadioButton.Size = new System.Drawing.Size(99, 22);
            this.gray50RadioButton.TabIndex = 14;
            this.gray50RadioButton.Text = "50% Gray";
            this.gray50RadioButton.UseVisualStyleBackColor = true;
            // 
            // gray35RadioButton
            // 
            this.gray35RadioButton.AutoSize = true;
            this.gray35RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gray35RadioButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.gray35RadioButton.Location = new System.Drawing.Point(6, 106);
            this.gray35RadioButton.Name = "gray35RadioButton";
            this.gray35RadioButton.Size = new System.Drawing.Size(99, 22);
            this.gray35RadioButton.TabIndex = 13;
            this.gray35RadioButton.Text = "35% Gray";
            this.gray35RadioButton.UseVisualStyleBackColor = true;
            // 
            // gray25RadioButton
            // 
            this.gray25RadioButton.AutoSize = true;
            this.gray25RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gray25RadioButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.gray25RadioButton.Location = new System.Drawing.Point(6, 78);
            this.gray25RadioButton.Name = "gray25RadioButton";
            this.gray25RadioButton.Size = new System.Drawing.Size(99, 22);
            this.gray25RadioButton.TabIndex = 12;
            this.gray25RadioButton.Text = "25% Gray";
            this.gray25RadioButton.UseVisualStyleBackColor = true;
            // 
            // gray15RadioButton
            // 
            this.gray15RadioButton.AutoSize = true;
            this.gray15RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gray15RadioButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.gray15RadioButton.Location = new System.Drawing.Point(6, 50);
            this.gray15RadioButton.Name = "gray15RadioButton";
            this.gray15RadioButton.Size = new System.Drawing.Size(99, 22);
            this.gray15RadioButton.TabIndex = 11;
            this.gray15RadioButton.Text = "15% Gray";
            this.gray15RadioButton.UseVisualStyleBackColor = true;
            // 
            // fontsTabPage
            // 
            this.fontsTabPage.Controls.Add(this.installedFonts);
            this.fontsTabPage.Location = new System.Drawing.Point(4, 22);
            this.fontsTabPage.Name = "fontsTabPage";
            this.fontsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.fontsTabPage.Size = new System.Drawing.Size(1003, 314);
            this.fontsTabPage.TabIndex = 5;
            this.fontsTabPage.Text = "Fonts";
            this.fontsTabPage.UseVisualStyleBackColor = true;
            // 
            // installedFonts
            // 
            this.installedFonts.Location = new System.Drawing.Point(0, 1);
            this.installedFonts.Name = "installedFonts";
            this.installedFonts.Size = new System.Drawing.Size(982, 313);
            this.installedFonts.TabIndex = 0;
            // 
            // toolBtnNew
            // 
            this.toolBtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnNew.Image = global::TroyMICRDocumentGenerator.Properties.Resources.star2;
            this.toolBtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnNew.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.toolBtnNew.Name = "toolBtnNew";
            this.toolBtnNew.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolBtnNew.Size = new System.Drawing.Size(55, 36);
            this.toolBtnNew.Text = "New";
            this.toolBtnNew.Click += new System.EventHandler(this.NewToolbarButtonClicked);
            // 
            // toolBtnOpen
            // 
            this.toolBtnOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnOpen.Image = global::TroyMICRDocumentGenerator.Properties.Resources.file;
            this.toolBtnOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnOpen.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.toolBtnOpen.Name = "toolBtnOpen";
            this.toolBtnOpen.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolBtnOpen.Size = new System.Drawing.Size(55, 36);
            this.toolBtnOpen.Text = "Open";
            this.toolBtnOpen.Click += new System.EventHandler(this.OpenToolbarButtonClicked);
            // 
            // toolBtnSave
            // 
            this.toolBtnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnSave.Image = global::TroyMICRDocumentGenerator.Properties.Resources.save;
            this.toolBtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnSave.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.toolBtnSave.Name = "toolBtnSave";
            this.toolBtnSave.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolBtnSave.Size = new System.Drawing.Size(55, 36);
            this.toolBtnSave.Text = "Save";
            this.toolBtnSave.Click += new System.EventHandler(this.SaveToolbarButtonClicked);
            // 
            // toolBtnPrint
            // 
            this.toolBtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnPrint.Image = global::TroyMICRDocumentGenerator.Properties.Resources.print;
            this.toolBtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnPrint.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.toolBtnPrint.Name = "toolBtnPrint";
            this.toolBtnPrint.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolBtnPrint.Size = new System.Drawing.Size(55, 36);
            this.toolBtnPrint.Text = "Print";
            this.toolBtnPrint.Click += new System.EventHandler(this.PrintToolbarButtonClicked);
            // 
            // toolBtnHelp
            // 
            this.toolBtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnHelp.Image = global::TroyMICRDocumentGenerator.Properties.Resources.question;
            this.toolBtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnHelp.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.toolBtnHelp.Name = "toolBtnHelp";
            this.toolBtnHelp.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolBtnHelp.Size = new System.Drawing.Size(55, 36);
            this.toolBtnHelp.Text = "About";
            this.toolBtnHelp.Click += new System.EventHandler(this.HelpToolbarButtonClicked);
            // 
            // toolBtnPrintTestPage
            // 
            this.toolBtnPrintTestPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnPrintTestPage.Image = global::TroyMICRDocumentGenerator.Properties.Resources.testPrint;
            this.toolBtnPrintTestPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnPrintTestPage.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.toolBtnPrintTestPage.Name = "toolBtnPrintTestPage";
            this.toolBtnPrintTestPage.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolBtnPrintTestPage.Size = new System.Drawing.Size(55, 36);
            this.toolBtnPrintTestPage.Text = "Print Test Patterns";
            this.toolBtnPrintTestPage.Click += new System.EventHandler(this.PrintTestDocument);
            // 
            // toolStripMainMenu
            // 
            this.toolStripMainMenu.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMainMenu.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMainMenu.ImageScalingSize = new System.Drawing.Size(31, 32);
            this.toolStripMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnNew,
            this.toolBtnOpen,
            this.toolBtnSave,
            this.toolBtnPrint,
            this.toolBtnHelp,
            this.toolBtnPrintTestPage});
            this.toolStripMainMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMainMenu.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripMainMenu.MaximumSize = new System.Drawing.Size(1018, 39);
            this.toolStripMainMenu.MinimumSize = new System.Drawing.Size(1018, 39);
            this.toolStripMainMenu.Name = "toolStripMainMenu";
            this.toolStripMainMenu.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripMainMenu.Size = new System.Drawing.Size(1018, 39);
            this.toolStripMainMenu.TabIndex = 1;
            this.toolStripMainMenu.Text = "toolStrip1";
            // 
            // TroyMICRDocGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 393);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.toolStripMainMenu);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1018, 432);
            this.MinimumSize = new System.Drawing.Size(1018, 432);
            this.Name = "TroyMICRDocGenerator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Troy MICR Document Generator - New";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ApplicationClosingEvent);
            this.Load += new System.EventHandler(this.ApplicationFormLoadEvent);
            this.tabPrinter.ResumeLayout(false);
            this.tabPrinter.PerformLayout();
            this.grpPrintOptions.ResumeLayout(false);
            this.grpPrintOptions.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tabCheck.ResumeLayout(false);
            this.tabCheck.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkDetailsNumericUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLeftMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTopMargin)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabTestDocument.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.milisecondsDelayNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondDelayNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfPrintsNumericUpDown)).EndInit();
            this.printOptionsGroupBox.ResumeLayout(false);
            this.printOptionsGroupBox.PerformLayout();
            this.fontsTabPage.ResumeLayout(false);
            this.toolStripMainMenu.ResumeLayout(false);
            this.toolStripMainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private IContainer components;
        private TabPage tabPrinter;
        private TextBox txtPrinterSerialNo;
        private Button btnGetSerialNumber;
        private Label lblPrinterSerialNo;
        private GroupBox grpPrintOptions;
        private RadioButton rd4UpPrinting;
        private RadioButton rd3UpPrinting;
        private Label lblBatchId;
        private TextBox txtJobName;
        private Label lblJobName;
        private ComboBox cbPaperSource;
        private Label lblPaperSource;
        private ComboBox cbPaperOutput;
        private Label lblPaperOutput;
        private ComboBox cbPaperSize;
        private Label lblPaperSize;
        private TextBox txtDensity;
        private Label lblDensity;
        private ComboBox cbPrinterName;
        private TextBox txtTonerType;
        private Label lblTonerType;
        private TextBox txtPrinterModel;
        private Label lblPrinterModel;
        private Label lblPrinterName;
        private TabControl tabMain;
        private ToolStripButton toolBtnNew;
        private ToolStripButton toolBtnOpen;
        private ToolStripButton toolBtnSave;
        private ToolStripButton toolBtnPrint;
        private ToolStripButton toolBtnHelp;
        private ToolStripButton toolBtnPrintTestPage;
        private ToolStrip toolStripMainMenu;
        private TabPage tabCheck;
        private TabPage tabTestDocument;
        private TextBox txtBatchId;
        private TableLayoutPanel tableLayoutPanel1;
        private Label lblCheck2;
        private Label lblCheck3;
        private Label lblCheck1;
        private Label lblCheck4;
        private TextBox txtHeight1;
        private TextBox txtHeight4;
        private TextBox txtHeight3;
        private TextBox txtHeight2;
        private Label lblCheckHdr;
        private Label lblYHght;
        private Label label7;
        private Label label9;
        private TextBox txtCheckNo;
        private TextBox txtNumberOfPages;
        private CheckBox checkDetailsPrintContinuously;
        private Label label8;
        private Label label6;
        private Label lblNumberOfPages;
        private GroupBox groupBox1;
        private Label label2;
        private Label label3;
        private RadioButton rdSoftFont;
        private RadioButton rdInstalledFont;
        private RadioButton rdResidentFont;
        private TextBox txtResidentFontID;
        private ComboBox cbMICRSourceFont;
        private GroupBox groupBox2;
        private MICRIDControl micrIDCtrl;
        private GroupBox groupBox5;
        private NumericUpDown numericUpDownLeftMargin;
        private NumericUpDown numericUpDownTopMargin;
        private Label label4;
        private Label label5;
        private NumericUpDown secondDelayNumericUpDown;
        private Label delayLable;
        private TextBox printCountTextBox;
        private Label printCountLabel;
        private Button stopPrintingButton;
        private CheckBox continuePrintingCheckBox;
        private NumericUpDown numberOfPrintsNumericUpDown;
        private Label numberOfPrintsLabel;
        private NumericUpDown milisecondsDelayNumericUpDown;
        private Label delayLabel;
        private Label checkDetailsDelayLabel;
        private NumericUpDown checkDetailsNumericUpDown;
        private Button checkDetailsStopPrinting;
        private TabPage fontsTabPage;
        private OpenFileDialog openFileDialog;
        private InstalledFonts installedFonts;
        private GroupBox printOptionsGroupBox;
        private RadioButton gray35RadioButton;
        private RadioButton gray25RadioButton;
        private RadioButton gray15RadioButton;
        private RadioButton gray50RadioButton;
        private RadioButton whitePageRadioButton;
        private RadioButton gradientRadioButton;
        private RadioButton blackPage;
        private GroupBox groupBox3;
        private RadioButton testPatternRadioButton;
        private CheckBox ignoreSourceCheckBox;
        private ComboBox cbMICRSourceFontEscapeSequence;
        private RadioButton rdEscapeSequence;
        private ToolTip toolTip1;
        private Button printToryFontListButton;
    }
}