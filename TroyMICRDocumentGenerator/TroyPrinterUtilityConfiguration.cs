﻿
using System.Collections.Generic;

namespace TroyMICRDocumentGenerator
{
    public class TroyPrinterUtilityConfiguration
    {
        public bool UseTwoByteStrings = false;
        public string DefaultCopyString = "COPY";
        public string InternalPrintQueueName = "Print To File";
        public bool MicrFeaturesEnabled = false;
        public bool AuditFeaturesEnabled = false;
        public bool DxiFeaturesEnabled = false;
        public bool DemoReportsEnabled = false;
        public bool IncludeTroyportPrintersInList = false;
        public bool DebugMode = false;
        public List<string> ReportLanguages = new List<string>();
        public List<string> CurrencySymbols = new List<string>();
        public List<string> ThousandsSeparator = new List<string>();
        public List<string> MpTags = new List<string>();
        public List<string> DecimalSymbols = new List<string>();
        public List<string> FontForTemplates = new List<string>();
        public List<string> TroyFontList = new List<string>();
        public int MaxNumberOfTroyFonts = 5;
        public bool UseAdminForAudit = false;
        public bool UseDownloadForAudit = false;
        public bool UseAdminForSecurity = false;
        public bool UseDownloadForSecurity = false;
        public bool UseAdminForTrayMapping = false;
        public bool UseDownloadForTrayMapping = false;
        public bool UseAdminForExPT = false;
        public bool UseDownloadForExPT = false;
        public bool UseDownloadForPgProfile = false;
        public bool UseAdminForPgProfile = false;
        public bool UseDownloadForDatCapProfile = false;
        public bool UseAdminForDatCapProfile = false;
        public bool UseGlyphsForTemplateLeadingText = true;
        public bool UseGlyphsForRegionStaticText = true;
        public bool UseGlyphsForMpMessage = true;
        public bool UseGlyphsForPantoMp = true;
        public bool UseGlyphsForPantoMsg = true;
        public bool DeleteTempFilesToPrinter = true;
        public bool DeleteGlyphFile = true;
    }
}
