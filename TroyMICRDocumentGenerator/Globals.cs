﻿using System;
using System.Text;
using System.IO;

namespace TroyMICRDocumentGenerator
{
    public static class Globals
    {
        public static int StartingFontId = 0;

        private static string BeginJob = "\u001B%-12345X@PJL ENTER LANGUAGE = PCL\u000D\u000A\u001BE";

        private static string EndJob = "\u001BE\u001B%-12345X";

        private static string UpdatePrinterPath = "PrinterUpdate";

        private static byte[] AdminUserBytes = new byte[] { 0x1B, 0x25, 0x75, 0x35, 0x57, 0x41, 0x44, 0x4D, 0x49, 0x4E };

        private static byte[] LoginBytes = new byte[] { 0x1B, 0x25, 0x75, 0x31, 0x53 };

        private static byte[] LogoffBytes = new byte[] { 0x1B, 0x25, 0x75, 0x30, 0x53 };

        private static byte[] EnterDownloadBytes = new byte[] { 0x1B, 0x25, 0x76, 0x31, 0x44 };

        private static byte[] ExitDownloadBytes = new byte[] { 0x1B, 0x25, 0x76, 0x30, 0x44 };

        public static string MainPath = "";

        public static string SaveLocation = Path.Combine(MainPath, "SavedSettings");

        private static decimal cmConv = (decimal)2.54;

        public static string ReplaceSpecialChars(string str)
        {
            try
            {
                string retstr = str.Replace("/e", "\u001B");
                retstr = retstr.Replace(@"/r", "\u000D");
                retstr = retstr.Replace(@"/n", "\u000A");
                retstr = retstr.Replace(@"/s", "\u0002");
                retstr = retstr.Replace(@"/x", "\u0003");
                retstr = retstr.Replace(@"/t", "\u0009");
                retstr = retstr.Replace(@"//", "\\");
                return retstr;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ConvertCmTo600Dpi), nameof(Globals));
                return string.Empty;
            }
        }

        public static int ConvertCmTo600Dpi(decimal cm)
        {
            try
            {
                decimal inches = cm / cmConv;
                inches = inches * 600;

                return Convert.ToInt32(inches);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ConvertCmTo600Dpi), nameof(Globals));
                return 0;
            }
        }

        public static int ConvertCmTo720Dpi(decimal cm)
        {
            try
            {
                decimal inches = cm / cmConv;
                inches = inches * 720;

                return Convert.ToInt32(inches);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ConvertCmTo720Dpi), nameof(Globals));
                return 0;
            }
        }

        public static void DownloadBytesToPrinter(byte[] outputbytes, bool adminLoginNeeded, bool downloadModeNeeded, bool includeJobBeginEnd = true)
        {
            try
            {
                string tempfilename = Path.Combine(MainPath, UpdatePrinterPath + @"\tempoutput" + DateTime.Now.ToString("yyMMddhhmmss") + ".prn");

                BinaryWriter outbuf = new BinaryWriter(File.Open(tempfilename, FileMode.Create));
                outbuf.Write(outputbytes, 0, outputbytes.Length);
                outbuf.Flush();
                outbuf.Close();
                DownloadFileToPrinter(tempfilename, adminLoginNeeded, downloadModeNeeded, includeJobBeginEnd);

                File.Delete(tempfilename);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DownloadBytesToPrinter), nameof(Globals));
            }
        }

        public static void DownloadFileToPrinter(string fileName, string leadingPcl, string trailingPcl, bool adminLoginNeeded, bool downloadModeNeeded, bool includeJobBeginEnd = true)
        {
            try
            {
                string tempfilename = Path.Combine(MainPath, UpdatePrinterPath + @"\temp" + DateTime.Now.ToString("yyMMddhhmmss") + ".prn");
                BinaryWriter outbuf = new BinaryWriter(File.Open(tempfilename, FileMode.Create));

                if (includeJobBeginEnd)
                {
                    byte[] beginJob = new UTF8Encoding().GetBytes(BeginJob);
                    outbuf.Write(beginJob, 0, beginJob.Length);
                }
                if (adminLoginNeeded)
                {
                    outbuf.Write(AdminUserBytes, 0, AdminUserBytes.Length);
                    string temp = "\u001B%p" + AdminPassword.Length.ToString() + "W";
                    byte[] passwordBytes = new UTF8Encoding().GetBytes(temp);
                    outbuf.Write(passwordBytes, 0, passwordBytes.Length);
                    byte[] AdminPasswordBytes = null;
                    if (tpuc.UseTwoByteStrings)
                    {
                        AdminPasswordBytes = new UnicodeEncoding().GetBytes(AdminPassword);
                    }
                    else
                    {
                        AdminPasswordBytes = new UTF8Encoding().GetBytes(AdminPassword);
                    }
                    outbuf.Write(AdminPasswordBytes, 0, AdminPasswordBytes.Length);
                    outbuf.Write(LoginBytes, 0, LoginBytes.Length);

                }
                if (downloadModeNeeded)
                {
                    outbuf.Write(EnterDownloadBytes, 0, EnterDownloadBytes.Length);
                }
                if (!string.IsNullOrEmpty(leadingPcl))
                {
                    byte[] temp = new UTF8Encoding().GetBytes(leadingPcl);
                    outbuf.Write(temp, 0, temp.Length);
                }
                FileStream pgFile = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryReader binReader = new BinaryReader(pgFile);
                byte[] inBuffer = new Byte[pgFile.Length];
                inBuffer = binReader.ReadBytes(Convert.ToInt32(pgFile.Length));
                outbuf.Write(inBuffer, 0, inBuffer.Length);
                binReader.Close();

                if (!string.IsNullOrEmpty(trailingPcl))
                {
                    byte[] temp = new UTF8Encoding().GetBytes(trailingPcl);
                    outbuf.Write(temp, 0, temp.Length);
                }

                if (downloadModeNeeded)
                {
                    outbuf.Write(ExitDownloadBytes, 0, ExitDownloadBytes.Length);
                }
                if (adminLoginNeeded)
                {
                    outbuf.Write(LogoffBytes, 0, LogoffBytes.Length);
                }
                if (includeJobBeginEnd)
                {
                    byte[] endJob = new UTF8Encoding().GetBytes(EndJob);
                    outbuf.Write(endJob, 0, endJob.Length);
                }

                outbuf.Flush();
                outbuf.Close();
                PrintToSpooler.SendFileToPrinter(SelectedPrinter, tempfilename, "Troy Printer Utility");

                if (tpuc.DeleteTempFilesToPrinter) File.Delete(tempfilename);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DownloadFileToPrinter), nameof(Globals));
            }
        }

        public static void DownloadFileToPrinter(string fileName, bool adminLoginNeeded, bool downloadModeNeeded, bool includeJobBeginEnd = true)
        {
            try
            {
                if ((!adminLoginNeeded) && (!downloadModeNeeded) && (!includeJobBeginEnd))
                {
                    PrintToSpooler.SendFileToPrinter(SelectedPrinter, fileName, "Troy Printer Utility");
                }
                else
                {
                    string tempfilename = Path.Combine(MainPath, UpdatePrinterPath + @"\temp" + DateTime.Now.ToString("yyMMddhhmmss") + ".prn");
                    BinaryWriter outbuf = new BinaryWriter(File.Open(tempfilename, FileMode.Create));

                    if (includeJobBeginEnd)
                    {
                        byte[] beginJob = new UTF8Encoding().GetBytes(BeginJob);
                        outbuf.Write(beginJob, 0, beginJob.Length);
                    }
                    if (adminLoginNeeded)
                    {
                        outbuf.Write(AdminUserBytes, 0, AdminUserBytes.Length);
                        string temp = "\u001B%p" + AdminPassword.Length.ToString() + "W";
                        byte[] passwordBytes = new UTF8Encoding().GetBytes(temp);
                        outbuf.Write(passwordBytes, 0, passwordBytes.Length);
                        byte[] AdminPasswordBytes = null;
                        if (tpuc.UseTwoByteStrings)
                        {
                            AdminPasswordBytes = new UnicodeEncoding().GetBytes(AdminPassword);
                        }
                        else
                        {
                            AdminPasswordBytes = new UTF8Encoding().GetBytes(AdminPassword);
                        }
                        outbuf.Write(AdminPasswordBytes, 0, AdminPasswordBytes.Length);
                        outbuf.Write(LoginBytes, 0, LoginBytes.Length);

                    }
                    if (downloadModeNeeded)
                    {
                        outbuf.Write(EnterDownloadBytes, 0, EnterDownloadBytes.Length);
                    }

                    FileStream pgFile = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    BinaryReader binReader = new BinaryReader(pgFile);
                    byte[] inBuffer = new Byte[pgFile.Length];
                    inBuffer = binReader.ReadBytes(Convert.ToInt32(pgFile.Length));
                    outbuf.Write(inBuffer, 0, inBuffer.Length);
                    binReader.Close();


                    if (downloadModeNeeded)
                    {
                        outbuf.Write(ExitDownloadBytes, 0, ExitDownloadBytes.Length);
                    }
                    if (adminLoginNeeded)
                    {
                        outbuf.Write(LogoffBytes, 0, LogoffBytes.Length);
                    }
                    if (includeJobBeginEnd)
                    {
                        byte[] endJob = new UTF8Encoding().GetBytes(EndJob);
                        outbuf.Write(endJob, 0, endJob.Length);
                    }

                    outbuf.Flush();
                    outbuf.Close();
                    PrintToSpooler.SendFileToPrinter(SelectedPrinter, tempfilename, "Troy Printer Utility");

                    if (tpuc.DeleteTempFilesToPrinter) File.Delete(tempfilename);

                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DownloadFileToPrinter), nameof(Globals));
            }
        }

        public static TroyPrinterUtilityConfiguration tpuc = null;
        public static string SelectedPrinter = "";
        public static string MainConfigurationFileName = "TroyPrinterUtilityConfiguration.xml";
        public static string AdminPassword = "";
    }
}
