﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace TroyMICRDocumentGenerator
{
    partial class MICRIDControl
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MICRIDControl));
            this.lblAmountField = new System.Windows.Forms.Label();
            this.lblOnUSField = new System.Windows.Forms.Label();
            this.lblTransitField = new System.Windows.Forms.Label();
            this.lblAuxOnUSField = new System.Windows.Forms.Label();
            this.lblNumbering = new System.Windows.Forms.Label();
            this.txt44 = new System.Windows.Forms.TextBox();
            this.txt46 = new System.Windows.Forms.TextBox();
            this.txt45 = new System.Windows.Forms.TextBox();
            this.txt47 = new System.Windows.Forms.TextBox();
            this.txt49 = new System.Windows.Forms.TextBox();
            this.txt48 = new System.Windows.Forms.TextBox();
            this.txt50 = new System.Windows.Forms.TextBox();
            this.txt52 = new System.Windows.Forms.TextBox();
            this.txt51 = new System.Windows.Forms.TextBox();
            this.txt53 = new System.Windows.Forms.TextBox();
            this.txt54 = new System.Windows.Forms.TextBox();
            this.txt14 = new System.Windows.Forms.TextBox();
            this.txt13 = new System.Windows.Forms.TextBox();
            this.txt15 = new System.Windows.Forms.TextBox();
            this.txt17 = new System.Windows.Forms.TextBox();
            this.txt16 = new System.Windows.Forms.TextBox();
            this.txt18 = new System.Windows.Forms.TextBox();
            this.txt20 = new System.Windows.Forms.TextBox();
            this.txt19 = new System.Windows.Forms.TextBox();
            this.txt21 = new System.Windows.Forms.TextBox();
            this.txt23 = new System.Windows.Forms.TextBox();
            this.txt22 = new System.Windows.Forms.TextBox();
            this.txt24 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.txt5 = new System.Windows.Forms.TextBox();
            this.txt4 = new System.Windows.Forms.TextBox();
            this.txt6 = new System.Windows.Forms.TextBox();
            this.txt8 = new System.Windows.Forms.TextBox();
            this.txt7 = new System.Windows.Forms.TextBox();
            this.txt9 = new System.Windows.Forms.TextBox();
            this.txt11 = new System.Windows.Forms.TextBox();
            this.txt10 = new System.Windows.Forms.TextBox();
            this.txt12 = new System.Windows.Forms.TextBox();
            this.txt26 = new System.Windows.Forms.TextBox();
            this.txt25 = new System.Windows.Forms.TextBox();
            this.txt27 = new System.Windows.Forms.TextBox();
            this.txt29 = new System.Windows.Forms.TextBox();
            this.txt28 = new System.Windows.Forms.TextBox();
            this.txt30 = new System.Windows.Forms.TextBox();
            this.txt32 = new System.Windows.Forms.TextBox();
            this.txt31 = new System.Windows.Forms.TextBox();
            this.txt33 = new System.Windows.Forms.TextBox();
            this.txt35 = new System.Windows.Forms.TextBox();
            this.txt34 = new System.Windows.Forms.TextBox();
            this.txt36 = new System.Windows.Forms.TextBox();
            this.txt38 = new System.Windows.Forms.TextBox();
            this.txt37 = new System.Windows.Forms.TextBox();
            this.txt39 = new System.Windows.Forms.TextBox();
            this.txt41 = new System.Windows.Forms.TextBox();
            this.txt40 = new System.Windows.Forms.TextBox();
            this.txt42 = new System.Windows.Forms.TextBox();
            this.txt55 = new System.Windows.Forms.TextBox();
            this.txt43 = new System.Windows.Forms.TextBox();
            this.txt56 = new System.Windows.Forms.TextBox();
            this.txt58 = new System.Windows.Forms.TextBox();
            this.txt57 = new System.Windows.Forms.TextBox();
            this.txtStartAuxOnUS = new System.Windows.Forms.TextBox();
            this.lblStartingCheckAmt = new System.Windows.Forms.Label();
            this.txtStartTransit = new System.Windows.Forms.TextBox();
            this.txtStartOnUS = new System.Windows.Forms.TextBox();
            this.txtStartAmount = new System.Windows.Forms.TextBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CMC7RadioButton = new System.Windows.Forms.RadioButton();
            this.E13BRadioButton = new System.Windows.Forms.RadioButton();
            this.displayFontLablel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAmountField
            // 
            this.lblAmountField.AutoSize = true;
            this.lblAmountField.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountField.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblAmountField.Location = new System.Drawing.Point(858, 72);
            this.lblAmountField.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAmountField.Name = "lblAmountField";
            this.lblAmountField.Size = new System.Drawing.Size(106, 18);
            this.lblAmountField.TabIndex = 129;
            this.lblAmountField.Text = "Amount Field";
            // 
            // lblOnUSField
            // 
            this.lblOnUSField.AutoSize = true;
            this.lblOnUSField.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOnUSField.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblOnUSField.Location = new System.Drawing.Point(585, 72);
            this.lblOnUSField.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOnUSField.Name = "lblOnUSField";
            this.lblOnUSField.Size = new System.Drawing.Size(99, 18);
            this.lblOnUSField.TabIndex = 128;
            this.lblOnUSField.Text = "On US Field";
            // 
            // lblTransitField
            // 
            this.lblTransitField.AutoSize = true;
            this.lblTransitField.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransitField.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblTransitField.Location = new System.Drawing.Point(292, 72);
            this.lblTransitField.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTransitField.Name = "lblTransitField";
            this.lblTransitField.Size = new System.Drawing.Size(128, 18);
            this.lblTransitField.TabIndex = 127;
            this.lblTransitField.Text = "Transit No Field";
            // 
            // lblAuxOnUSField
            // 
            this.lblAuxOnUSField.AutoSize = true;
            this.lblAuxOnUSField.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAuxOnUSField.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblAuxOnUSField.Location = new System.Drawing.Point(58, 69);
            this.lblAuxOnUSField.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAuxOnUSField.Name = "lblAuxOnUSField";
            this.lblAuxOnUSField.Size = new System.Drawing.Size(136, 18);
            this.lblAuxOnUSField.TabIndex = 126;
            this.lblAuxOnUSField.Text = "Aux. On US Field";
            // 
            // lblNumbering
            // 
            this.lblNumbering.AutoSize = true;
            this.lblNumbering.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumbering.Location = new System.Drawing.Point(-1, 35);
            this.lblNumbering.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumbering.Name = "lblNumbering";
            this.lblNumbering.Size = new System.Drawing.Size(985, 13);
            this.lblNumbering.TabIndex = 125;
            this.lblNumbering.Text = resources.GetString("lblNumbering.Text");
            // 
            // txt44
            // 
            this.txt44.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt44.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt44.Location = new System.Drawing.Point(239, 50);
            this.txt44.Margin = new System.Windows.Forms.Padding(0);
            this.txt44.Name = "txt44";
            this.txt44.Size = new System.Drawing.Size(17, 17);
            this.txt44.TabIndex = 18;
            this.txt44.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt44.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt44.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt46
            // 
            this.txt46.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt46.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt46.Location = new System.Drawing.Point(205, 50);
            this.txt46.Margin = new System.Windows.Forms.Padding(0);
            this.txt46.Name = "txt46";
            this.txt46.Size = new System.Drawing.Size(17, 17);
            this.txt46.TabIndex = 16;
            this.txt46.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt46.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt46.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt45
            // 
            this.txt45.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt45.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt45.Location = new System.Drawing.Point(222, 50);
            this.txt45.Margin = new System.Windows.Forms.Padding(0);
            this.txt45.Name = "txt45";
            this.txt45.Size = new System.Drawing.Size(17, 17);
            this.txt45.TabIndex = 17;
            this.txt45.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt45.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt45.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt47
            // 
            this.txt47.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt47.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt47.Location = new System.Drawing.Point(188, 50);
            this.txt47.Margin = new System.Windows.Forms.Padding(0);
            this.txt47.Name = "txt47";
            this.txt47.Size = new System.Drawing.Size(17, 17);
            this.txt47.TabIndex = 15;
            this.txt47.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt47.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt47.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt49
            // 
            this.txt49.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt49.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt49.Location = new System.Drawing.Point(154, 50);
            this.txt49.Margin = new System.Windows.Forms.Padding(0);
            this.txt49.Name = "txt49";
            this.txt49.Size = new System.Drawing.Size(17, 17);
            this.txt49.TabIndex = 13;
            this.txt49.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt49.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt49.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt48
            // 
            this.txt48.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt48.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt48.Location = new System.Drawing.Point(171, 50);
            this.txt48.Margin = new System.Windows.Forms.Padding(0);
            this.txt48.Name = "txt48";
            this.txt48.Size = new System.Drawing.Size(17, 17);
            this.txt48.TabIndex = 14;
            this.txt48.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt48.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt48.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt50
            // 
            this.txt50.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt50.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt50.Location = new System.Drawing.Point(137, 50);
            this.txt50.Margin = new System.Windows.Forms.Padding(0);
            this.txt50.Name = "txt50";
            this.txt50.Size = new System.Drawing.Size(17, 17);
            this.txt50.TabIndex = 12;
            this.txt50.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt50.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt50.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt52
            // 
            this.txt52.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt52.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt52.Location = new System.Drawing.Point(103, 50);
            this.txt52.Margin = new System.Windows.Forms.Padding(0);
            this.txt52.Name = "txt52";
            this.txt52.Size = new System.Drawing.Size(17, 17);
            this.txt52.TabIndex = 10;
            this.txt52.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt52.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt52.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt51
            // 
            this.txt51.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt51.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt51.Location = new System.Drawing.Point(120, 50);
            this.txt51.Margin = new System.Windows.Forms.Padding(0);
            this.txt51.Name = "txt51";
            this.txt51.Size = new System.Drawing.Size(17, 17);
            this.txt51.TabIndex = 11;
            this.txt51.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt51.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt51.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt53
            // 
            this.txt53.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt53.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt53.Location = new System.Drawing.Point(86, 50);
            this.txt53.Margin = new System.Windows.Forms.Padding(0);
            this.txt53.Name = "txt53";
            this.txt53.Size = new System.Drawing.Size(17, 17);
            this.txt53.TabIndex = 9;
            this.txt53.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt53.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt53.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt54
            // 
            this.txt54.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt54.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt54.Location = new System.Drawing.Point(69, 50);
            this.txt54.Margin = new System.Windows.Forms.Padding(0);
            this.txt54.Name = "txt54";
            this.txt54.Size = new System.Drawing.Size(17, 17);
            this.txt54.TabIndex = 8;
            this.txt54.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt54.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt54.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt14
            // 
            this.txt14.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt14.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt14.Location = new System.Drawing.Point(759, 50);
            this.txt14.Margin = new System.Windows.Forms.Padding(0);
            this.txt14.Name = "txt14";
            this.txt14.Size = new System.Drawing.Size(17, 17);
            this.txt14.TabIndex = 48;
            this.txt14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt14.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt14.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt13
            // 
            this.txt13.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt13.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt13.Location = new System.Drawing.Point(776, 50);
            this.txt13.Margin = new System.Windows.Forms.Padding(0);
            this.txt13.Name = "txt13";
            this.txt13.Size = new System.Drawing.Size(17, 17);
            this.txt13.TabIndex = 49;
            this.txt13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt13.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt15
            // 
            this.txt15.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt15.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt15.Location = new System.Drawing.Point(742, 50);
            this.txt15.Margin = new System.Windows.Forms.Padding(0);
            this.txt15.Name = "txt15";
            this.txt15.Size = new System.Drawing.Size(17, 17);
            this.txt15.TabIndex = 47;
            this.txt15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt15.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt15.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt17
            // 
            this.txt17.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt17.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt17.Location = new System.Drawing.Point(708, 50);
            this.txt17.Margin = new System.Windows.Forms.Padding(0);
            this.txt17.Name = "txt17";
            this.txt17.Size = new System.Drawing.Size(17, 17);
            this.txt17.TabIndex = 45;
            this.txt17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt17.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt17.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt16
            // 
            this.txt16.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt16.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt16.Location = new System.Drawing.Point(725, 50);
            this.txt16.Margin = new System.Windows.Forms.Padding(0);
            this.txt16.Name = "txt16";
            this.txt16.Size = new System.Drawing.Size(17, 17);
            this.txt16.TabIndex = 46;
            this.txt16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt16.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt16.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt18
            // 
            this.txt18.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt18.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt18.Location = new System.Drawing.Point(691, 50);
            this.txt18.Margin = new System.Windows.Forms.Padding(0);
            this.txt18.Name = "txt18";
            this.txt18.Size = new System.Drawing.Size(17, 17);
            this.txt18.TabIndex = 44;
            this.txt18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt18.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt18.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt20
            // 
            this.txt20.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt20.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt20.Location = new System.Drawing.Point(657, 50);
            this.txt20.Margin = new System.Windows.Forms.Padding(0);
            this.txt20.Name = "txt20";
            this.txt20.Size = new System.Drawing.Size(17, 17);
            this.txt20.TabIndex = 42;
            this.txt20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt20.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt20.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt19
            // 
            this.txt19.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt19.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt19.Location = new System.Drawing.Point(674, 50);
            this.txt19.Margin = new System.Windows.Forms.Padding(0);
            this.txt19.Name = "txt19";
            this.txt19.Size = new System.Drawing.Size(17, 17);
            this.txt19.TabIndex = 43;
            this.txt19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt19.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt19.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt21
            // 
            this.txt21.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt21.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt21.Location = new System.Drawing.Point(640, 50);
            this.txt21.Margin = new System.Windows.Forms.Padding(0);
            this.txt21.Name = "txt21";
            this.txt21.Size = new System.Drawing.Size(17, 17);
            this.txt21.TabIndex = 41;
            this.txt21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt21.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt23
            // 
            this.txt23.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt23.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt23.Location = new System.Drawing.Point(606, 50);
            this.txt23.Margin = new System.Windows.Forms.Padding(0);
            this.txt23.Name = "txt23";
            this.txt23.Size = new System.Drawing.Size(17, 17);
            this.txt23.TabIndex = 39;
            this.txt23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt23.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt23.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt22
            // 
            this.txt22.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt22.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt22.Location = new System.Drawing.Point(623, 50);
            this.txt22.Margin = new System.Windows.Forms.Padding(0);
            this.txt22.Name = "txt22";
            this.txt22.Size = new System.Drawing.Size(17, 17);
            this.txt22.TabIndex = 40;
            this.txt22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt22.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt24
            // 
            this.txt24.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt24.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt24.Location = new System.Drawing.Point(589, 50);
            this.txt24.Margin = new System.Windows.Forms.Padding(0);
            this.txt24.Name = "txt24";
            this.txt24.Size = new System.Drawing.Size(17, 17);
            this.txt24.TabIndex = 38;
            this.txt24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt24.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt24.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt2
            // 
            this.txt2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt2.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt2.Location = new System.Drawing.Point(968, 50);
            this.txt2.Margin = new System.Windows.Forms.Padding(0);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(17, 17);
            this.txt2.TabIndex = 60;
            this.txt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt1
            // 
            this.txt1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt1.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt1.Location = new System.Drawing.Point(985, 50);
            this.txt1.Margin = new System.Windows.Forms.Padding(0);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(17, 17);
            this.txt1.TabIndex = 61;
            this.txt1.Text = "A";
            this.txt1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt3
            // 
            this.txt3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt3.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt3.Location = new System.Drawing.Point(951, 50);
            this.txt3.Margin = new System.Windows.Forms.Padding(0);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(17, 17);
            this.txt3.TabIndex = 59;
            this.txt3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt5
            // 
            this.txt5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt5.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt5.Location = new System.Drawing.Point(917, 50);
            this.txt5.Margin = new System.Windows.Forms.Padding(0);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(17, 17);
            this.txt5.TabIndex = 57;
            this.txt5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt4
            // 
            this.txt4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt4.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt4.Location = new System.Drawing.Point(934, 50);
            this.txt4.Margin = new System.Windows.Forms.Padding(0);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(17, 17);
            this.txt4.TabIndex = 58;
            this.txt4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt6
            // 
            this.txt6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt6.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt6.Location = new System.Drawing.Point(900, 50);
            this.txt6.Margin = new System.Windows.Forms.Padding(0);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(17, 17);
            this.txt6.TabIndex = 56;
            this.txt6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt6.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt8
            // 
            this.txt8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt8.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt8.Location = new System.Drawing.Point(866, 50);
            this.txt8.Margin = new System.Windows.Forms.Padding(0);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(17, 17);
            this.txt8.TabIndex = 54;
            this.txt8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt8.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt7
            // 
            this.txt7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt7.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt7.Location = new System.Drawing.Point(883, 50);
            this.txt7.Margin = new System.Windows.Forms.Padding(0);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(17, 17);
            this.txt7.TabIndex = 55;
            this.txt7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt7.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt9
            // 
            this.txt9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt9.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt9.Location = new System.Drawing.Point(849, 50);
            this.txt9.Margin = new System.Windows.Forms.Padding(0);
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(17, 17);
            this.txt9.TabIndex = 53;
            this.txt9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt9.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt11
            // 
            this.txt11.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt11.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt11.Location = new System.Drawing.Point(815, 50);
            this.txt11.Margin = new System.Windows.Forms.Padding(0);
            this.txt11.Name = "txt11";
            this.txt11.Size = new System.Drawing.Size(17, 17);
            this.txt11.TabIndex = 51;
            this.txt11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt11.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt10
            // 
            this.txt10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt10.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt10.Location = new System.Drawing.Point(832, 50);
            this.txt10.Margin = new System.Windows.Forms.Padding(0);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(17, 17);
            this.txt10.TabIndex = 52;
            this.txt10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt10.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt12
            // 
            this.txt12.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt12.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt12.Location = new System.Drawing.Point(798, 50);
            this.txt12.Margin = new System.Windows.Forms.Padding(0);
            this.txt12.Name = "txt12";
            this.txt12.Size = new System.Drawing.Size(17, 17);
            this.txt12.TabIndex = 50;
            this.txt12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt12.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt26
            // 
            this.txt26.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt26.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt26.Location = new System.Drawing.Point(555, 50);
            this.txt26.Margin = new System.Windows.Forms.Padding(0);
            this.txt26.Name = "txt26";
            this.txt26.Size = new System.Drawing.Size(17, 17);
            this.txt26.TabIndex = 36;
            this.txt26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt26.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt26.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt25
            // 
            this.txt25.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt25.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt25.Location = new System.Drawing.Point(572, 50);
            this.txt25.Margin = new System.Windows.Forms.Padding(0);
            this.txt25.Name = "txt25";
            this.txt25.Size = new System.Drawing.Size(17, 17);
            this.txt25.TabIndex = 37;
            this.txt25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt25.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt25.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt27
            // 
            this.txt27.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt27.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt27.Location = new System.Drawing.Point(538, 50);
            this.txt27.Margin = new System.Windows.Forms.Padding(0);
            this.txt27.Name = "txt27";
            this.txt27.Size = new System.Drawing.Size(17, 17);
            this.txt27.TabIndex = 35;
            this.txt27.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt27.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt27.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt29
            // 
            this.txt29.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt29.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt29.Location = new System.Drawing.Point(504, 50);
            this.txt29.Margin = new System.Windows.Forms.Padding(0);
            this.txt29.Name = "txt29";
            this.txt29.Size = new System.Drawing.Size(17, 17);
            this.txt29.TabIndex = 33;
            this.txt29.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt29.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt29.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt28
            // 
            this.txt28.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt28.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt28.Location = new System.Drawing.Point(521, 50);
            this.txt28.Margin = new System.Windows.Forms.Padding(0);
            this.txt28.Name = "txt28";
            this.txt28.Size = new System.Drawing.Size(17, 17);
            this.txt28.TabIndex = 34;
            this.txt28.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt28.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt28.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt30
            // 
            this.txt30.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt30.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt30.Location = new System.Drawing.Point(487, 50);
            this.txt30.Margin = new System.Windows.Forms.Padding(0);
            this.txt30.Name = "txt30";
            this.txt30.Size = new System.Drawing.Size(17, 17);
            this.txt30.TabIndex = 32;
            this.txt30.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt30.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt30.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt32
            // 
            this.txt32.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt32.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt32.Location = new System.Drawing.Point(453, 50);
            this.txt32.Margin = new System.Windows.Forms.Padding(0);
            this.txt32.Name = "txt32";
            this.txt32.Size = new System.Drawing.Size(17, 17);
            this.txt32.TabIndex = 30;
            this.txt32.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt32.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt32.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt31
            // 
            this.txt31.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt31.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt31.Location = new System.Drawing.Point(470, 50);
            this.txt31.Margin = new System.Windows.Forms.Padding(0);
            this.txt31.Name = "txt31";
            this.txt31.Size = new System.Drawing.Size(17, 17);
            this.txt31.TabIndex = 31;
            this.txt31.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt31.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt33
            // 
            this.txt33.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt33.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt33.Location = new System.Drawing.Point(431, 50);
            this.txt33.Margin = new System.Windows.Forms.Padding(0);
            this.txt33.Name = "txt33";
            this.txt33.Size = new System.Drawing.Size(17, 17);
            this.txt33.TabIndex = 29;
            this.txt33.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt33.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt33.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt35
            // 
            this.txt35.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt35.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt35.Location = new System.Drawing.Point(397, 50);
            this.txt35.Margin = new System.Windows.Forms.Padding(0);
            this.txt35.Name = "txt35";
            this.txt35.Size = new System.Drawing.Size(17, 17);
            this.txt35.TabIndex = 27;
            this.txt35.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt35.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt35.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt34
            // 
            this.txt34.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt34.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt34.Location = new System.Drawing.Point(414, 50);
            this.txt34.Margin = new System.Windows.Forms.Padding(0);
            this.txt34.Name = "txt34";
            this.txt34.Size = new System.Drawing.Size(17, 17);
            this.txt34.TabIndex = 28;
            this.txt34.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt34.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt34.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt36
            // 
            this.txt36.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt36.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt36.Location = new System.Drawing.Point(380, 50);
            this.txt36.Margin = new System.Windows.Forms.Padding(0);
            this.txt36.Name = "txt36";
            this.txt36.Size = new System.Drawing.Size(17, 17);
            this.txt36.TabIndex = 26;
            this.txt36.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt36.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt36.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt38
            // 
            this.txt38.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt38.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt38.Location = new System.Drawing.Point(346, 50);
            this.txt38.Margin = new System.Windows.Forms.Padding(0);
            this.txt38.Name = "txt38";
            this.txt38.Size = new System.Drawing.Size(17, 17);
            this.txt38.TabIndex = 24;
            this.txt38.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt38.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt38.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt37
            // 
            this.txt37.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt37.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt37.Location = new System.Drawing.Point(363, 50);
            this.txt37.Margin = new System.Windows.Forms.Padding(0);
            this.txt37.Name = "txt37";
            this.txt37.Size = new System.Drawing.Size(17, 17);
            this.txt37.TabIndex = 25;
            this.txt37.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt37.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt37.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt39
            // 
            this.txt39.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt39.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt39.Location = new System.Drawing.Point(329, 50);
            this.txt39.Margin = new System.Windows.Forms.Padding(0);
            this.txt39.Name = "txt39";
            this.txt39.Size = new System.Drawing.Size(17, 17);
            this.txt39.TabIndex = 23;
            this.txt39.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt39.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt39.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt41
            // 
            this.txt41.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt41.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt41.Location = new System.Drawing.Point(295, 50);
            this.txt41.Margin = new System.Windows.Forms.Padding(0);
            this.txt41.Name = "txt41";
            this.txt41.Size = new System.Drawing.Size(17, 17);
            this.txt41.TabIndex = 21;
            this.txt41.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt41.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt41.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt40
            // 
            this.txt40.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt40.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt40.Location = new System.Drawing.Point(312, 50);
            this.txt40.Margin = new System.Windows.Forms.Padding(0);
            this.txt40.Name = "txt40";
            this.txt40.Size = new System.Drawing.Size(17, 17);
            this.txt40.TabIndex = 22;
            this.txt40.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt40.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt40.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt42
            // 
            this.txt42.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt42.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt42.Location = new System.Drawing.Point(278, 50);
            this.txt42.Margin = new System.Windows.Forms.Padding(0);
            this.txt42.Name = "txt42";
            this.txt42.Size = new System.Drawing.Size(17, 17);
            this.txt42.TabIndex = 20;
            this.txt42.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt42.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt42.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt55
            // 
            this.txt55.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt55.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt55.Location = new System.Drawing.Point(52, 50);
            this.txt55.Margin = new System.Windows.Forms.Padding(0);
            this.txt55.Name = "txt55";
            this.txt55.Size = new System.Drawing.Size(17, 17);
            this.txt55.TabIndex = 7;
            this.txt55.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt55.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt55.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt43
            // 
            this.txt43.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt43.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt43.Location = new System.Drawing.Point(261, 50);
            this.txt43.Margin = new System.Windows.Forms.Padding(0);
            this.txt43.Name = "txt43";
            this.txt43.Size = new System.Drawing.Size(17, 17);
            this.txt43.TabIndex = 19;
            this.txt43.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt43.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt43.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt56
            // 
            this.txt56.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt56.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt56.Location = new System.Drawing.Point(35, 50);
            this.txt56.Margin = new System.Windows.Forms.Padding(0);
            this.txt56.Name = "txt56";
            this.txt56.Size = new System.Drawing.Size(17, 17);
            this.txt56.TabIndex = 6;
            this.txt56.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt56.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt56.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt58
            // 
            this.txt58.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt58.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt58.Location = new System.Drawing.Point(1, 50);
            this.txt58.Margin = new System.Windows.Forms.Padding(0);
            this.txt58.Name = "txt58";
            this.txt58.Size = new System.Drawing.Size(17, 17);
            this.txt58.TabIndex = 4;
            this.txt58.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt58.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt58.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txt57
            // 
            this.txt57.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt57.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txt57.Location = new System.Drawing.Point(18, 50);
            this.txt57.Margin = new System.Windows.Forms.Padding(0);
            this.txt57.Name = "txt57";
            this.txt57.Size = new System.Drawing.Size(17, 17);
            this.txt57.TabIndex = 5;
            this.txt57.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyDownEvent);
            this.txt57.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MicrIdKeyPressedEvent);
            this.txt57.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MicrIdKeyUpEvent);
            // 
            // txtStartAuxOnUS
            // 
            this.txtStartAuxOnUS.Enabled = false;
            this.txtStartAuxOnUS.Location = new System.Drawing.Point(178, 6);
            this.txtStartAuxOnUS.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtStartAuxOnUS.Name = "txtStartAuxOnUS";
            this.txtStartAuxOnUS.Size = new System.Drawing.Size(75, 20);
            this.txtStartAuxOnUS.TabIndex = 0;
            this.txtStartAuxOnUS.TextChanged += new System.EventHandler(this.AutoIncrStartAtChangedEvent);
            this.txtStartAuxOnUS.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AutoIncrStartValueKeyDownEvent);
            this.txtStartAuxOnUS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AutoIncrStartValueKeyPressedEvent);
            // 
            // lblStartingCheckAmt
            // 
            this.lblStartingCheckAmt.AutoSize = true;
            this.lblStartingCheckAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartingCheckAmt.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblStartingCheckAmt.Location = new System.Drawing.Point(2, 6);
            this.lblStartingCheckAmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStartingCheckAmt.Name = "lblStartingCheckAmt";
            this.lblStartingCheckAmt.Size = new System.Drawing.Size(85, 18);
            this.lblStartingCheckAmt.TabIndex = 131;
            this.lblStartingCheckAmt.Text = "Auto Incr. ";
            // 
            // txtStartTransit
            // 
            this.txtStartTransit.Enabled = false;
            this.txtStartTransit.Location = new System.Drawing.Point(370, 6);
            this.txtStartTransit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtStartTransit.Name = "txtStartTransit";
            this.txtStartTransit.Size = new System.Drawing.Size(75, 20);
            this.txtStartTransit.TabIndex = 1;
            this.txtStartTransit.TextChanged += new System.EventHandler(this.AutoIncrStartAtChangedEvent);
            this.txtStartTransit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AutoIncrStartValueKeyDownEvent);
            this.txtStartTransit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AutoIncrStartValueKeyPressedEvent);
            // 
            // txtStartOnUS
            // 
            this.txtStartOnUS.Enabled = false;
            this.txtStartOnUS.Location = new System.Drawing.Point(714, 6);
            this.txtStartOnUS.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtStartOnUS.Name = "txtStartOnUS";
            this.txtStartOnUS.Size = new System.Drawing.Size(75, 20);
            this.txtStartOnUS.TabIndex = 2;
            this.txtStartOnUS.TextChanged += new System.EventHandler(this.AutoIncrStartAtChangedEvent);
            this.txtStartOnUS.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AutoIncrStartValueKeyDownEvent);
            this.txtStartOnUS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AutoIncrStartValueKeyPressedEvent);
            // 
            // txtStartAmount
            // 
            this.txtStartAmount.Enabled = false;
            this.txtStartAmount.Location = new System.Drawing.Point(926, 6);
            this.txtStartAmount.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtStartAmount.Name = "txtStartAmount";
            this.txtStartAmount.Size = new System.Drawing.Size(75, 20);
            this.txtStartAmount.TabIndex = 3;
            this.txtStartAmount.TextChanged += new System.EventHandler(this.AutoIncrStartAtChangedEvent);
            this.txtStartAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AutoIncrStartValueKeyDownEvent);
            this.txtStartAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AutoIncrStartValueKeyPressedEvent);
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNote.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblNote.Location = new System.Drawing.Point(2, 95);
            this.lblNote.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(482, 18);
            this.lblNote.TabIndex = 135;
            this.lblNote.Text = "Note: Auto Increment symbol \'#\' will be indicated with a blue background.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(293, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 136;
            this.label1.Text = " Start at:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(637, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 18);
            this.label2.TabIndex = 137;
            this.label2.Text = " Start at:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(849, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 18);
            this.label3.TabIndex = 138;
            this.label3.Text = " Start at:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(98, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 139;
            this.label4.Text = " Start at:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.displayFontLablel);
            this.groupBox1.Controls.Add(this.E13BRadioButton);
            this.groupBox1.Controls.Add(this.CMC7RadioButton);
            this.groupBox1.Location = new System.Drawing.Point(506, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 27);
            this.groupBox1.TabIndex = 143;
            this.groupBox1.TabStop = false;
            // 
            // CMC7RadioButton
            // 
            this.CMC7RadioButton.AutoSize = true;
            this.CMC7RadioButton.Location = new System.Drawing.Point(196, 8);
            this.CMC7RadioButton.Name = "CMC7RadioButton";
            this.CMC7RadioButton.Size = new System.Drawing.Size(54, 17);
            this.CMC7RadioButton.TabIndex = 141;
            this.CMC7RadioButton.Text = "CMC7";
            this.CMC7RadioButton.UseVisualStyleBackColor = true;
            this.CMC7RadioButton.CheckedChanged += new System.EventHandler(this.CMC7RadioButton_CheckedChanged);
            // 
            // E13BRadioButton
            // 
            this.E13BRadioButton.AutoSize = true;
            this.E13BRadioButton.Checked = true;
            this.E13BRadioButton.Location = new System.Drawing.Point(130, 8);
            this.E13BRadioButton.Name = "E13BRadioButton";
            this.E13BRadioButton.Size = new System.Drawing.Size(54, 17);
            this.E13BRadioButton.TabIndex = 140;
            this.E13BRadioButton.TabStop = true;
            this.E13BRadioButton.Text = "E-13B";
            this.E13BRadioButton.UseVisualStyleBackColor = true;
            this.E13BRadioButton.CheckedChanged += new System.EventHandler(this.CMC7RadioButton_CheckedChanged);
            // 
            // displayFontLablel
            // 
            this.displayFontLablel.AutoSize = true;
            this.displayFontLablel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayFontLablel.Location = new System.Drawing.Point(17, 7);
            this.displayFontLablel.Name = "displayFontLablel";
            this.displayFontLablel.Size = new System.Drawing.Size(107, 18);
            this.displayFontLablel.TabIndex = 142;
            this.displayFontLablel.Text = "Display Font:";
            // 
            // MICRIDControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNote);
            this.Controls.Add(this.txtStartAmount);
            this.Controls.Add(this.txtStartOnUS);
            this.Controls.Add(this.txtStartTransit);
            this.Controls.Add(this.lblStartingCheckAmt);
            this.Controls.Add(this.txtStartAuxOnUS);
            this.Controls.Add(this.lblAmountField);
            this.Controls.Add(this.lblOnUSField);
            this.Controls.Add(this.lblTransitField);
            this.Controls.Add(this.lblAuxOnUSField);
            this.Controls.Add(this.lblNumbering);
            this.Controls.Add(this.txt44);
            this.Controls.Add(this.txt46);
            this.Controls.Add(this.txt45);
            this.Controls.Add(this.txt47);
            this.Controls.Add(this.txt49);
            this.Controls.Add(this.txt48);
            this.Controls.Add(this.txt50);
            this.Controls.Add(this.txt52);
            this.Controls.Add(this.txt51);
            this.Controls.Add(this.txt53);
            this.Controls.Add(this.txt54);
            this.Controls.Add(this.txt14);
            this.Controls.Add(this.txt13);
            this.Controls.Add(this.txt15);
            this.Controls.Add(this.txt17);
            this.Controls.Add(this.txt16);
            this.Controls.Add(this.txt18);
            this.Controls.Add(this.txt20);
            this.Controls.Add(this.txt19);
            this.Controls.Add(this.txt21);
            this.Controls.Add(this.txt23);
            this.Controls.Add(this.txt22);
            this.Controls.Add(this.txt24);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.txt6);
            this.Controls.Add(this.txt8);
            this.Controls.Add(this.txt7);
            this.Controls.Add(this.txt9);
            this.Controls.Add(this.txt11);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt12);
            this.Controls.Add(this.txt26);
            this.Controls.Add(this.txt25);
            this.Controls.Add(this.txt27);
            this.Controls.Add(this.txt29);
            this.Controls.Add(this.txt28);
            this.Controls.Add(this.txt30);
            this.Controls.Add(this.txt32);
            this.Controls.Add(this.txt31);
            this.Controls.Add(this.txt33);
            this.Controls.Add(this.txt35);
            this.Controls.Add(this.txt34);
            this.Controls.Add(this.txt36);
            this.Controls.Add(this.txt38);
            this.Controls.Add(this.txt37);
            this.Controls.Add(this.txt39);
            this.Controls.Add(this.txt41);
            this.Controls.Add(this.txt40);
            this.Controls.Add(this.txt42);
            this.Controls.Add(this.txt55);
            this.Controls.Add(this.txt43);
            this.Controls.Add(this.txt56);
            this.Controls.Add(this.txt58);
            this.Controls.Add(this.txt57);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "MICRIDControl";
            this.Size = new System.Drawing.Size(1008, 123);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private IContainer components;
        private Label lblAmountField;
        private Label lblOnUSField;
        private Label lblTransitField;
        private Label lblAuxOnUSField;
        private Label lblNumbering;
        private TextBox txt44;
        private TextBox txt46;
        private TextBox txt45;
        private TextBox txt47;
        private TextBox txt49;
        private TextBox txt48;
        private TextBox txt50;
        private TextBox txt52;
        private TextBox txt51;
        private TextBox txt53;
        private TextBox txt54;
        private TextBox txt14;
        private TextBox txt13;
        private TextBox txt15;
        private TextBox txt17;
        private TextBox txt16;
        private TextBox txt18;
        private TextBox txt20;
        private TextBox txt19;
        private TextBox txt21;
        private TextBox txt23;
        private TextBox txt22;
        private TextBox txt24;
        private TextBox txt2;
        private TextBox txt1;
        private TextBox txt3;
        private TextBox txt5;
        private TextBox txt4;
        private TextBox txt6;
        private TextBox txt8;
        private TextBox txt7;
        private TextBox txt9;
        private TextBox txt11;
        private TextBox txt10;
        private TextBox txt12;
        private TextBox txt26;
        private TextBox txt25;
        private TextBox txt27;
        private TextBox txt29;
        private TextBox txt28;
        private TextBox txt30;
        private TextBox txt32;
        private TextBox txt31;
        private TextBox txt33;
        private TextBox txt35;
        private TextBox txt34;
        private TextBox txt36;
        private TextBox txt38;
        private TextBox txt37;
        private TextBox txt39;
        private TextBox txt41;
        private TextBox txt40;
        private TextBox txt42;
        private TextBox txt55;
        private TextBox txt43;
        private TextBox txt56;
        private TextBox txt58;
        private TextBox txt57;
        private TextBox txtStartAuxOnUS;
        private Label lblStartingCheckAmt;
        private TextBox txtStartTransit;
        private TextBox txtStartOnUS;
        private TextBox txtStartAmount;
        private Label lblNote;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private GroupBox groupBox1;
        private Label displayFontLablel;
        private RadioButton E13BRadioButton;
        private RadioButton CMC7RadioButton;
    }
}
