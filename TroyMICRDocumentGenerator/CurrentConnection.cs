﻿using System;

namespace TroyMICRDocumentGenerator
{
    public class CurrentConnection
    {
        SocketCommunications socket = null;
        string _printQueueName = String.Empty;

        Boolean usingIpConnection = false;

        String currentInitializedSocketAddr = String.Empty;

        private static CurrentConnection instance;
        public static CurrentConnection Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CurrentConnection();
                }
                return instance;
            }
        }

        public bool isBidiAvaibable()
        {
            return socket.BidiAvailable;
        }

        public String GetDebugInfo()
        {
            try
            {
                string retstring;

                if (usingIpConnection)
                {
                    retstring = "Using IP Address.  ";
                    if (socket != null)
                    {
                        retstring += " Is Connected: " + socket.client.Connected.ToString() + " ";
                        retstring += " Connected Success: " + socket.ConnectedSuccessfully.ToString() + " ";
                        retstring += " Bidi Avail: " + socket.BidiAvailable.ToString() + " ";
                        retstring += " Last Error: " + socket.LatestError;
                    }
                    else
                    {
                        retstring += "socket is Null. ";
                    }
                }
                else
                {
                    retstring = "Using Print Queue.  ";
                    retstring = "Queue Name: " + _printQueueName;
                }

                return retstring;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetDebugInfo), nameof(CurrentConnection));
                return string.Empty;
            }
        }

        public void SetCurrentPrintQueueConnection(string printQueueName)
        {
            try
            {
                usingIpConnection = false;
                _printQueueName = printQueueName;
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetCurrentPrintQueueConnection), nameof(CurrentConnection));
            }
        }

        public bool SetCurrentIpConnection(string ipAddress, int ipPort, out string FailMessage)
        {
            try
            {
                usingIpConnection = true;
                FailMessage = String.Empty;
                bool retValue = true;
                socket = new SocketCommunications(ipAddress, ipPort);
                if (currentInitializedSocketAddr != ipAddress || socket.client == null || !socket.client.Connected)
                {
                    if (!socket.Initialize())
                    {
                        FailMessage = "Printer initialize failed.";
                        retValue = false;
                    }
                    else
                    {
                        currentInitializedSocketAddr = ipAddress;
                    }
                }
                return retValue;
            }
            catch(Exception ex)
            {
                FailMessage = ex.Message;
                return false;
            }
        }

        public Boolean ResetCurrentIpConnection()
        {
            try
            {
                if (socket != null)
                {
                    return socket.ResetConnection();
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ResetCurrentIpConnection), nameof(CurrentConnection));
                return false;
            }
        }

        public void CloseCurrentIpConnection()
        {
            try
            {
                if (socket != null)
                {
                    socket.Disconnect();
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(CloseCurrentIpConnection), nameof(CurrentConnection));
            }
        }

        public bool SendFile(string FileName)
        {
            bool retValue = true;
            try
            {
                if (usingIpConnection)
                {
                    retValue = socket.SendFile(FileName);
                }
                else
                {
                    PrintToSpooler.SendFileToPrinter(_printQueueName, FileName, "Troy DVT Test");
                }

                return retValue;
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(CurrentConnection), nameof(SendFile));
                return retValue;
            }
        }

        public bool SendFileBidi(string Filename, out string response)
        {
            try
            {
                return socket.SendFileBidi(Filename, out response);
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(CurrentConnection), nameof(SendFile));
                response = null;
                return false;
            }
        }
    }
}
