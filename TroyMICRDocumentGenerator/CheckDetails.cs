﻿namespace TroyMICRDocumentGenerator
{
    public class CheckDetails
    {
        public string PrintDate;
        public string PrintTime;
        public string PrinterModel;
        public string PrinterSerialNumber;
        public string MICRFontPrinted;
        public string TonerCartrideType;
        public string BIASVoltage;
        public string FileName;
        public string BatchId;
        public string AppVersion;
        public string FontID;
        public string FontName;
        public string FontHeight;
        public int MICRLeftOffset;
        public int MICRTopOffset;
        public int TotalChecks;
        public int ChecksPerPage;
        public string[] CheckMICR;
        public int[] YOffset;
        public int PaperSize;
    }
}
