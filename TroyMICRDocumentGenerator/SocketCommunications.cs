﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Timers;

namespace TroyMICRDocumentGenerator
{
    public class SocketCommunications
    {
        System.Timers.Timer keepPortOpen;
        private string _ipAddress;
        int _port;
        private IPAddress ipAddr = null;
        public Socket client = null;
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent disconnectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);

        private IPEndPoint remoteEP = null;
        IPHostEntry ipHostEntry = null;
        public bool DeviceFound { get; set; }
        public bool BidiAvailable { get; set; }
        public bool ConnectedSuccessfully { get; set; }

        public string LatestError { get; set; }
        public string ConnectionStatus { get; set; }
        public string BidiStatus { get; set; }
        public static string Response = string.Empty;

        public SocketCommunications(String IpAddress, int Port)
        {
            _ipAddress = IpAddress;
            _port = Port;
        }

        public Boolean ResetConnection()
        {
            try
            {
                keepPortOpen.Enabled = false;
                Disconnect();
                client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                Thread.Sleep(2000);
                connectDone.WaitOne();

                if (client.Connected)
                {
                    ConnectedSuccessfully = true;
                    return true;

                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ResetConnection), nameof(SocketCommunications));
                return false;
            }
        }

        public Boolean Initialize(Boolean ForTest = false)
        {
            Boolean retValue = true;
            LatestError = String.Empty;

            try
            {
                ipHostEntry = Dns.Resolve(_ipAddress);

                List<IPAddress> ipv4Address = ipHostEntry.AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork).ToList();
                ipAddr = ipv4Address[0];

                if (ipAddr != null)
                {
                    remoteEP = new IPEndPoint(ipAddr, _port);
                    client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                    Thread.Sleep(2000);
                    connectDone.WaitOne();

                    if (client.Connected)
                    {
                        ConnectedSuccessfully = true;
                        DeviceFound = PingAddress();

                        if (DeviceFound)
                        {
                            BidiAvailable = CheckBidi();
                            if (!BidiAvailable)
                            {
                                return false;
                            }
                        }
                        if (!ForTest)
                        {
                            keepPortOpen = new System.Timers.Timer(60000);
                            keepPortOpen.Elapsed += new ElapsedEventHandler(OnKeepSocketAlive);
                            keepPortOpen.Enabled = true;
                        }
                    }
                    else
                    {
                        ConnectedSuccessfully = false;
                        DeviceFound = false;
                        BidiAvailable = false;
                    }
                }
                else
                {
                    LatestError = "IP Address was not found in the list of Host Entry Addresses";
                    DeviceFound = false;
                    retValue = false;
                }
                return retValue;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Initialize), nameof(SocketCommunications));
                LatestError = ex.Message;
                DeviceFound = false;
                return false;
            }
        }

        private void OnKeepSocketAlive(object sender, ElapsedEventArgs e)
        {
            SendString("\u001B%-12345X@PJL\u000A@PJL ENTER LANGUAGE=PCL\u000A\u000D\u001BE\u001B%-12345X@PJL EOJ\u000A\u001B%-12345X");
        }

        private Boolean PingAddress()
        {
            try
            {
                if (ipAddr != null)
                {
                    Ping ping = new Ping();
                    PingReply pingresult = ping.Send(ipAddr);
                    if (pingresult.Status.ToString() == "Success")
                    {

                        return true;
                    }
                    else
                    {
                        ConnectionStatus = "Ping failed";
                        return false;
                    }
                }
                else
                {
                    ConnectionStatus = "Ping failed";
                    return false;
                }
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(PingAddress), nameof(SocketCommunications));
                return false;
            }
        }

        private Boolean CheckBidi()
        {
            try
            {
                sendDone.Reset();
                Send(client, "@PJL ECHO TEST\u000A\u001B%-12345X");
                sendDone.WaitOne();
                receiveDone.Reset();
                AutoResetEvent autoEvent = new AutoResetEvent(false);
                System.Threading.Timer bidiResponseTimer = new System.Threading.Timer(OnBidiResponse, autoEvent, 10000, 10000);
                Receive(client);
                receiveDone.WaitOne();
                autoEvent.Set();
                bidiResponseTimer.Dispose();
                autoEvent.Close();

                if (String.IsNullOrEmpty(Response))
                {
                    ConnectionStatus = "No Echo Received";
                    BidiStatus = "No Response";
                    BidiAvailable = false;
                    return false;
                }
                else
                {
                    ConnectionStatus = "Connected";

                    Response = String.Empty;
                    sendDone.Reset();
                    Send(client, "@PJL TROY CFG=50103\u000A\u001B%-12345X");
                    sendDone.WaitOne();

                    receiveDone.Reset();
                    autoEvent = new AutoResetEvent(false);
                    bidiResponseTimer = new System.Threading.Timer(OnBidiResponse, autoEvent, 10000, 10000);
                    Receive(client);
                    receiveDone.WaitOne();
                    autoEvent.Set();
                    bidiResponseTimer.Dispose();
                    autoEvent.Close();

                    if (String.IsNullOrEmpty(Response))
                    {
                        BidiAvailable = false;
                        BidiStatus = "Not Available";
                        return false;
                    }
                    else
                    {
                        if (Response.ToUpper().Contains("NOT IMPLEMENTED"))
                        {
                            BidiAvailable = false;
                            BidiStatus = "Feature Bit Disabled";
                            return false;
                        }
                        else
                        {
                            BidiAvailable = true;
                            BidiStatus = "Available";
                            return true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(CheckBidi), nameof(SocketCommunications));
                return false;
            }
        }

        private void OnBidiResponse(object state)
        {
            try
            {
                AutoResetEvent autoEvent = (AutoResetEvent)state;
                autoEvent.Set();
                receiveDone.Set();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(OnBidiResponse), nameof(SocketCommunications));
            }
        }


        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket con = (Socket)ar.AsyncState;
                con.EndConnect(ar);
                connectDone.Set();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ConnectCallback), nameof(SocketCommunications));
            }
        }

        private static void SendSynchoronous(Socket client, String data)
        {
            try
            {
                // Convert the string data to byte data using ASCII encoding.  
                byte[] byteData = Encoding.ASCII.GetBytes(data);

                int bytesSent = client.Send(byteData);
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(SendSynchoronous), nameof(SocketCommunications));
            }
        }

        private static void Send(Socket client, String data)
        {
            try
            {
                // Convert the string data to byte data using ASCII encoding.  
                byte[] byteData = Encoding.ASCII.GetBytes(data);

                // Begin sending the data to the remote device.  
                client.BeginSend(byteData, 0, byteData.Length, 0,
                    new AsyncCallback(SendCallback), client);
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(Send), nameof(SocketCommunications));
            }
        }

        public bool SendString(string DataToSend)
        {
            try
            {
                long dataBytesCount = DataToSend.Length;
                byte[] bytesToSend = Encoding.ASCII.GetBytes(DataToSend);
                int startIndex = 0;
                while (dataBytesCount > 0)
                {

                    if (dataBytesCount > client.SendBufferSize)
                    {
                        client.BeginSend(bytesToSend, startIndex, client.SendBufferSize, 0, new AsyncCallback(SendCallback), client);
                        startIndex += client.SendBufferSize;
                        dataBytesCount -= client.SendBufferSize;
                    }
                    else
                    {
                        client.BeginSend(bytesToSend, startIndex, (int)dataBytesCount, 0, new AsyncCallback(SendCallback), client);
                        dataBytesCount = 0;
                    }
                    sendDone.WaitOne();
                }
                return true;

            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SendString), nameof(SocketCommunications));
                LatestError = ex.Message;
                return false;
            }
        }

        public bool SendFile(string FileNameWithPath)
        {
            try
            {
                if (keepPortOpen != null)
                {
                    keepPortOpen.Enabled = false;
                }

                FileStream sendFileStream = new FileStream(FileNameWithPath, FileMode.Open, FileAccess.Read);
                BinaryReader binReader = new BinaryReader(sendFileStream);
                long dataBytesCount = sendFileStream.Length;
                byte[] bytesToSend = new Byte[sendFileStream.Length];
                bytesToSend = binReader.ReadBytes(Convert.ToInt32(sendFileStream.Length));

                int startIndex = 0;
                while (dataBytesCount > 0)
                {
                    if (dataBytesCount > client.SendBufferSize)
                    {
                        client.BeginSend(bytesToSend, startIndex, client.SendBufferSize, 0, new AsyncCallback(SendCallback), client);
                        startIndex += client.SendBufferSize;
                        dataBytesCount -= client.SendBufferSize;
                    }
                    else
                    {
                        client.BeginSend(bytesToSend, startIndex, (int)dataBytesCount, 0, new AsyncCallback(SendCallback), client);
                        dataBytesCount = 0;
                    }
                    sendDone.WaitOne();
                }
                sendFileStream.Close();
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SendFile), nameof(SocketCommunications));
                LatestError = ex.Message;
                return false;
            }
            finally
            {
                if (keepPortOpen != null)
                {
                    keepPortOpen.Enabled = true;
                }
            }
        }

        public bool SendFileBidi(string FileNameWithPath, out string response)
        {
            response = "";
            try
            {
                if (keepPortOpen != null)
                {
                    keepPortOpen.Enabled = false;
                }

                FileStream sendFileStream = new FileStream(FileNameWithPath, FileMode.Open, FileAccess.Read);
                BinaryReader binReader = new BinaryReader(sendFileStream);
                long dataBytesCount = sendFileStream.Length;
                byte[] bytesToSend = new Byte[sendFileStream.Length];
                bytesToSend = binReader.ReadBytes(Convert.ToInt32(sendFileStream.Length));

                sendDone.Reset();
                client.BeginSend(bytesToSend, 0, bytesToSend.Length, 0,
                    new AsyncCallback(SendCallback), client);
                sendDone.WaitOne();
                Boolean rxedBidi = false;
                while (!rxedBidi)
                {
                    receiveDone.Reset();
                    AutoResetEvent autoEvent = new AutoResetEvent(false);
                    System.Threading.Timer bidiResponseTimer = new System.Threading.Timer(OnBidiResponse, autoEvent, 10000, 10000);
                    Receive(client);
                    receiveDone.WaitOne();
                    if (Response.StartsWith("@PJL ECHO TROY CFG="))
                    {
                        rxedBidi = true;
                    }
                    autoEvent.Set();
                    bidiResponseTimer.Dispose();
                    autoEvent.Close();

                }

                if (String.IsNullOrEmpty(Response))
                {
                    response = "Bidi Not Available";
                    return false;
                }
                else
                {
                    if (Response.ToUpper().Contains("NOT IMPLEMENTED"))
                    {
                        response = "Bidi Feature Bit Disabled";
                        return false;
                    }
                    else
                    {
                        response = Response;
                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SendFileBidi), nameof(SocketCommunications));
                response = ex.Message;
                return false;
            }
            finally
            {
                keepPortOpen.Enabled = true;
            }
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket snd = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = snd.EndSend(ar);

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SendCallback), nameof(SocketCommunications));
            }
        }


        private static void Receive(Socket client)
        {
            try
            {
                // Create the state object.  
                ReceiveObject state = new ReceiveObject()
                {
                    rxSocket = client
                };

                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.buffer, 0, ReceiveObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Receive), nameof(SocketCommunications));
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                ReceiveObject state = (ReceiveObject)ar.AsyncState;
                Socket rx = state.rxSocket;

                // Read data from the remote device.  
                int bytesRead = rx.EndReceive(ar);


                if (bytesRead > 0)   //Not seeing the case where 0 bytes is returned.  I think its because the printer is not closing the connection
                {
                    // There might be more data, so store the data received so far.  
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                    Response = state.sb.ToString();


                    if (bytesRead >= state.buffer.Length)
                    {
                        // Get the rest of the data.                      
                        rx.BeginReceive(state.buffer, 0, ReceiveObject.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), state);
                    }
                    else
                    {
                        receiveDone.Set();
                    }
                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (state.sb.Length > 1)
                    {
                        Response = state.sb.ToString();
                    }
                    // Signal that all bytes have been received.  
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Logger.LogMessage(e, nameof(Receive), nameof(SocketCommunications));
                String temp = e.Message;
            }
        }


        public void Disconnect()
        {
            try
            {
                if (client != null && client.Connected)
                {
                    if (keepPortOpen != null)
                        keepPortOpen.Enabled = false;
                    client.Shutdown(SocketShutdown.Both);
                    client.BeginDisconnect(true, new AsyncCallback(DisonnectCallback), client);
                    disconnectDone.WaitOne();

                    if (client.Connected)
                    {
                        string temp = "Something went wrong";
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(Disconnect), nameof(SocketCommunications));
            }
        }

        private void DisonnectCallback(IAsyncResult ar)
        {
            try
            {
                Thread.Sleep(2000);
                Socket client = (Socket)ar.AsyncState;
                client.EndDisconnect(ar);
                disconnectDone.Set();
                client.Close();
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex, nameof(DisonnectCallback), nameof(SocketCommunications));
            }
        }
    }

    public class ReceiveObject
    {
        public Socket rxSocket = null;

        public const int BufferSize = 256;

        public byte[] buffer = new byte[BufferSize];

        public StringBuilder sb = new StringBuilder();
    }

}
