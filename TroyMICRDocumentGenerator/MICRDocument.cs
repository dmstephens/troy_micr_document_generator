﻿using System.Drawing.Printing;

namespace TroyMICRDocumentGenerator
{
    public class MICRDocument : PrintDocument
    {
        private int _currPageNumber;
        private int _totalPages;
        private CheckDetails _checkMICRDetails;

        public int CurrPageNumber
        {
            get => this._currPageNumber;
            set => this._currPageNumber = value;
        }

        public int TotalPages
        {
            get => this._totalPages;
            set => this._totalPages = value;
        }

        public CheckDetails CheckDetail => this._checkMICRDetails;

        public MICRDocument(int totalPages, CheckDetails checkDetails)
        {
            this._totalPages = totalPages;
            this._checkMICRDetails = checkDetails;
        }
    }
}
