﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Management.Automation;
using System.Printing;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace TroyMICRDocumentGenerator
{
    public partial class TroyMICRDocGenerator : Form
    {
        private const int _check1MacroId = 100;
        private const int _check2MacroId = 101;
        private const int _check3MacroId = 102;
        private const int _check4MacroId = 103;
        private const int _rectMacroId = 104;
        private const int _pageHeightLetter = 3185;
        private const int _pageHeightA4 = 3392;
        private const int _checkHeight3UP = 882;
        private const int _checkHeight4UP = 619;
        private Hashtable _fontTable = new Hashtable();
        private bool _newJob = true;
        private bool _jobChanged;
        private static string _appPath;
        private IntPtr _hPrinter = IntPtr.Zero;
        private float _pageWidthIn;
        private float _pageXIn;
        private float _pageYIn;
        private static int _pagesPrinted;
        private System.Timers.Timer _printTimer = new System.Timers.Timer();
        private bool _initFlag;
        private string _paperSource;
        private bool ContinuePrinting = false;
        static int PrintCount = 0;
        private string currentPrinter = String.Empty;
        private CancellationTokenSource _canceller;
        string printerName = string.Empty;
        string selectedItem = string.Empty;
        string fontPath = string.Empty;
        bool milisecondsUpdating = false;
        bool secondsUpdating = false;
        bool waitValue = false;
        bool printInProgress = false;
        
        private string[] PaperSizeValues = new string[2]
        {
      "Letter",
      "A4"
        };
        private string[] PaperSourceValues = new string[3]
        {
      "Automatic",
      "Tray 1",
      "Tray 2"
        };
        private string[] PaperOutputValues = new string[3]
        {
      "Automatic",
      "Tray1",
      "Tray2"
        };
        private int PaperSizeDefault;
        private int PaperOutputDefault;

        public static string ApplicationPath => TroyMICRDocGenerator._appPath;

        public TroyMICRDocGenerator()
        {
            try
            {
                this.InitializeComponent();
                this.CreateAppFolders();
                this.SetupToolTip();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "Constructor", nameof(TroyMICRDocGenerator));
            }
        }

        private void SetupToolTip()
        {
            var yourToolTip = new ToolTip();
            //The below are optional, of course,

            yourToolTip.ToolTipIcon = ToolTipIcon.Info;
            yourToolTip.IsBalloon = true;
            yourToolTip.ShowAlways = true;

            yourToolTip.SetToolTip(cbMICRSourceFontEscapeSequence, @"Enter the escape character of the font. <esc> is used in place of the escape character. For example you may enter ""<esc>(0Q<esc>(s1p12.0v0s0b1T"" ");
        }

        private void ApplicationFormLoadEvent(object sender, EventArgs e)
        {
            try
            {
                Logger.LogMessage("***************************************Troy MICR Document Generator Starting*******************************");
                this.micrIDCtrl.MICRIdChanged += new MicrIdControlChangedDelegate(this.MicrIdControlChanged);
                this.InitPrinterTab();
                this.InitCheckTab();
                this._jobChanged = false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ApplicationFormLoadEvent), nameof(TroyMICRDocGenerator));
            }
        }

        private void MicrIdControlChanged() => this._jobChanged = true;

        private void CreateAppFolders()
        {
            try
            {
                string str1 = string.Empty;
                string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                string str2 = !directoryName.StartsWith("file:\\") ? directoryName : directoryName.Substring(6);
                TroyMICRDocGenerator._appPath = str2;
                string path1 = str2 + "\\JOBS";
                if (!Directory.Exists(path1))
                {
                    Logger.LogMessage("Create Job Folder " + path1);
                    Directory.CreateDirectory(path1);
                }
                string path2 = str2 + "\\MICRFONT";
                if (!Directory.Exists(path2))
                {
                    Logger.LogMessage("Create MICR Font Folder " + path1);
                    Directory.CreateDirectory(path2);
                }
                str1 = (string)null;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "Constructor", nameof(TroyMICRDocGenerator));
            }
        }

        private void NewToolbarButtonClicked(object sender, EventArgs e)
        {
            try
            {
                if (this._jobChanged && MessageBox.Show("Job properties have changed. Do you want to continue without saving?", "Troy MICR Document Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                Logger.LogMessage("NEW Toolbar Button Clicked. Clearing up all the form controls");
                this.InitPrinterTabControls();
                this.InitCheckTabControls();
                this._newJob = true;
                this._jobChanged = false;
                this.Text = "Troy MICR Document Generator - New";
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(NewToolbarButtonClicked), nameof(TroyMICRDocGenerator));
            }
        }

        private void OpenToolbarButtonClicked(object sender, EventArgs e)
        {
            try
            {
                JobDetails jobDetails1 = (JobDetails)null;
                if (this._jobChanged && MessageBox.Show("Job properties have changed. Do you want to continue without saving?", "Troy MICR Document Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                JobList jobList = new JobList();
                int num = (int)jobList.ShowDialog();
                if (jobList.DialogResult == DialogResult.OK && jobList.jobFile != null)
                {
                    Logger.LogMessage("OPEN Toolbar Button Clicked. Reading the Job File " + jobList.jobFile);
                    JobDetails jobDetails2 = this.ReadJobFile(jobList.jobFile);
                    if (jobDetails2 != null)
                    {
                        this.DisplayJobFileValuesOnPrinterTab(jobDetails2);
                        this.DisplayJobFileValuesOnCheckTab(jobDetails2);
                        this._newJob = false;
                        this._jobChanged = false;
                        this.Text = "Troy MICR Document Generator - " + jobDetails2.JobName;
                        jobDetails1 = (JobDetails)null;
                    }
                }
                jobList.Dispose();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(OpenToolbarButtonClicked), nameof(TroyMICRDocGenerator));
            }
        }

        private JobDetails ReadJobFile(string jobFile)
        {
            try
            {
                if (!File.Exists(jobFile))
                    return (JobDetails)null;
                XmlDocument xmlDocument = new XmlDocument();
                try
                {
                    xmlDocument.Load(jobFile);
                }
                catch (Exception ex)
                {
                    int num = (int)MessageBox.Show("The Job file is not a valid file. Please select a valid job file.", "Troy MICR Document Generator");
                    return (JobDetails)null;
                }
                JobDetails jobDetails = new JobDetails();
                XmlNode xmlNode1 = xmlDocument.SelectSingleNode("//MICRJob/JobName");
                if (xmlNode1 != null)
                    jobDetails.JobName = xmlNode1.InnerText;
                XmlNode xmlNode2 = xmlDocument.SelectSingleNode("//MICRJob/BatchId");
                if (xmlNode2 != null)
                    jobDetails.BatchId = xmlNode2.InnerText;
                XmlNode xmlNode3 = xmlDocument.SelectSingleNode("//MICRJob/NoOfChecks");
                if (xmlNode3 != null)
                    jobDetails.NoOfChecks = int.Parse(xmlNode3.InnerText);
                XmlNode xmlNode4 = xmlDocument.SelectSingleNode("//MICRJob/TotalChecks");
                jobDetails.TotalChecks = xmlNode4 == null ? 0 : int.Parse(xmlNode4.InnerText);
                XmlNode xmlNode5 = xmlDocument.SelectSingleNode("//MICRJob/Printer/Name");
                if (xmlNode5 != null)
                    jobDetails.PrinterName = xmlNode5.InnerText;
                XmlNode xmlNode6 = xmlDocument.SelectSingleNode("//MICRJob/Printer/Model");
                if (xmlNode6 != null)
                    jobDetails.PrinterModel = xmlNode6.InnerText;
                XmlNode xmlNode7 = xmlDocument.SelectSingleNode("//MICRJob/Printer/SerialNumber");
                if (xmlNode7 != null)
                    jobDetails.PrinterSerialNo = xmlNode7.InnerText;
                XmlNode xmlNode8 = xmlDocument.SelectSingleNode("//MICRJob/Printer/PaperSize");
                if (xmlNode8 != null)
                    jobDetails.PaperSize = xmlNode8.InnerText;
                XmlNode xmlNode9 = xmlDocument.SelectSingleNode("//MICRJob/Printer/PaperSource");
                if (xmlNode9 != null)
                    jobDetails.PaperSource = xmlNode9.InnerText;
                XmlNode xmlNode10 = xmlDocument.SelectSingleNode("//MICRJob/Printer/PaperOutput");
                if (xmlNode10 != null)
                    jobDetails.PaperOutput = xmlNode10.InnerText;
                XmlNode xmlNode11 = xmlDocument.SelectSingleNode("//MICRJob/Printer/TonerType");
                if (xmlNode11 != null)
                    jobDetails.TonerType = xmlNode11.InnerText;
                XmlNode xmlNode12 = xmlDocument.SelectSingleNode("//MICRJob/Printer/Density");
                if (xmlNode12 != null)
                    jobDetails.Density = xmlNode12.InnerText;
                XmlNode xmlNode13 = xmlDocument.SelectSingleNode("//MICRJob/Printer/PrintOptions");
                if (xmlNode13 != null)
                    jobDetails.PrintOption = int.Parse(xmlNode13.InnerText);
                XmlNode xmlNode14 = xmlDocument.SelectSingleNode("//MICRJob/Printer/YOffset1");
                if (xmlNode14 != null)
                    jobDetails.YOffset1 = float.Parse(xmlNode14.InnerText);
                XmlNode xmlNode15 = xmlDocument.SelectSingleNode("//MICRJob/Printer/YOffset2");
                if (xmlNode15 != null)
                    jobDetails.YOffset2 = float.Parse(xmlNode15.InnerText);
                XmlNode xmlNode16 = xmlDocument.SelectSingleNode("//MICRJob/Printer/YOffset3");
                if (xmlNode16 != null)
                    jobDetails.YOffset3 = float.Parse(xmlNode16.InnerText);
                XmlNode xmlNode17 = xmlDocument.SelectSingleNode("//MICRJob/Printer/YOffset4");
                if (xmlNode17 != null)
                    jobDetails.YOffset4 = float.Parse(xmlNode17.InnerText);
                XmlNode xmlNode18 = xmlDocument.SelectSingleNode("//MICRJob/MICRFont/FontOptions");
                if (xmlNode18 != null)
                    jobDetails.FontOption = int.Parse(xmlNode18.InnerText);
                XmlNode xmlNode19 = xmlDocument.SelectSingleNode("//MICRJob/MICRFont/Id");
                if (xmlNode19 != null)
                    jobDetails.FontId = xmlNode19.InnerText;
                XmlNode xmlNode20 = xmlDocument.SelectSingleNode("//MICRJob/MICRFont/Name");
                if (xmlNode20 != null)
                    jobDetails.FontName = xmlNode20.InnerText;
                XmlNode xmlNode21 = xmlDocument.SelectSingleNode("//MICRJob/MICRFont/File");
                if (xmlNode21 != null)
                    jobDetails.FontFile = xmlNode21.InnerText;
                XmlNode xmlNode22 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/AuxOnUSField");
                if (xmlNode22 != null)
                    jobDetails.AuxOnUSField = xmlNode22.InnerText;
                XmlNode xmlNode23 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/TransitNoField");
                if (xmlNode23 != null)
                    jobDetails.TransitField = xmlNode23.InnerText;
                XmlNode xmlNode24 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/OnUSField");
                if (xmlNode24 != null)
                    jobDetails.OnUSField = xmlNode24.InnerText;
                XmlNode xmlNode25 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/AmountField");
                if (xmlNode25 != null)
                    jobDetails.AmountField = xmlNode25.InnerText;
                XmlNode xmlNode26 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtAuxOnUSField");
                if (xmlNode26 != null)
                    jobDetails.StartAtAuxOnUSField = int.Parse(xmlNode26.InnerText);
                XmlNode xmlNode27 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtTransitField");
                if (xmlNode27 != null)
                    jobDetails.StartAtTransitField = int.Parse(xmlNode27.InnerText);
                XmlNode xmlNode28 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtOnUSField");
                if (xmlNode28 != null)
                    jobDetails.StartAtOnUSField = int.Parse(xmlNode28.InnerText);
                XmlNode xmlNode29 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtAmountField");
                if (xmlNode29 != null)
                    jobDetails.StartAtAmountField = int.Parse(xmlNode29.InnerText);
                XmlNode xmlNode30 = xmlDocument.SelectSingleNode("//MICRJob/MICROffset/LeftMargin");
                if (xmlNode30 != null)
                    jobDetails.LeftMargin = double.Parse(xmlNode30.InnerText);
                XmlNode xmlNode31 = xmlDocument.SelectSingleNode("//MICRJob/MICROffset/TopMargin");
                if (xmlNode31 != null)
                    jobDetails.TopMargin = double.Parse(xmlNode31.InnerText);
                return jobDetails;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ReadJobFile), nameof(TroyMICRDocGenerator));
                return (JobDetails)null;
            }
        }

        private void SaveToolbarButtonClicked(object sender, EventArgs e)
        {
            try
            {
                if (!this.ValidateUserSettings())
                    return;
                JobDetails jobDetail = new JobDetails();
                this.GetPrinterTabUserSettings(ref jobDetail);
                this.GetCheckTabUserSettings(ref jobDetail);
                if (this.SaveJobDetails(jobDetail))
                {
                    this._jobChanged = false;
                    this._newJob = false;
                    Logger.LogMessage("SAVE Toolbar Button Clicked. Saving the Job " + jobDetail.JobName + "  at " + TroyMICRDocGenerator._appPath + "\\JOBS");
                    this.Text = "Troy MICR Document Generator - " + jobDetail.JobName;
                }
                jobDetail = (JobDetails)null;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SaveToolbarButtonClicked), nameof(TroyMICRDocGenerator));
            }
        }

        private bool SaveJobDetails(JobDetails jobDetails)
        {
            try
            {
                string str1 = TroyMICRDocGenerator._appPath + "\\JOBS";
                string str2 = jobDetails.JobName.EndsWith(".job", StringComparison.CurrentCultureIgnoreCase) ? str1 + "\\" + jobDetails.JobName : str1 + "\\" + jobDetails.JobName + ".job";
                if (File.Exists(str2))
                {
                    if (this._newJob && MessageBox.Show("A job with the name " + jobDetails.JobName + " already exists. It will overwrite the existing job file. Do you want to continue?", "Troy MICR Document Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
                        return false;
                    File.Delete(str2);
                }
                XmlWriter xmlWriter = XmlWriter.Create(str2, new XmlWriterSettings()
                {
                    Indent = true,
                    ConformanceLevel = ConformanceLevel.Document
                });
                xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                xmlWriter.WriteStartElement("MICRJob");
                xmlWriter.WriteStartElement("JobName");
                xmlWriter.WriteString(jobDetails.JobName);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("BatchId");
                xmlWriter.WriteString(jobDetails.BatchId);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("NoOfChecks");
                xmlWriter.WriteValue(jobDetails.NoOfChecks);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("TotalChecks");
                xmlWriter.WriteValue(jobDetails.TotalChecks);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Printer");
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString(jobDetails.PrinterName);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Model");
                xmlWriter.WriteString(jobDetails.PrinterModel);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("SerialNumber");
                xmlWriter.WriteString(jobDetails.PrinterSerialNo);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("PaperSize");
                xmlWriter.WriteString(jobDetails.PaperSize);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("PaperSource");
                xmlWriter.WriteString(jobDetails.PaperSource);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("PaperOutput");
                xmlWriter.WriteString(jobDetails.PaperOutput);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("TonerType");
                xmlWriter.WriteString(jobDetails.TonerType);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Density");
                xmlWriter.WriteString(jobDetails.Density);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("PrintOptions");
                xmlWriter.WriteValue(jobDetails.PrintOption);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("YOffset1");
                xmlWriter.WriteValue(jobDetails.YOffset1);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("YOffset2");
                xmlWriter.WriteValue(jobDetails.YOffset2);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("YOffset3");
                xmlWriter.WriteValue(jobDetails.YOffset3);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("YOffset4");
                xmlWriter.WriteValue(jobDetails.YOffset4);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("MICRFont");
                xmlWriter.WriteStartElement("FontOptions");
                xmlWriter.WriteValue(jobDetails.FontOption);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Id");
                xmlWriter.WriteString(jobDetails.FontId);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString(jobDetails.FontName);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("File");
                xmlWriter.WriteString(jobDetails.FontFile);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("MICRId");
                xmlWriter.WriteStartElement("AuxOnUSField");
                xmlWriter.WriteString(jobDetails.AuxOnUSField);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("TransitNoField");
                xmlWriter.WriteString(jobDetails.TransitField);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("OnUSField");
                xmlWriter.WriteString(jobDetails.OnUSField);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("AmountField");
                xmlWriter.WriteString(jobDetails.AmountField);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("StartAtAuxOnUSField");
                xmlWriter.WriteString(jobDetails.StartAtAuxOnUSField.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("StartAtTransitField");
                xmlWriter.WriteString(jobDetails.StartAtTransitField.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("StartAtOnUSField");
                xmlWriter.WriteString(jobDetails.StartAtOnUSField.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("StartAtAmountField");
                xmlWriter.WriteString(jobDetails.StartAtAmountField.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("MICROffset");
                xmlWriter.WriteStartElement("LeftMargin");
                xmlWriter.WriteString(jobDetails.LeftMargin.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("TopMargin");
                xmlWriter.WriteString(jobDetails.TopMargin.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.Flush();
                xmlWriter.Close();
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SaveJobDetails), nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private bool ValidateUserSettings()
        {
            try
            {
                ArrayList errorList = new ArrayList();
                bool flag1 = this.ValidatePrinterTabValues(errorList);
                bool flag2 = this.ValidateCheckTabValues(errorList);
                if (flag1 && flag2)
                    return true;
                if (ignoreSourceCheckBox.Checked)
                {
                    return true;
                }
                string text = "Missing or invalid data in the following properties: \r\n";


                    for (int index = 0; index < errorList.Count; ++index)
                        text = text + (string)errorList[index] + "\r\n";
                    errorList.Clear();
                    int num = (int)MessageBox.Show(text, "Troy MICR Document Generator", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                return false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ValidateUserSettings), nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private string AutoIncrementMicrId(string micrId, int checkValue)
        {
            try
            {
                string str1 = micrId;
                int startIndex = str1.IndexOf('#');
                if (startIndex == -1)
                    return str1;
                int length1 = str1.LastIndexOf('#') - startIndex + 1;
                string str2 = "";
                string str3 = checkValue.ToString();
                int length2 = str3.Length;
                if (length2 > length1)
                {
                    int count = length2 - length1;
                    str3 = str3.Remove(0, count);
                }
                for (int index = 0; index < length1 - length2; ++index)
                    str2 += "0";
                string newValue = str2 + str3;
                string oldValue = str1.Substring(startIndex, length1);
                return str1.Replace(oldValue, newValue);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ValidateUserSettings), nameof(TroyMICRDocGenerator));
                return string.Empty;
            }
        }

        private void UpdateCheckFieldsValue()
        {
            try
            {
                bool jobChanged = this._jobChanged;
                int num = int.Parse(this.txtCheckNo.Text);
                int autoIncrAmountAt = this.micrIDCtrl.StartAutoIncrAmountAt;
                int autoIncrAuxOnUsAt = this.micrIDCtrl.StartAutoIncrAuxOnUSAt;
                int autoIncrTransitAt = this.micrIDCtrl.StartAutoIncrTransitAt;
                int startAutoIncrOnUsAt = this.micrIDCtrl.StartAutoIncrOnUSAt;
                if (this.micrIDCtrl.AmountField.Contains("#"))
                    this.micrIDCtrl.StartAutoIncrAmountAt = autoIncrAmountAt + num;
                if (this.micrIDCtrl.AuxOnUSField.Contains("#"))
                    this.micrIDCtrl.StartAutoIncrAuxOnUSAt = autoIncrAuxOnUsAt + num;
                if (this.micrIDCtrl.TransitField.Contains("#"))
                    this.micrIDCtrl.StartAutoIncrTransitAt = autoIncrTransitAt + num;
                if (this.micrIDCtrl.OnUSField.Contains("#"))
                    this.micrIDCtrl.StartAutoIncrOnUSAt = startAutoIncrOnUsAt + num;
                if (!this._newJob)
                    this.UpdateCheckFieldValuesInJob(this.micrIDCtrl.StartAutoIncrAuxOnUSAt, this.micrIDCtrl.StartAutoIncrTransitAt, this.micrIDCtrl.StartAutoIncrOnUSAt, this.micrIDCtrl.StartAutoIncrAmountAt);
                this._jobChanged = jobChanged;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdateCheckFieldsValue), nameof(TroyMICRDocGenerator));
            }
        }

        private void UpdateCheckFieldValuesInJob(
          int startAtAuxOnUS,
          int startAtTransit,
          int startAtOnUS,
          int startAtAmount)
        {
            try
            {
                string text = this.txtJobName.Text;
                string str1 = TroyMICRDocGenerator._appPath + "\\JOBS";
                string str2 = text.EndsWith(".job", StringComparison.CurrentCultureIgnoreCase) ? str1 + "\\" + text : str1 + "\\" + text + ".job";
                if (!File.Exists(str2))
                    return;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(str2);
                XmlNode xmlNode1 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtAuxOnUSField");
                if (xmlNode1 != null)
                    xmlNode1.InnerText = startAtAuxOnUS.ToString();
                XmlNode xmlNode2 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtTransitField");
                if (xmlNode2 != null)
                    xmlNode2.InnerText = startAtTransit.ToString();
                XmlNode xmlNode3 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtOnUSField");
                if (xmlNode3 != null)
                    xmlNode3.InnerText = startAtOnUS.ToString();
                XmlNode xmlNode4 = xmlDocument.SelectSingleNode("//MICRJob/MICRId/StartAtAmountField");
                if (xmlNode4 != null)
                    xmlNode4.InnerText = startAtAmount.ToString();
                xmlDocument.Save(str2);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdateCheckFieldValuesInJob), nameof(TroyMICRDocGenerator));
            }
        }

        private void PrintToolbarButtonClicked(object sender, EventArgs e)
        {
            try
            {
                if (!this.ValidateUserSettings())
                    return;

                CheckDetails checkDetails = new CheckDetails();
                checkDetails.PrintDate = DateTime.Now.Month.ToString() + "-" + (object)DateTime.Now.Day + "-" + (object)DateTime.Now.Year;
                checkDetails.PrintTime = DateTime.Now.Hour.ToString() + ":" + (object)DateTime.Now.Minute + ":" + (object)DateTime.Now.Second;
                checkDetails.PrinterModel = this.txtPrinterModel.Text;
                checkDetails.PrinterSerialNumber = this.txtPrinterSerialNo.Text;
                checkDetails.TonerCartrideType = this.txtTonerType.Text;
                checkDetails.BIASVoltage = this.txtDensity.Text;
                checkDetails.FileName = this.txtJobName.Text;
                checkDetails.BatchId = this.txtBatchId.Text;
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                checkDetails.AppVersion = version.ToString();
                checkDetails.FontHeight = "10";
                checkDetails.FontID = "16835";
                if (this.rdSoftFont.Checked)
                {
                    string itemText = this.cbMICRSourceFont.GetItemText(this.cbMICRSourceFont.SelectedItem);
                    checkDetails.MICRFontPrinted = "Soft Font " + itemText;
                    if (itemText.EndsWith(".t15", StringComparison.CurrentCultureIgnoreCase))
                        checkDetails.FontHeight = "12";
                }
                else if (this.rdResidentFont.Checked)
                {
                    string text = this.txtResidentFontID.Text;
                    checkDetails.MICRFontPrinted = "Resident Font " + text;
                    checkDetails.FontID = this.txtResidentFontID.Text.Trim();
                }
                else
                {
                    string itemText = this.cbMICRSourceFont.GetItemText(this.cbMICRSourceFont.SelectedItem);
                    checkDetails.MICRFontPrinted = "Installed Font " + itemText;
                }
                checkDetails.MICRLeftOffset = (int)this.numericUpDownLeftMargin.Value;
                checkDetails.MICRTopOffset = (int)this.numericUpDownTopMargin.Value;
                checkDetails.ChecksPerPage = !this.rd3UpPrinting.Checked ? 4 : 3;
                int length = int.Parse(this.txtCheckNo.Text);
                if (checkDetailsPrintContinuously.Checked)
                {
                    length = 10000;
                }

                if (length == 0)
                {
                    int num1 = (int)MessageBox.Show("Please enter a valid check count.", "Troy MICR Document Generator");
                }
                else
                {
                    int totalPages = (int)Math.Ceiling((double)((float)length / (float)checkDetails.ChecksPerPage));

                    if (totalPages == 0)
                        totalPages = 1;

                    checkDetails.TotalChecks = length;
                    checkDetails.YOffset = new int[checkDetails.ChecksPerPage];
                    if (this.txtHeight1.Text.Length == 0)
                    {
                        checkDetails.YOffset[0] = -1;
                    }
                    else
                    {
                        float num2 = float.Parse(this.txtHeight1.Text);
                        checkDetails.YOffset[0] = (int)Math.Ceiling((double)num2 * 300.0);
                    }
                    if (this.txtHeight2.Text.Length == 0)
                    {
                        checkDetails.YOffset[1] = -1;
                    }
                    else
                    {
                        float num3 = float.Parse(this.txtHeight2.Text);
                        checkDetails.YOffset[1] = (int)Math.Ceiling((double)num3 * 300.0);
                    }
                    if (this.txtHeight3.Text.Length == 0)
                    {
                        checkDetails.YOffset[2] = -1;
                    }
                    else
                    {
                        float num4 = float.Parse(this.txtHeight3.Text);
                        checkDetails.YOffset[2] = (int)Math.Ceiling((double)num4 * 300.0);
                    }
                    if (this.rd4UpPrinting.Checked)
                    {
                        if (this.txtHeight4.Text.Length == 0)
                        {
                            checkDetails.YOffset[3] = -1;
                        }
                        else
                        {
                            float num5 = float.Parse(this.txtHeight4.Text);
                            checkDetails.YOffset[3] = (int)Math.Ceiling((double)num5 * 300.0);
                        }
                    }
                    if (checkDetails.ChecksPerPage == 3)
                    {
                        if (checkDetails.YOffset[0] != -1 && checkDetails.YOffset[0] < 45)
                            checkDetails.YOffset[0] = 45;
                    }
                    else if (checkDetails.YOffset[0] != -1 && checkDetails.YOffset[0] < 30)
                        checkDetails.YOffset[0] = 30;
                    string auxOnUsField = this.micrIDCtrl.AuxOnUSField;
                    int autoIncrAuxOnUsAt = this.micrIDCtrl.StartAutoIncrAuxOnUSAt;
                    string transitField = this.micrIDCtrl.TransitField;
                    int autoIncrTransitAt = this.micrIDCtrl.StartAutoIncrTransitAt;
                    string onUsField = this.micrIDCtrl.OnUSField;
                    int startAutoIncrOnUsAt = this.micrIDCtrl.StartAutoIncrOnUSAt;
                    string amountField = this.micrIDCtrl.AmountField;
                    int autoIncrAmountAt = this.micrIDCtrl.StartAutoIncrAmountAt;
                    checkDetails.CheckMICR = new string[length];
                    for (int index = 0; index < length; ++index)
                    {
                        string str = this.AutoIncrementMicrId(auxOnUsField, autoIncrAuxOnUsAt + index) + this.AutoIncrementMicrId(transitField, autoIncrTransitAt + index) + this.AutoIncrementMicrId(onUsField, startAutoIncrOnUsAt + index) + this.AutoIncrementMicrId(amountField, autoIncrAmountAt + index);
                        checkDetails.CheckMICR[index] = str;
                    }
                    checkDetails.PaperSize = string.Compare(this.cbPaperSize.GetItemText(this.cbPaperSize.SelectedItem), "A4", true) != 0 ? 0 : 1;
                    PrintDocument printDoc = (PrintDocument)new MICRDocument(totalPages, checkDetails);
                    printDoc.PrintPage += new PrintPageEventHandler(this.PrintMICRDocPage);
                    string itemText = this.cbPrinterName.GetItemText(this.cbPrinterName.SelectedItem);
                    string printerStatus = (string)null;
                    string printerError = (string)null;
                    if (this.GetPrinterStatus(itemText, ref printerStatus, ref printerError))
                    {
                        if (MessageBox.Show("There is an error on the Printer. Error details are as follows: \r\nPrinter Status = " + printerStatus + "\tError = " + printerError + ". \r\nDo you want to continue?", "Troy MICR Document Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
                            return;
                    }
                    printDoc.PrinterSettings.PrinterName = itemText;
                    this.SetSelectedPaperSize(checkDetails.PaperSize, printDoc);
                    string str1 = this.cbPaperSource.GetItemText(this.cbPaperSource.SelectedItem);
                    if (string.Compare(str1, "Automatic", true) == 0)
                        str1 = "Automatically Select";
                    if (!this.SetPrinterPaperSource(printDoc, str1))
                        return;
                    if (!this.IsValidYOffset(checkDetails.YOffset, checkDetails.ChecksPerPage, checkDetails.PaperSize, checkDetails.MICRTopOffset))
                    {
                        int num6 = (int)MessageBox.Show("Y Offsets / MICR Top Offset for check are not valid. Please enter correct values.", "Troy MICR Document Generator");
                    }
                    else
                    {
                        this._pageWidthIn = printDoc.DefaultPageSettings.PrintableArea.Width / 100f;
                        this._pageXIn = printDoc.DefaultPageSettings.PrintableArea.X / 100f;
                        this._pageYIn = printDoc.DefaultPageSettings.PrintableArea.Y / 100f;
                        if (new PrinterSettings()
                        {
                            PrinterName = itemText
                        }.IsValid)
                        {
                            bool doesPrinterExist = PrinterExists("Print to File PCL5");
                            if (!doesPrinterExist)
                            {
                                createNewPrintToFilePrinter();
                            }

                            TroyMICRDocGenerator._pagesPrinted = 0;
                            printDoc.PrinterSettings.PrinterName = "Print to File PCL5";
                            printDoc.PrinterSettings.PrintToFile = true;
                            printDoc.PrinterSettings.PrintFileName = "Output.PRN";
                            printDoc.Print();
                            PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, @"Output.PRN", "Troy Print Test");
                            this.UpdateCheckFieldsValue();
                        }
                        else
                        {
                            int num7 = (int)MessageBox.Show("Printer " + itemText + " is not a valid printer. Please select a valid printer.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PrintToolbarButtonClicked), nameof(TroyMICRDocGenerator));
            }
        }

        private void sendToPrinter(CheckDetails checkDetails, int totalPages, string auxOnUsField, int autoIncrAuxOnUsAt, string transitField, int autoIncrTransitAt, string onUsField, int startAutoIncrOnUsAt, string amountField, int autoIncrAmountAt, int checksPerPage, ref int currentCheck, string itemText)
        {
            try
            {
                for (int i = 0; i < totalPages; i++)
                {
                    if (checkDetailsNumericUpDown.Value > 0)
                    {
                        Thread.Sleep((int)(checkDetailsNumericUpDown.Value * 1000));
                    }
                    for (int j = 0; j < checksPerPage; j++)
                    {

                        string str = this.AutoIncrementMicrId(auxOnUsField, autoIncrAuxOnUsAt + currentCheck) + this.AutoIncrementMicrId(transitField, autoIncrTransitAt + currentCheck) + this.AutoIncrementMicrId(onUsField, startAutoIncrOnUsAt + i) + this.AutoIncrementMicrId(amountField, autoIncrAmountAt + currentCheck);
                        checkDetails.CheckMICR[j] = str;
                        currentCheck++;
                    }
                    PrintDocument printDoc = (PrintDocument)new MICRDocument(1, checkDetails);
                    printDoc.PrintPage += new PrintPageEventHandler(this.PrintMICRDocPage);

                    printDoc.PrinterSettings.PrinterName = itemText;
                    this.SetSelectedPaperSize(checkDetails.PaperSize, printDoc);
                    string str1 = selectedItem;
                    if (string.Compare(str1, "Automatic", true) == 0)
                        str1 = "Automatically Select";
                    if (!this.SetPrinterPaperSource(printDoc, str1))
                        return;
                    if (!this.IsValidYOffset(checkDetails.YOffset, checkDetails.ChecksPerPage, checkDetails.PaperSize, checkDetails.MICRTopOffset))
                    {
                        int num6 = (int)MessageBox.Show("Y Offsets / MICR Top Offset for check are not valid. Please enter correct values.", "Troy MICR Document Generator");
                    }
                    else
                    {
                        this._pageWidthIn = printDoc.DefaultPageSettings.PrintableArea.Width / 100f;
                        this._pageXIn = printDoc.DefaultPageSettings.PrintableArea.X / 100f;
                        this._pageYIn = printDoc.DefaultPageSettings.PrintableArea.Y / 100f;
                        if (new PrinterSettings()
                        {
                            PrinterName = itemText
                        }.IsValid)
                        {
                            bool doesPrinterExist = PrinterExists("Print to File PCL5");
                            if (!doesPrinterExist)
                            {
                                createNewPrintToFilePrinter();
                            }

                            TroyMICRDocGenerator._pagesPrinted = 0;
                            printDoc.PrinterSettings.PrinterName = "Print to File PCL5";
                            printDoc.PrinterSettings.PrintToFile = true;
                            printDoc.PrinterSettings.PrintFileName = "Output.PRN";
                            printDoc.Print();
                            PrintToSpooler.SendFileToPrinter(printerName, @"Output.PRN", "Troy Print Test");

                        }
                        else
                        {
                            int num7 = (int)MessageBox.Show("Printer " + itemText + " is not a valid printer. Please select a valid printer.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(sendToPrinter), nameof(TroyMICRDocGenerator));
            }
        }

        private static void createNewPrintToFilePrinter()
        {
            try
            {
                Logger.LogMessage("Reading powershell script.");

                string text = System.IO.File.ReadAllText(@"addPrinter.ps1");

                using (PowerShell PowerShellInst = PowerShell.Create())
                {

                    PowerShellInst.AddScript(text);
                    Collection<PSObject> PSOutput = PowerShellInst.Invoke();
                    Logger.LogMessage("Powershell script executed.");
                    foreach (PSObject obj in PSOutput)
                    {
                        if (obj != null)
                        {
                            Logger.LogMessage("Output is : " + obj.ToString());
                        }
                    }
                    Logger.LogMessage("Done adding printer.");
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(createNewPrintToFilePrinter), nameof(TroyMICRDocGenerator));
            }
        }

        public bool PrinterExists(string printerName)
        {
            try
            {
                if (String.IsNullOrEmpty(printerName)) { throw new ArgumentNullException("printerName"); }
                return PrinterSettings.InstalledPrinters.Cast<string>().Any(name => printerName.ToUpper().Trim() == name.ToUpper().Trim());
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PrinterExists), nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private bool SetPrinterPaperSource(PrintDocument printDoc, string paperSrc)
        {
            try
            {
                PaperSource paperSource = (PaperSource)null;
                for (int index = 0; index < printDoc.PrinterSettings.PaperSources.Count; ++index)
                {
                    if (string.Compare(printDoc.PrinterSettings.PaperSources[index].SourceName.Trim(), paperSrc, true) == 0)
                    {
                        paperSource = printDoc.PrinterSettings.PaperSources[index];
                        break;
                    }
                }
                if (!ignoreSourceCheckBox.Checked)
                {
                    if (paperSource == null)
                    {
                        int num = (int)MessageBox.Show("The Paper Source selected is not supported by the printer.\r\n Please select a different paper source and try again.", "Troy MICR Document Generator", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return false;
                    }

                    printDoc.DefaultPageSettings.PaperSource = paperSource;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetPrinterPaperSource), nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private bool IsValidYOffset(
          int[] yOffset,
          int checksPerPage,
          int paperSize,
          int micrTopOffset)
        {
            try
            {
                int num1 = paperSize != 0 ? 3392 : 3185;
                int num2;
                int num3;
                if (checksPerPage == 3)
                {
                    num2 = 882 + micrTopOffset;
                    num3 = 15;
                }
                else
                {
                    num2 = 619 + micrTopOffset;
                    num3 = 19;
                }
                return yOffset[0] == -1 && yOffset[1] == -1 && yOffset[2] == -1 || (yOffset[1] == -1 || yOffset[0] == -1 || yOffset[1] - yOffset[0] >= num2 + num3 && yOffset[1] + num2 <= num1 && yOffset[0] + num2 <= num1) && (yOffset[1] == -1 || yOffset[2] == -1 || yOffset[2] - yOffset[1] >= num2 + num3 && yOffset[2] + num2 <= num1) && (yOffset[0] == -1 || yOffset[2] == -1 || yOffset[2] - yOffset[0] >= num2 * 2 + num3) && (checksPerPage != 4 || (yOffset[2] == -1 || yOffset[3] == -1 || yOffset[3] - yOffset[2] >= num2 + num3 && yOffset[3] + num2 <= num1) && (yOffset[0] == -1 || yOffset[3] == -1 || yOffset[3] - yOffset[0] >= num2 * 3 + num3) && (yOffset[1] == -1 || yOffset[3] == -1 || yOffset[3] - yOffset[1] >= num2 * 2 + num3));
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(IsValidYOffset), nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private void SetSelectedPaperSize(int selPaperSize, PrintDocument printDoc)
        {
            try
            {
                PaperKind paperKind = PaperKind.Letter;
                switch (selPaperSize)
                {
                    case 0:
                        paperKind = PaperKind.Letter;
                        break;
                    case 1:
                        paperKind = PaperKind.A4;
                        break;
                }
                foreach (PaperSize paperSiz in printDoc.PrinterSettings.PaperSizes)
                {
                    if (paperSiz.Kind == paperKind)
                    {
                        printDoc.DefaultPageSettings.PaperSize = paperSiz;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetSelectedPaperSize), nameof(TroyMICRDocGenerator));
            }
        }

        private void PrintMICRDocPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                MICRDocument micrDoc = (MICRDocument)sender;
                if (micrDoc == null)
                    return;
                e.Graphics.PageUnit = GraphicsUnit.Inch;
                if (this.rd3UpPrinting.Checked)
                    this.Print3Up(e.Graphics, micrDoc);
                else
                    this.Print4Up(e.Graphics, micrDoc);
                ++TroyMICRDocGenerator._pagesPrinted;
                Logger.LogMessage("OnPrintTimedEvent:  Pages Printed:   " + (object)TroyMICRDocGenerator._pagesPrinted);
                ++micrDoc.CurrPageNumber;
                if (micrDoc.CurrPageNumber == micrDoc.TotalPages)
                    e.HasMorePages = false;
                else
                    e.HasMorePages = true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PrintMICRDocPage), nameof(TroyMICRDocGenerator));
            }
        }

        //private string getMICRFontType()
        //{
        //    string result = string.Empty;

        //    if (rdResidentFont.Checked)
        //    {
        //        result = "Resident Font";
        //    }
        //    else if (rdSoftFont.Checked)
        //    {
        //        result = "Soft Font";
        //    }
        //    else if (rdInstalledFont.Checked)
        //    {
        //        result = "Installed Font";
        //    }
        //    return result;
        //}

        private string Create3UPCheckMacro(int startY, int macroId, CheckDetails checkDetail)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("\u001B&f");
                stringBuilder.Append(macroId);
                stringBuilder.Append("Y");
                stringBuilder.Append("\u001B&f0X");
                stringBuilder.Append("\u001B&u300D\u001B(4B\u001B(sp3h12vsb102T\u001B*p75x");
                stringBuilder.Append(startY);
                stringBuilder.Append("Y!\"#\u001B*p75x");
                stringBuilder.Append(startY + 50);
                stringBuilder.Append("Y$%&");
                stringBuilder.Append("\u001B&f0S\u001B*p0Y\u001B&f1S\u001B*p25x");
                stringBuilder.Append(startY + (int)sbyte.MaxValue);
                stringBuilder.Append("Y\u001B*c36G\u001B*c2350a25b2P\r\n");
                stringBuilder.Append("\u001B(8U\u001B(sp12.00h10.00v1s2b4099T\u001B*p75x");
                stringBuilder.Append(startY + 100);
                stringBuilder.Append("YMDG\u001B*p180x");
                stringBuilder.Append(startY + 100);
                stringBuilder.Append("Yv");
                stringBuilder.Append(checkDetail.AppVersion);
                stringBuilder.Append("\u001B&f0S\u001B*p0Y\u001B&f1S\u001B(8U\u001B(sp12.00h10.00vsb4099T");
                stringBuilder.Append("\u001B*p90x");
                stringBuilder.Append(startY + 325);
                stringBuilder.Append("YON-US\u001B*p90x");
                stringBuilder.Append(startY + 375);
                stringBuilder.Append("YAverage");
                stringBuilder.Append("\u001B*p360x");
                stringBuilder.Append(startY + 350);
                stringBuilder.Append("Y=\u001B*p780x");
                stringBuilder.Append(startY + 350);
                stringBuilder.Append("Y%");
                stringBuilder.Append("\u001B*p90x");
                stringBuilder.Append(startY + 475);
                stringBuilder.Append("YDocument\u001B*p90x");
                stringBuilder.Append(startY + 525);
                stringBuilder.Append("YAverage");
                stringBuilder.Append("\u001B*p360x");
                stringBuilder.Append(startY + 500);
                stringBuilder.Append("Y=\u001B*p780x");
                stringBuilder.Append(startY + 500);
                stringBuilder.Append("Y%");
                stringBuilder.Append("\u001B*p90x");
                stringBuilder.Append(startY + 625);
                stringBuilder.Append("YDocument\u001B*p90x");
                stringBuilder.Append(startY + 675);
                stringBuilder.Append("YRange");
                stringBuilder.Append("\u001B*p360x");
                stringBuilder.Append(startY + 650);
                stringBuilder.Append("Y=\u001B*p780x");
                stringBuilder.Append(startY + 650);
                stringBuilder.Append("Y%");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 250);
                stringBuilder.Append("YDate Printed\u001B*p1400x");
                stringBuilder.Append(startY + 250);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 300);
                stringBuilder.Append("YTime Printed\u001B*p1400x");
                stringBuilder.Append(startY + 300);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 350);
                stringBuilder.Append("YPrinter Model\u001B*p1400x");
                stringBuilder.Append(startY + 350);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 400);
                stringBuilder.Append("YPrinter Serial Number\u001B*p1400x");
                stringBuilder.Append(startY + 400);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 450);
                stringBuilder.Append("YToner Cartridge Type\u001B*p1400x");
                stringBuilder.Append(startY + 450);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 500);
                stringBuilder.Append("YMICR Font Printed\u001B*p1400x");
                stringBuilder.Append(startY + 500);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 550);
                stringBuilder.Append("YBias Voltage/Density\u001B*p1400x");
                stringBuilder.Append(startY + 550);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 600);
                stringBuilder.Append("YFile Name\u001B*p1400x");
                stringBuilder.Append(startY + 600);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p850x");
                stringBuilder.Append(startY + 650);
                stringBuilder.Append("YBatch ID\u001B*p1400x");
                stringBuilder.Append(startY + 650);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B(8U\u001B(sp12.00h10.00vs2b4099T");
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 250);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrintDate);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 300);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrintTime);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 350);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrinterModel);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 400);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrinterSerialNumber);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 450);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.TonerCartrideType);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 500);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.MICRFontPrinted);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 550);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.BIASVoltage);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 600);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.FileName);
                stringBuilder.Append("\u001B*p1505x");
                stringBuilder.Append(startY + 650);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.BatchId);
                stringBuilder.Append("\u001B&f1X");
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Create3UPCheckMacro), nameof(TroyMICRDocGenerator));
                return string.Empty;
            }
        }

        private string Create4UPCheckMacro(int startY, int macroId, CheckDetails checkDetail)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("\u001B&f");
                stringBuilder.Append(macroId);
                stringBuilder.Append("Y");
                stringBuilder.Append("\u001B&f0X");
                stringBuilder.Append("\u001B&u300D\u001B(4B\u001B(sp3h12vsb102T\u001B*p705x");
                stringBuilder.Append(startY + 19);
                stringBuilder.Append("Y!\"#\u001B*p705x");
                stringBuilder.Append(startY + 69);
                stringBuilder.Append("Y$%&");
                stringBuilder.Append("\u001B(8U\u001B(sp12.00h10.00v1s2b4099T\u001B*p705x");
                stringBuilder.Append(startY + 100);
                stringBuilder.Append("YMICR DOCUMENT\u001B*p705x");
                stringBuilder.Append(startY + 138);
                stringBuilder.Append("YGENERATOR v");
                stringBuilder.Append(checkDetail.AppVersion);
                stringBuilder.Append("\u001B&f0S\u001B*p0Y\u001B&f1S\u001B(8U\u001B(sp12.00h10.00vsb4099T");
                stringBuilder.Append("\u001B*p720x");
                stringBuilder.Append(startY + 247);
                stringBuilder.Append("YON-US Avg.");
                stringBuilder.Append("\u001B*p990x");
                stringBuilder.Append(startY + 247);
                stringBuilder.Append("Y=\u001B*p1200x");
                stringBuilder.Append(startY + 247);
                stringBuilder.Append("Y%");
                stringBuilder.Append("\u001B*p720x");
                stringBuilder.Append(startY + 341);
                stringBuilder.Append("YDoc. Avg.");
                stringBuilder.Append("\u001B*p990x");
                stringBuilder.Append(startY + 341);
                stringBuilder.Append("Y=\u001B*p1200x");
                stringBuilder.Append(startY + 341);
                stringBuilder.Append("Y%");
                stringBuilder.Append("\u001B*p720x");
                stringBuilder.Append(startY + 435);
                stringBuilder.Append("YDoc. Range");
                stringBuilder.Append("\u001B*p990x");
                stringBuilder.Append(startY + 435);
                stringBuilder.Append("Y=\u001B*p1200x");
                stringBuilder.Append(startY + 435);
                stringBuilder.Append("Y%");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY);
                stringBuilder.Append("YDate Printed\u001B*p1400x");
                stringBuilder.Append(startY);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 51);
                stringBuilder.Append("YTime Printed\u001B*p1400x");
                stringBuilder.Append(startY + 51);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 132);
                stringBuilder.Append("YPrinter Model\u001B*p1400x");
                stringBuilder.Append(startY + 132);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 182);
                stringBuilder.Append("YSerial Number\u001B*p1400x");
                stringBuilder.Append(startY + 182);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 232);
                stringBuilder.Append("YToner Type\u001B*p1400x");
                stringBuilder.Append(startY + 232);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 282);
                stringBuilder.Append("YMICR Font\u001B*p1400x");
                stringBuilder.Append(startY + 282);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 332);
                stringBuilder.Append("YVoltage/Density\u001B*p1400x");
                stringBuilder.Append(startY + 332);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 413);
                stringBuilder.Append("YFile Name\u001B*p1400x");
                stringBuilder.Append(startY + 413);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B*p1275x");
                stringBuilder.Append(startY + 463);
                stringBuilder.Append("YBatch ID\u001B*p1400x");
                stringBuilder.Append(startY + 463);
                stringBuilder.Append("Y=");
                stringBuilder.Append("\u001B(8U\u001B(sp12.00h10.00vs2b4099T");
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrintDate);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 51);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrintTime);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 132);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrinterModel);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 182);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.PrinterSerialNumber);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 232);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.TonerCartrideType);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 282);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.MICRFontPrinted);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 332);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.BIASVoltage);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 413);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.FileName);
                stringBuilder.Append("\u001B*p1755x");
                stringBuilder.Append(startY + 463);
                stringBuilder.Append("Y");
                stringBuilder.Append(checkDetail.BatchId);
                stringBuilder.Append("\u001B&f1X");
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Create4UPCheckMacro), nameof(TroyMICRDocGenerator));
                return string.Empty;
            }
        }

        private string Create3UPRectangleMacro(int check1Y, int check2Y, int check3Y, int macroId)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("\u001B&f");
                stringBuilder.Append(macroId);
                stringBuilder.Append("Y");
                stringBuilder.Append("\u001B&f0X");
                stringBuilder.Append("\u001B&u300D\u001B&f1S\u001B*p74x");
                stringBuilder.Append(check3Y + 711);
                stringBuilder.Append("Y\u001B*c751a1b0P\u001B*p-450Y\u001B*c0P\u001B*c1a450b0P\u001B*p+750X\u001B&f0S\u001B*p0Y\r\n\u001B&f1S\u001B*c0P\u001B*p75x");
                stringBuilder.Append(check3Y + 562);
                stringBuilder.Append("Y\u001B*c751a1b0P\u001B*p75x");
                stringBuilder.Append(check3Y + 412);
                stringBuilder.Append("Y\u001B*c0P");
                stringBuilder.Append("\u001B*p74x");
                stringBuilder.Append(check1Y + 711);
                stringBuilder.Append("Y\u001B*c0P\u001B*p-450Y\u001B*c0P\u001B*c1a450b0P\u001B*p+750X\u001B&f0S\u001B*p0Y\r\n\u001B&f1S\u001B*c0P\u001B*p75x");
                stringBuilder.Append(check1Y + 562);
                stringBuilder.Append("Y\u001B*c751a1b0P\u001B*p75x");
                stringBuilder.Append(check1Y + 412);
                stringBuilder.Append("Y\u001B*c0P");
                stringBuilder.Append("\u001B*p74x");
                stringBuilder.Append(check2Y + 711);
                stringBuilder.Append("Y\u001B*c0P\u001B*p-450Y\u001B*c0P\u001B*c1a450b0P\u001B*p+750X\u001B&f0S\u001B*p0Y\r\n\u001B&f1S\u001B*c0P\u001B*p75x");
                stringBuilder.Append(check2Y + 562);
                stringBuilder.Append("Y\u001B*c751a1b0P\u001B*p75x");
                stringBuilder.Append(check2Y + 412);
                stringBuilder.Append("Y\u001B*c0P");
                stringBuilder.Append("\u001B&f1X");
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Create3UPRectangleMacro), nameof(TroyMICRDocGenerator));
                return string.Empty;
            }
        }

        private string Create4UPRectangleMacro(
          int check1Y,
          int check2Y,
          int check3Y,
          int check4Y,
          int macroId)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("\u001B&f");
                stringBuilder.Append(macroId);
                stringBuilder.Append("Y");
                stringBuilder.Append("\u001B&f0X");
                stringBuilder.Append("\u001B&u300D\u001B&f1S\u001B*p704x");
                stringBuilder.Append(check4Y + 468);
                stringBuilder.Append("Y\u001B*c541a1b0P\u001B*p-281Y\u001B*c0P\u001B*c1a281b0P\u001B*p+540X\u001B&f0S\u001B*p0Y\r\n\u001B&f1S\u001B*c0P\u001B*p705x");
                stringBuilder.Append(check4Y + 375);
                stringBuilder.Append("Y\u001B*c541a1b0P\u001B*p705x");
                stringBuilder.Append(check4Y + 282);
                stringBuilder.Append("Y\u001B*c0P");
                stringBuilder.Append("\u001B*p704x");
                stringBuilder.Append(check1Y + 468);
                stringBuilder.Append("Y\u001B*c0P\u001B*p-281Y\u001B*c0P\u001B*c1a281b0P\u001B*p+540X\u001B&f0S\u001B*p0Y\r\n\u001B&f1S\u001B*c0P\u001B*p705x");
                stringBuilder.Append(check1Y + 375);
                stringBuilder.Append("Y\u001B*c541a1b0P\u001B*p705x");
                stringBuilder.Append(check1Y + 282);
                stringBuilder.Append("Y\u001B*c0P");
                stringBuilder.Append("\u001B*p704x");
                stringBuilder.Append(check2Y + 468);
                stringBuilder.Append("Y\u001B*c0P\u001B*p-281Y\u001B*c0P\u001B*c1a281b0P\u001B*p+540X\u001B&f0S\u001B*p0Y\r\n\u001B&f1S\u001B*c0P\u001B*p705x");
                stringBuilder.Append(check2Y + 375);
                stringBuilder.Append("Y\u001B*c541a1b0P\u001B*p705x");
                stringBuilder.Append(check2Y + 282);
                stringBuilder.Append("Y\u001B*c0P");
                stringBuilder.Append("\u001B*p704x");
                stringBuilder.Append(check3Y + 468);
                stringBuilder.Append("Y\u001B*c0P\u001B*p-281Y\u001B*c0P\u001B*c1a281b0P\u001B*p+540X\u001B&f0S\u001B*p0Y\r\n\u001B&f1S\u001B*c0P\u001B*p705x");
                stringBuilder.Append(check3Y + 375);
                stringBuilder.Append("Y\u001B*c541a1b0P\u001B*p-30x-26Y\u001B*p705x");
                stringBuilder.Append(check3Y + 282);
                stringBuilder.Append("Y\u001B*c0P");
                stringBuilder.Append("\u001B&f1X");
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Create4UPRectangleMacro), nameof(TroyMICRDocGenerator));
                return string.Empty;
            }
        }

        private void DownloadCheckMacro(IntPtr hdcPrinter, int macroId, string macroDefn)
        {
            try
            {
                int destinationIndex1 = 0;
                byte[] bytes1 = Encoding.ASCII.GetBytes(macroDefn);
                int cbInput = bytes1.Length + 2;
                byte[] lpvInData = new byte[cbInput];
                if (lpvInData == null)
                {
                    Logger.LogMessage("Failed to allocate memory for the escapeBytes", "DownloadCheck1Macro", nameof(TroyMICRDocGenerator));
                }
                else
                {
                    byte[] bytes2 = BitConverter.GetBytes(cbInput - 2);
                    if (bytes2 == null)
                    {
                        Logger.LogMessage("Failed to Get Bytes for the escape Data Length!", "DownloadCheck1Macro", nameof(TroyMICRDocGenerator));
                    }
                    else
                    {
                        Array.Copy((Array)bytes2, 0, (Array)lpvInData, destinationIndex1, 2);
                        int destinationIndex2 = destinationIndex1 + 2;
                        Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex2, bytes1.Length);
                        int num1 = destinationIndex2 + bytes1.Length;
                        int num2 = WIN32.ExtEscape(hdcPrinter, WIN32.PASSTHROUGH, cbInput, lpvInData, 0, IntPtr.Zero);
                        if (num2 > 0)
                            return;
                        int lastWin32Error = Marshal.GetLastWin32Error();
                        Logger.LogMessage("WIN32.ExtEscape Failed Return Value: " + (object)num2 + "  and Error: " + (object)lastWin32Error, "DownloadCheck1Macro", nameof(TroyMICRDocGenerator));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "DownloadCheck1Macro", nameof(TroyMICRDocGenerator));
            }
        }

        private void ExecuteMacro(IntPtr hdcPrinter, int macroId)
        {
            try
            {
                int destinationIndex1 = 0;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("\u001B&f");
                stringBuilder.Append(macroId);
                stringBuilder.Append("Y\u001B&f3X");
                byte[] bytes1 = Encoding.ASCII.GetBytes(stringBuilder.ToString());
                int cbInput = bytes1.Length + 2;
                byte[] lpvInData = new byte[cbInput];
                if (lpvInData == null)
                {
                    Logger.LogMessage("Failed to allocate memory for the escapeBytes", nameof(ExecuteMacro), nameof(TroyMICRDocGenerator));
                }
                else
                {
                    byte[] bytes2 = BitConverter.GetBytes(cbInput - 2);
                    if (bytes2 == null)
                    {
                        Logger.LogMessage("Failed to Get Bytes for the escape Data Length!", nameof(ExecuteMacro), nameof(TroyMICRDocGenerator));
                    }
                    else
                    {
                        Array.Copy((Array)bytes2, 0, (Array)lpvInData, destinationIndex1, 2);
                        int destinationIndex2 = destinationIndex1 + 2;
                        Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex2, bytes1.Length);
                        int num1 = destinationIndex2 + bytes1.Length;
                        int num2 = WIN32.ExtEscape(hdcPrinter, WIN32.PASSTHROUGH, cbInput, lpvInData, 0, IntPtr.Zero);
                        if (num2 > 0)
                            return;
                        int lastWin32Error = Marshal.GetLastWin32Error();
                        Logger.LogMessage("WIN32.ExtEscape Failed Return Value: " + (object)num2 + "  and Error: " + (object)lastWin32Error, "DownloadCheck1Macro", nameof(TroyMICRDocGenerator));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ExecuteMacro), nameof(TroyMICRDocGenerator));
            }
        }

        private void DrawCheckNumbers(IntPtr hdcPrinter, string pclCommand)
        {
            try
            {
                int destinationIndex1 = 0;
                byte[] bytes1 = Encoding.ASCII.GetBytes(pclCommand);
                int cbInput = bytes1.Length + 2;
                byte[] lpvInData = new byte[cbInput];
                if (lpvInData == null)
                {
                    Logger.LogMessage("Failed to allocate memory for the escapeBytes", "ExecuteMacro", nameof(TroyMICRDocGenerator));
                }
                else
                {
                    byte[] bytes2 = BitConverter.GetBytes(cbInput - 2);
                    if (bytes2 == null)
                    {
                        Logger.LogMessage("Failed to Get Bytes for the escape Data Length!", "ExecuteMacro", nameof(TroyMICRDocGenerator));
                    }
                    else
                    {
                        Array.Copy((Array)bytes2, 0, (Array)lpvInData, destinationIndex1, 2);
                        int destinationIndex2 = destinationIndex1 + 2;
                        Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex2, bytes1.Length);
                        int num1 = destinationIndex2 + bytes1.Length;
                        int num2 = WIN32.ExtEscape(hdcPrinter, WIN32.PASSTHROUGH, cbInput, lpvInData, 0, IntPtr.Zero);
                        if (num2 > 0)
                            return;
                        int lastWin32Error = Marshal.GetLastWin32Error();
                        Logger.LogMessage("WIN32.ExtEscape Failed Return Value: " + (object)num2 + "  and Error: " + (object)lastWin32Error, "DownloadCheck1Macro", nameof(TroyMICRDocGenerator));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DrawCheckNumbers), nameof(TroyMICRDocGenerator));
            }
        }

        private void Print3Up(Graphics g, MICRDocument micrDoc)
        {
            try
            {
                CheckDetails checkDetail = micrDoc.CheckDetail;
                int num1 = 0;
                int num2 = 962;
                string str1 = "    ";
                int[] numArray = new int[3];
                int micrTopOffset = checkDetail.MICRTopOffset;
                if (checkDetail.PaperSize == 0)
                {
                    if (micrTopOffset > 30)
                    {
                        int num3 = (2280 - (micrTopOffset - 30) - (48 + num2) - num2) / 2;
                        numArray[0] = 48;
                        numArray[1] = 48 + num2 + num3;
                        numArray[2] = 2280 - (micrTopOffset - 30);
                    }
                    else
                    {
                        numArray[0] = 48;
                        numArray[1] = 1164;
                        numArray[2] = 2280;
                    }
                    num1 = 2450;
                }
                else
                {
                    if (micrTopOffset > 30)
                    {
                        int num4 = (2481 - (micrTopOffset - 30) - (48 + num2) - num2) / 2;
                        numArray[0] = 48;
                        numArray[1] = 48 + num2 + num4;
                        numArray[2] = 2481 - (micrTopOffset - 30);
                    }
                    else
                    {
                        numArray[0] = 48;
                        numArray[1] = 1264;
                        numArray[2] = 2481;
                    }
                    num1 = 2380;
                    str1 = "   ";
                }
                int num5 = micrDoc.CurrPageNumber * checkDetail.ChecksPerPage;
                int index1 = num5;
                int num6 = checkDetail.TotalChecks - micrDoc.CurrPageNumber * checkDetail.ChecksPerPage;
                if (num6 > checkDetail.ChecksPerPage)
                    num6 = checkDetail.ChecksPerPage;
                Font textFont = (Font)null;
                SolidBrush blackBrush = (SolidBrush)null;
                float pageWidthIn = this._pageWidthIn;
                int pageWidth = (int)((double)pageWidthIn * 300.0);
                if (this.rdInstalledFont.Checked)
                {
                    try
                    {
                        string itemText = this.cbMICRSourceFont.GetItemText(this.cbMICRSourceFont.SelectedItem);
                        if (itemText == null)
                            return;
                        textFont = new Font(itemText, 12f, FontStyle.Regular);
                        blackBrush = new SolidBrush(Color.Black);
                        for (int index2 = 0; index2 < num6; ++index2)
                        {
                            int num7 = checkDetail.YOffset[index2] == -1 ? numArray[index2] : checkDetail.YOffset[index2];
                            string str2 = str1 + checkDetail.CheckMICR[index1];
                            float num8 = (float)(checkDetail.MICRLeftOffset - 9) / 300f;
                            float y = (float)(num7 + checkDetail.MICRTopOffset + 851 - 33) / 300f;
                            float x = num8;
                            float num9 = 0.125f;
                            for (int index3 = 0; index3 < str2.Length && (double)x + (double)num9 <= (double)pageWidthIn; ++index3)
                            {
                                this.DrawText(g, blackBrush, textFont, str2[index3].ToString(), x, y);
                                x += num9;
                            }
                            ++index1;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        textFont?.Dispose();
                        blackBrush?.Dispose();
                    }
                }
                IntPtr num10 = IntPtr.Zero;
                string str3 = (string)null;
                int num11 = num5;
                try
                {
                    num10 = g.GetHdc();
                    if (num10 == IntPtr.Zero)
                        return;
                    this.SetTopMargin(num10);
                    if (num11 == 0)
                    {
                        this.DownloadTroyTextFont(num10);
                        if (!this.rdInstalledFont.Checked)
                            this.DownloadMICRIdFont(num10);
                        int num12 = checkDetail.YOffset[0] == -1 ? numArray[0] : checkDetail.YOffset[0];
                        string macroDefn1 = this.Create3UPCheckMacro(num12, 100, checkDetail);
                        if (macroDefn1 == null)
                            return;
                        this.DownloadCheckMacro(num10, 100, macroDefn1);
                        int num13 = checkDetail.YOffset[1] == -1 ? numArray[1] : checkDetail.YOffset[1];
                        str3 = (string)null;
                        string macroDefn2 = this.Create3UPCheckMacro(num13, 101, checkDetail);
                        if (macroDefn2 == null)
                            return;
                        this.DownloadCheckMacro(num10, 101, macroDefn2);
                        int num14 = checkDetail.YOffset[2] == -1 ? numArray[2] : checkDetail.YOffset[2];
                        str3 = (string)null;
                        string macroDefn3 = this.Create3UPCheckMacro(num14, 102, checkDetail);
                        if (macroDefn3 == null)
                            return;
                        this.DownloadCheckMacro(num10, 102, macroDefn3);
                        str3 = (string)null;
                        string macroDefn4 = this.Create3UPRectangleMacro(num12, num13, num14, 104);
                        if (macroDefn4 == null)
                            return;
                        this.DownloadCheckMacro(num10, 104, macroDefn4);
                    }
                    this.ExecuteMacro(num10, 104);
                    this.ExecuteMacro(num10, 100);
                    this.ExecuteMacro(num10, 101);
                    this.ExecuteMacro(num10, 102);
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("\u001B&u300D\u001B(8U\u001B(sp12.00h10.00vsb4099T");
                    for (int index4 = 0; index4 < 3; ++index4)
                    {
                        int num15 = checkDetail.YOffset[index4] == -1 ? numArray[index4] : checkDetail.YOffset[index4];
                        stringBuilder.Append("\u001B*p434x");
                        stringBuilder.Append(num15 + 100);
                        stringBuilder.Append("Y(");
                        stringBuilder.Append(num11 + 1);
                        stringBuilder.Append(")");
                        ++num11;
                    }
                    int index5 = num5;
                    for (int index6 = 0; index6 < num6; ++index6)
                    {
                        int num16 = checkDetail.YOffset[index6] == -1 ? numArray[index6] : checkDetail.YOffset[index6];
                        if (!this.rdInstalledFont.Checked)
                            this.DrawMICRId(num10, checkDetail.MICRLeftOffset, num16 + checkDetail.MICRTopOffset + 851, pageWidth, str1 + checkDetail.CheckMICR[index5], checkDetail);
                        ++index5;
                    }
                    stringBuilder.Append("\u001B&u600D");
                    this.DrawCheckNumbers(num10, stringBuilder.ToString());
                }
                catch (Exception ex)
                {
                    Logger.LogMessage(ex, "Print3Up DrawTroyText, Line", nameof(TroyMICRDocGenerator));
                }
                finally
                {
                    if (num10 != IntPtr.Zero)
                        g.ReleaseHdc(num10);
                }
                IntPtr zero = IntPtr.Zero;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Print3Up), nameof(TroyMICRDocGenerator));
            }
        }

        private void Print4Up(Graphics g, MICRDocument micrDoc)
        {
            try
            {
                CheckDetails checkDetail = micrDoc.CheckDetail;
                if (checkDetail == null)
                    return;
                int num1 = 0;
                int num2 = 0;
                int num3 = 713;
                int[] numArray = new int[4];
                string str1 = "    ";
                int micrTopOffset = checkDetail.MICRTopOffset;
                if (checkDetail.PaperSize == 0)
                {
                    if (micrTopOffset > 30)
                    {
                        int num4 = (2544 - (micrTopOffset - 30) - (47 + 2 * num3) - num3) / 3;
                        numArray[0] = 47;
                        numArray[1] = 47 + num3 + num4;
                        numArray[2] = numArray[1] + num3 + num4;
                        numArray[3] = 2544 - (checkDetail.MICRTopOffset - 30);
                    }
                    else
                    {
                        numArray[0] = 47;
                        numArray[1] = 879;
                        numArray[2] = 1713;
                        numArray[3] = 2544;
                    }
                    num1 = 2450;
                }
                else
                {
                    if (micrTopOffset > 30)
                    {
                        int num5 = (2743 - (micrTopOffset - 30) - (47 + 2 * num3) - num3) / 3;
                        numArray[0] = 47;
                        numArray[1] = numArray[0] + num3 + num5;
                        numArray[2] = numArray[1] + num3 + num5;
                        numArray[3] = 2743 - (micrTopOffset - 30);
                    }
                    else
                    {
                        numArray[0] = 47;
                        numArray[1] = 946;
                        numArray[2] = 1844;
                        numArray[3] = 2743;
                    }
                    num1 = 2380;
                    str1 = "   ";
                }
                int num6 = micrDoc.CurrPageNumber * checkDetail.ChecksPerPage;
                int index1 = num6;
                int num7 = checkDetail.TotalChecks - micrDoc.CurrPageNumber * checkDetail.ChecksPerPage;
                if (num7 > checkDetail.ChecksPerPage)
                    num7 = checkDetail.ChecksPerPage;
                Font textFont = (Font)null;
                SolidBrush blackBrush = (SolidBrush)null;
                float pageWidthIn = this._pageWidthIn;
                int pageWidth = (int)((double)pageWidthIn * 300.0);
                if (this.rdInstalledFont.Checked)
                {
                    try
                    {
                        string itemText = this.cbMICRSourceFont.GetItemText(this.cbMICRSourceFont.SelectedItem);
                        if (itemText == null)
                            return;
                        textFont = new Font(itemText, 12f, FontStyle.Regular);
                        blackBrush = new SolidBrush(Color.Black);
                        num2 = numArray[0];
                        for (int index2 = 0; index2 < num7; ++index2)
                        {
                            int num8 = checkDetail.YOffset[index2] == -1 ? numArray[index2] : checkDetail.YOffset[index2];
                            string str2 = str1 + checkDetail.CheckMICR[index1];
                            float num9 = (float)(checkDetail.MICRLeftOffset - 9) / 300f;
                            float y = (float)(num8 + checkDetail.MICRTopOffset + 602 - 33) / 300f;
                            float x = num9;
                            float num10 = 0.125f;
                            for (int index3 = 0; index3 < str2.Length && (double)x + (double)num10 <= (double)pageWidthIn; ++index3)
                            {
                                this.DrawText(g, blackBrush, textFont, str2[index3].ToString(), x, y);
                                x += num10;
                            }
                            ++index1;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        textFont?.Dispose();
                        blackBrush?.Dispose();
                    }
                }
                IntPtr num11 = IntPtr.Zero;
                string str3 = (string)null;
                int num12 = num6;
                try
                {
                    num11 = g.GetHdc();
                    if (num11 == IntPtr.Zero)
                        return;
                    this.SetTopMargin(num11);
                    if (num12 == 0)
                    {
                        this.DownloadTroyTextFont(num11);
                        if (!this.rdInstalledFont.Checked)
                            this.DownloadMICRIdFont(num11);
                        int num13 = checkDetail.YOffset[0] == -1 ? numArray[0] : checkDetail.YOffset[0];
                        string macroDefn1 = this.Create4UPCheckMacro(num13, 100, checkDetail);
                        if (macroDefn1 == null)
                            return;
                        this.DownloadCheckMacro(num11, 100, macroDefn1);
                        int num14 = checkDetail.YOffset[1] == -1 ? numArray[1] : checkDetail.YOffset[1];
                        str3 = (string)null;
                        string macroDefn2 = this.Create4UPCheckMacro(num14, 101, checkDetail);
                        if (macroDefn2 == null)
                            return;
                        this.DownloadCheckMacro(num11, 101, macroDefn2);
                        int num15 = checkDetail.YOffset[2] == -1 ? numArray[2] : checkDetail.YOffset[2];
                        str3 = (string)null;
                        string macroDefn3 = this.Create4UPCheckMacro(num15, 102, checkDetail);
                        if (macroDefn3 == null)
                            return;
                        this.DownloadCheckMacro(num11, 102, macroDefn3);
                        int num16 = checkDetail.YOffset[3] == -1 ? numArray[3] : checkDetail.YOffset[3];
                        str3 = (string)null;
                        string macroDefn4 = this.Create4UPCheckMacro(num16, 103, checkDetail);
                        if (macroDefn4 == null)
                            return;
                        this.DownloadCheckMacro(num11, 103, macroDefn4);
                        string macroDefn5 = this.Create4UPRectangleMacro(num13, num14, num15, num16, 104);
                        if (macroDefn5 == null)
                            return;
                        this.DownloadCheckMacro(num11, 104, macroDefn5);
                    }
                    this.ExecuteMacro(num11, 104);
                    this.ExecuteMacro(num11, 100);
                    this.ExecuteMacro(num11, 101);
                    this.ExecuteMacro(num11, 102);
                    this.ExecuteMacro(num11, 103);
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("\u001B&u300D\u001B(8U\u001B(sp12.00h10.00vsb4099T");
                    for (int index4 = 0; index4 < 4; ++index4)
                    {
                        int num17 = checkDetail.YOffset[index4] == -1 ? numArray[index4] : checkDetail.YOffset[index4];
                        stringBuilder.Append("\u001B*p1180x");
                        stringBuilder.Append(num17 + 138);
                        stringBuilder.Append("Y(");
                        stringBuilder.Append(num12 + 1);
                        stringBuilder.Append(")");
                        ++num12;
                    }
                    int index5 = num6;
                    for (int index6 = 0; index6 < num7; ++index6)
                    {
                        int num18 = checkDetail.YOffset[index6] == -1 ? numArray[index6] : checkDetail.YOffset[index6];
                        if (!this.rdInstalledFont.Checked)
                            this.DrawMICRId(num11, checkDetail.MICRLeftOffset, num18 + checkDetail.MICRTopOffset + 602, pageWidth, str1 + checkDetail.CheckMICR[index5], checkDetail);
                        ++index5;
                    }
                    stringBuilder.Append("\u001B&u600D");
                    this.DrawCheckNumbers(num11, stringBuilder.ToString());
                }
                catch (Exception ex)
                {
                    Logger.LogMessage(ex, "Print3Up DrawTroyText, Line", nameof(TroyMICRDocGenerator));
                }
                finally
                {
                    if (num11 != IntPtr.Zero)
                        g.ReleaseHdc(num11);
                }
                IntPtr zero = IntPtr.Zero;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(Print4Up), nameof(TroyMICRDocGenerator));
            }
        }

        private int GetCheckSpacing(
          TroyMICRDocGenerator.Enum_PaperSize paperSize,
          TroyMICRDocGenerator.Enum_PrintOptions printOptions,
          int startY,
          int micrTopOffset)
        {
            try
            {
                int num1 = 0;
                switch (paperSize)
                {
                    case TroyMICRDocGenerator.Enum_PaperSize.Letter:
                        num1 = 3185;
                        break;
                    case TroyMICRDocGenerator.Enum_PaperSize.A4:
                        num1 = 3392;
                        break;
                }
                int num2;
                int num3;
                if (printOptions == TroyMICRDocGenerator.Enum_PrintOptions.Print_3_Up)
                {
                    num2 = 3;
                    num3 = 882 + micrTopOffset;
                }
                else
                {
                    num2 = 4;
                    num3 = 619 + micrTopOffset;
                }
                return (num1 - num3 * num2 - startY) / (num2 - 1);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "IsValidYOffset", nameof(TroyMICRDocGenerator));
                return -1;
            }
        }

        private void DownloadTroyTextFont(IntPtr hdcPrinter)
        {
            try
            {
                int destinationIndex1 = 0;
                string path = TroyMICRDocGenerator._appPath + "\\MICRFONT\\TROYLINE.FNT";
                byte[] numArray = File.ReadAllBytes(path);
                if (numArray == null)
                {
                    Logger.LogMessage("Failed to read the Font File: " + path, nameof(DownloadTroyTextFont), nameof(TroyMICRDocGenerator));
                }
                else
                {
                    string s1 = "\u001B&u300D\u001B*p0x0Y\u001B*v1N\u001B*l184o\u001B*c" + "102" + "D";
                    string s2 = "\u001B*c5F";
                    byte[] bytes1 = Encoding.ASCII.GetBytes(s1);
                    byte[] bytes2 = Encoding.ASCII.GetBytes(s2);
                    int cbInput = numArray.Length + bytes1.Length * 2 + bytes2.Length + 2;
                    byte[] lpvInData = new byte[cbInput];
                    if (lpvInData == null)
                    {
                        Logger.LogMessage("Failed to allocate memory for the escapeBytes", nameof(DownloadTroyTextFont), nameof(TroyMICRDocGenerator));
                    }
                    else
                    {
                        byte[] bytes3 = BitConverter.GetBytes(cbInput - 2);
                        if (bytes3 == null)
                        {
                            Logger.LogMessage("Failed to Get Bytes for the escape Data Length!", nameof(DownloadTroyTextFont), nameof(TroyMICRDocGenerator));
                        }
                        else
                        {
                            Array.Copy((Array)bytes3, 0, (Array)lpvInData, destinationIndex1, 2);
                            int destinationIndex2 = destinationIndex1 + 2;
                            Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex2, bytes1.Length);
                            int destinationIndex3 = destinationIndex2 + bytes1.Length;
                            Array.Copy((Array)numArray, 0, (Array)lpvInData, destinationIndex3, numArray.Length);
                            int destinationIndex4 = destinationIndex3 + numArray.Length;
                            Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex4, bytes1.Length);
                            int destinationIndex5 = destinationIndex4 + bytes1.Length;
                            Array.Copy((Array)bytes2, 0, (Array)lpvInData, destinationIndex5, bytes2.Length);
                            int num = WIN32.ExtEscape(hdcPrinter, WIN32.PASSTHROUGH, cbInput, lpvInData, 0, IntPtr.Zero);
                            if (num > 0)
                                return;
                            int lastWin32Error = Marshal.GetLastWin32Error();
                            Logger.LogMessage("WIN32.ExtEscape Failed Return Value: " + (object)num + "  and Error: " + (object)lastWin32Error, "printDoc_PrintPage", nameof(TroyMICRDocGenerator));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DownloadTroyTextFont), nameof(TroyMICRDocGenerator));
            }
        }

        private void SetTopMargin(IntPtr hdcPrinter)
        {
            try
            {
                int destinationIndex1 = 0;
                byte[] bytes1 = Encoding.ASCII.GetBytes("\u001B*p0x0Y\r\n\u001B&l1C\u001B&l12E\u001B&l0C\r\n");
                if (bytes1 == null || bytes1.Length == 0)
                {
                    Logger.LogMessage("Failed to allocate memory for the cmdBytes", nameof(SetTopMargin), nameof(TroyMICRDocGenerator));
                }
                else
                {
                    int cbInput = bytes1.Length + 2;
                    byte[] lpvInData = new byte[cbInput];
                    if (lpvInData == null)
                    {
                        Logger.LogMessage("Failed to allocate memory for the escapeBytes", nameof(SetTopMargin), nameof(TroyMICRDocGenerator));
                    }
                    else
                    {
                        byte[] bytes2 = BitConverter.GetBytes(cbInput - 2);
                        if (bytes2 == null)
                        {
                            Logger.LogMessage("Failed to Get Bytes for the escape Data Length!", nameof(SetTopMargin), nameof(TroyMICRDocGenerator));
                        }
                        else
                        {
                            Array.Copy((Array)bytes2, 0, (Array)lpvInData, destinationIndex1, 2);
                            int destinationIndex2 = destinationIndex1 + 2;
                            Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex2, bytes1.Length);
                            int num1 = destinationIndex2 + bytes1.Length;
                            int num2 = WIN32.ExtEscape(hdcPrinter, WIN32.PASSTHROUGH, cbInput, lpvInData, 0, IntPtr.Zero);
                            if (num2 > 0)
                                return;
                            int lastWin32Error = Marshal.GetLastWin32Error();
                            Logger.LogMessage("WIN32.ExtEscape Failed Return Value: " + (object)num2 + "  and Error: " + (object)lastWin32Error, nameof(SetTopMargin), nameof(TroyMICRDocGenerator));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetTopMargin), nameof(TroyMICRDocGenerator));
            }
        }

        private void DownloadMICRIdFont(IntPtr hdcPrinter)
        {
            try
            {
                int destinationIndex1 = 0;
                string path = (string)this._fontTable[(object)this.cbMICRSourceFont.GetItemText(this.cbMICRSourceFont.SelectedItem)];//fontPath;

                if (path == null)
                {
                    Logger.LogMessage("Failed to get Font File Path!", "DrawMICRId", nameof(TroyMICRDocGenerator));
                }
                else
                {
                    byte[] numArray = File.ReadAllBytes(path);
                    if (numArray == null)
                    {
                        Logger.LogMessage("Failed to read the Font File: " + path, "printDoc_PrintPage", nameof(TroyMICRDocGenerator));
                    }
                    else
                    {
                        string s1 = "\u001B&u300D\u001B&l1c8E\u001B*c" + "16835" + "D";
                        string s2 = "\u001B*c5F\r\n\u001B&l1C\u001B&l12E\u001B&l0C\r\n";
                        byte[] bytes1 = Encoding.ASCII.GetBytes(s1);
                        byte[] bytes2 = Encoding.ASCII.GetBytes(s2);
                        int cbInput = numArray.Length + bytes1.Length * 2 + bytes2.Length + 2;
                        byte[] lpvInData = new byte[cbInput];
                        if (lpvInData == null)
                        {
                            Logger.LogMessage("Failed to allocate memory for the escapeBytes", "printDoc_PrintPage", nameof(TroyMICRDocGenerator));
                        }
                        else
                        {
                            byte[] bytes3 = BitConverter.GetBytes(cbInput - 2);
                            if (bytes3 == null)
                            {
                                Logger.LogMessage("Failed to Get Bytes for the escape Data Length!", "printDoc_PrintPage", nameof(TroyMICRDocGenerator));
                            }
                            else
                            {
                                Array.Copy((Array)bytes3, 0, (Array)lpvInData, destinationIndex1, 2);
                                int destinationIndex2 = destinationIndex1 + 2;
                                Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex2, bytes1.Length);
                                int destinationIndex3 = destinationIndex2 + bytes1.Length;
                                Array.Copy((Array)numArray, 0, (Array)lpvInData, destinationIndex3, numArray.Length);
                                int destinationIndex4 = destinationIndex3 + numArray.Length;
                                Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex4, bytes1.Length);
                                int destinationIndex5 = destinationIndex4 + bytes1.Length;
                                Array.Copy((Array)bytes2, 0, (Array)lpvInData, destinationIndex5, bytes2.Length);
                                int num = WIN32.ExtEscape(hdcPrinter, WIN32.PASSTHROUGH, cbInput, lpvInData, 0, IntPtr.Zero);
                                if (num > 0)
                                    return;
                                int lastWin32Error = Marshal.GetLastWin32Error();
                                Logger.LogMessage("WIN32.ExtEscape Failed Return Value: " + (object)num + "  and Error: " + (object)lastWin32Error, "printDoc_PrintPage", nameof(TroyMICRDocGenerator));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DownloadMICRIdFont), nameof(TroyMICRDocGenerator));
            }
        }

        private void DrawMICRId(
          IntPtr hdcPrinter,
          int startX,
          int startY,
          int pageWidth,
          string micrText,
          CheckDetails checkDetail)
        {
            try
            {
                byte[] numArray1 = (byte[])null;
                byte[] numArray2 = (byte[])null;
                int destinationIndex1 = 0;
                string fontId = checkDetail.FontID;
                string fontHeight = checkDetail.FontHeight;
                string str = micrText;
                StringBuilder stringBuilder1 = new StringBuilder();
                float num1 = (float)startX;
                float num2 = 37.5f;
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.Append("\u001B&u300D\u001B(");
                stringBuilder2.Append(fontId);
                stringBuilder2.Append("X\u001B(s");
                stringBuilder2.Append(fontHeight);
                stringBuilder2.Append("v");
                stringBuilder2.Append("\u001B*p");
                stringBuilder2.Append(startX);
                stringBuilder2.Append("x");
                stringBuilder2.Append(startY);
                stringBuilder2.Append("Y");
                for (int index = 0; index < str.Length; ++index)
                {
                    stringBuilder1.Append(str[index]);
                    num1 += num2;
                    if ((double)num1 + (double)num2 <= (double)pageWidth)
                    {
                        bool useEscapeSequence = rdEscapeSequence.Checked;

                        if (useEscapeSequence)
                        {
                            String formattedEscapeSequence = string.Empty;

                            if (cbMICRSourceFontEscapeSequence.Text.Contains("<esc>(0Q<esc>(s1p12.0v0s0b0T - TROY E13-B"))
                            {
                                stringBuilder1.Append("\u001B(");
                                formattedEscapeSequence += "0Q";
                                formattedEscapeSequence += "\u001B";
                                formattedEscapeSequence += "(s1p12.0v0s0b0T";
                            }
                            else if (cbMICRSourceFontEscapeSequence.Text.Contains("<esc>(0Q<esc>(s1p12.0v0s0b1T - TROY CMC-7"))
                            {
                                stringBuilder1.Append("\u001B(");
                                formattedEscapeSequence += "0Q";
                                formattedEscapeSequence += "\u001B";
                                formattedEscapeSequence += "(s1p12.0v0s0b1T";
                            }
                            else
                            {
                                formattedEscapeSequence = cbMICRSourceFontEscapeSequence.Text.Replace("<esc>", "\u001B");
                            }

                            stringBuilder1.Append(formattedEscapeSequence);
                        }
                        else
                        {
                            stringBuilder1.Append("\u001B(");
                            stringBuilder1.Append(fontId);
                            stringBuilder1.Append("X\u001B(s");
                            stringBuilder1.Append(fontHeight);
                            stringBuilder1.Append("v");
                        }
                        stringBuilder1.Append("\u001B*p");
                        stringBuilder1.Append(num1);
                        stringBuilder1.Append("x");
                        stringBuilder1.Append(startY);
                        stringBuilder1.Append("Y");
                    }
                    else
                        break;
                }
                byte[] bytes1 = Encoding.ASCII.GetBytes(stringBuilder2.ToString());
                byte[] bytes2 = Encoding.ASCII.GetBytes(stringBuilder1.ToString());
                int cbInput = bytes1.Length + bytes2.Length + 2;
                byte[] lpvInData = new byte[cbInput];
                if (lpvInData == null)
                {
                    Logger.LogMessage("Failed to allocate memory for the escapeBytes", nameof(DrawMICRId), nameof(TroyMICRDocGenerator));
                }
                else
                {
                    byte[] bytes3 = BitConverter.GetBytes(cbInput - 2);
                    if (bytes3 == null)
                    {
                        Logger.LogMessage("Failed to Get Bytes for the escape Data Length!", nameof(DrawMICRId), nameof(TroyMICRDocGenerator));
                    }
                    else
                    {
                        Array.Copy((Array)bytes3, 0, (Array)lpvInData, destinationIndex1, 2);
                        int destinationIndex2 = destinationIndex1 + 2;
                        Array.Copy((Array)bytes1, 0, (Array)lpvInData, destinationIndex2, bytes1.Length);
                        int destinationIndex3 = destinationIndex2 + bytes1.Length;
                        Array.Copy((Array)bytes2, 0, (Array)lpvInData, destinationIndex3, bytes2.Length);
                        int num3 = WIN32.ExtEscape(hdcPrinter, WIN32.PASSTHROUGH, cbInput, lpvInData, 0, IntPtr.Zero);
                        if (num3 <= 0)
                        {
                            int lastWin32Error = Marshal.GetLastWin32Error();
                            Logger.LogMessage("WIN32.ExtEscape Failed Return Value: " + (object)num3 + "  and Error: " + (object)lastWin32Error, nameof(DrawMICRId), nameof(TroyMICRDocGenerator));
                        }
                        else
                        {
                            numArray1 = (byte[])null;
                            numArray2 = (byte[])null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DrawMICRId), nameof(TroyMICRDocGenerator));
            }
        }

        private void DrawText(
          Graphics g,
          SolidBrush blackBrush,
          Font textFont,
          string text,
          float x,
          float y)
        {
            try
            {
                PointF point = new PointF(x, y);
                g.DrawString(text, textFont, (Brush)blackBrush, point);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DrawText), nameof(TroyMICRDocGenerator));
            }
        }

        private void HelpToolbarButtonClicked(object sender, EventArgs e)
        {
            try
            {
                About about = new About();
                int num = (int)about.ShowDialog();
                if (about.IsDisposed)
                    return;
                about.Dispose();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(HelpToolbarButtonClicked), nameof(TroyMICRDocGenerator));
            }
        }

        private void btnStopPrinting_Click(object sender, EventArgs e)
        {
            stopPrinting();
        }
        
        private void checkDetailsStopPrintingButtonClick(object sender, EventArgs e)
        {
            stopPrintingClicked();
        }

        private void stopPrintingClicked()
        {
            checkDetailsStopPrinting.Enabled = checkDetailsPrintContinuously.Checked;
            if (_canceller != null)
            {
                _canceller.Cancel();
                MessageBox.Show("Printing Stopped");
            }
        }

        private void stopPrinting()
        {
            if (ContinuePrinting)
            {
                MessageBox.Show("Printing stopped.");
            }

            ContinuePrinting = false;
        }

        private void chkContinue_CheckedChanged(object sender, EventArgs e)
        {
            ContinuePrinting = continuePrintingCheckBox.Checked;
            numberOfPrintsNumericUpDown.Enabled = !continuePrintingCheckBox.Checked;
            stopPrintingButton.Enabled = continuePrintingCheckBox.Checked;
            toolBtnPrint.Enabled = !continuePrintingCheckBox.Checked;
        }

        private void checkDetailsPrintContinuously_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDetailsPrintContinuously.Checked)
            {
                checkDetailsNumericUpDown.Value = 6;
            }
            else
            {
                checkDetailsNumericUpDown.Value = 0;
            }
            stopPrintingClicked();
        }

        private void secondsUpDown_ValueChanged(object sender, EventArgs e)
        {
            updateMiliseconds(((System.Windows.Forms.NumericUpDown)(sender)).Value);
        }

        private void milisecondUpDown_ValueChanged(object sender, EventArgs e)
        {
            updateSeconds(((System.Windows.Forms.NumericUpDown)(sender)).Value);
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!waitValue)
            {
                if (tabMain.SelectedTab.Text == "Check Details")
                {
                    toolBtnPrintTestPage.Enabled = false;
                    toolBtnPrint.Enabled = true;
                    updateFontId();
                }

                else if (tabMain.SelectedTab.Text == "Print Test Patterns")
                {
                    toolBtnPrintTestPage.Enabled = true;
                    toolBtnPrint.Enabled = false;
                }

                else
                {
                    toolBtnPrintTestPage.Enabled = true;
                    toolBtnPrint.Enabled = true;
                }
            }
        }

        private void updateFontId()
        {
            if (Globals.StartingFontId > 0)
            {
                rdResidentFont.Checked = true;
                txtResidentFontID.Text = Globals.StartingFontId.ToString();
            }
        }

        private void updateMiliseconds(decimal value)
        {
            try
            {
                if (!secondsUpdating)
                {
                    milisecondsUpdating = true;
                    milisecondsDelayNumericUpDown.Value = (int)value * 1000;
                    milisecondsUpdating = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(updateMiliseconds), nameof(TroyMICRDocGenerator));
            }
        }

        private void updateSeconds(decimal value)
        {
            try
            {
                if (!milisecondsUpdating)
                {
                    secondsUpdating = true;
                    secondDelayNumericUpDown.Value = value / 1000;
                    secondsUpdating = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(updateSeconds), nameof(TroyMICRDocGenerator));
            }
        }

        private void PrintTestDocument(object sender, EventArgs e)
        {
            try
            {
                if ((cbPrinterName.Text != ""))
                {
                    stopPrintingButton.Focus();
                    int maxcntr = Convert.ToInt32((Int32)numberOfPrintsNumericUpDown.Value);
                    int delaycntr = Convert.ToInt32((int)milisecondsDelayNumericUpDown.Value);

                    if (continuePrintingCheckBox.Checked)
                    {
                        ContinuePrinting = true;

                        while (ContinuePrinting)
                        {
                            if (delaycntr > 0)
                            {
                                Thread.Sleep(delaycntr);
                            }
                            printRadioButton();

                            PrintCount++;
                            printCountTextBox.Text = PrintCount.ToString();
                            Application.DoEvents();

                        }
                    }
                    else
                    {
                        for (int cntr = 0; cntr < maxcntr; cntr++)
                        {
                            if (delaycntr > 0)
                            {
                                Thread.Sleep(delaycntr);
                            }
                            printRadioButton();
                            PrintCount++;
                            printCountTextBox.Text = PrintCount.ToString();
                            if (delaycntr > 0)
                            {
                                Thread.Sleep(delaycntr);
                            }
                        }
                        MessageBox.Show("File was sent to printer " + maxcntr.ToString() + " time(s).", "Sent", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Printer name and file name can not be blank.");
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PrintTestDocument), nameof(TroyMICRDocGenerator));
            }
        }

        private void printRadioButton()
        {
            if (testPatternRadioButton.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "test5.prn", "Troy Print Test");
            }
            if (gray15RadioButton.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "15GrayPage.prn", "Troy Print Test");
            }

            if (gray25RadioButton.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "25GrayPage.prn", "Troy Print Test");
            }
            if (gray35RadioButton.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "35GrayPage.prn", "Troy Print Test");
            }
            if (gray50RadioButton.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "50GrayPage.prn", "Troy Print Test");
            }
            if (blackPage.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "BlackPage.prn", "Troy Print Test");
            }
            if (gradientRadioButton.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "GradiantPage.prn", "Troy Print Test");

            }
            if (whitePageRadioButton.Checked)
            {
                PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "WhitePage.prn", "Troy Print Test");
            }
        }

        private void printTestDocument(string printerName)
        {
            printRadioButton();
        }

        private void JobChangedEvent(object sender, EventArgs e) => this._jobChanged = true;

        private void getSerialNumber(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtPrinterModel.Text))
                {
                    connectToPrinter();
                    return;
                }
                else
                {
                    MessageBox.Show("A printer must be selected before the serial number can be retrieved.");
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(getSerialNumber), nameof(TroyMICRDocGenerator));
                MessageBox.Show("An error occured while loading the serial number. Please see the log for additional details.");
                txtPrinterSerialNo.Text = String.Empty;
            }
        }

        private void connectToPrinter()
        {
            try
            {
                txtPrinterSerialNo.Text = "Loading...";
                string ipAddress = getPrinterIp(cbPrinterName.Text);
                if (string.IsNullOrEmpty(ipAddress))
                {
                    MessageBox.Show("Not able to connect to the printer.");
                    txtPrinterSerialNo.Text = String.Empty;
                    return;
                }

                bool setUp = SetupInstance(ipAddress, 9100);
                if (!setUp)
                {
                    txtPrinterSerialNo.Text = "";
                    return;
                }
                string bidiResult = string.Empty;

                IAsyncResult result;
                Action action = () =>
                {
                    string bidiRetunValue = string.Empty;
                    var retValue = CurrentConnection.Instance.SendFileBidi("BidiGetSerialNumber.pcl", out bidiRetunValue);
                    bidiRetunValue = bidiRetunValue.Remove(0, 26);
                    bidiRetunValue = bidiRetunValue.TrimEnd(new char[] { '\r', '\n', '\f' });
                    CurrentConnection.Instance.CloseCurrentIpConnection();
                    bidiResult = (string)bidiRetunValue.Clone();
                };

                result = action.BeginInvoke(null, null);
                if (!result.AsyncWaitHandle.WaitOne(30000))
                {
                    MessageBox.Show("Communication with the printer has timed out.");
                    txtPrinterSerialNo.Text = "";
                }
                txtPrinterSerialNo.Text = bidiResult;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(connectToPrinter), nameof(TroyMICRDocGenerator));
            }
        }

        public bool SetupInstance(string ipAddress, int port)
        {
            bool retValue = true;
            try
            {
                string failMessage = String.Empty;
                if (!CurrentConnection.Instance.SetCurrentIpConnection(ipAddress, port, out failMessage))
                {
                    retValue = false;
                }
                if (!string.IsNullOrEmpty(failMessage))
                {
                    MessageBox.Show("Communication with the printer failed. Please see the log for additional details.");
                    Logger.LogMessage(failMessage);
                }
                return retValue;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(connectToPrinter), nameof(TroyMICRDocGenerator));
                return false;
            }

        }

        private string getPrinterIp(string name)
        {
            try
            {
                string ipAddress = string.Empty;
                var server = new PrintServer();
                var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
                foreach (var queue in queues)
                {
                    if (queue.Name == name)
                    {
                        string printerURL = queue.Location;
                        printerURL = printerURL.Replace("http://", "");
                        printerURL = printerURL.Replace("https://", "");
                        printerURL = printerURL.Replace("https://", "");
                        int index = printerURL.IndexOf(":");
                        if (index >= 0)
                            printerURL = printerURL.Substring(0, index);
                        ipAddress = printerURL;
                    }
                }
                return ipAddress;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(getPrinterIp), nameof(TroyMICRDocGenerator));
                return null;
            }
        }

        private void ApplicationClosingEvent(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (this._jobChanged)
                {
                    switch (MessageBox.Show("Job has changed. Do you want to continue without saving it?", "Troy MICR Document Generator", MessageBoxButtons.YesNo))
                    {
                        case DialogResult.Yes:
                            e.Cancel = false;
                            Logger.LogMessage("***************************************Troy MICR Document Generator Closing*******************************");
                            return;
                        case DialogResult.No:
                            e.Cancel = true;
                            return;
                    }
                }
                Logger.LogMessage("***************************************Troy MICR Document Generator Closing*******************************");
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ApplicationClosingEvent), nameof(TroyMICRDocGenerator));
            }
        }

        private void numberOfPagesLeave(object sender, EventArgs e)
        {
            try
            {
                this.txtCheckNo.TextChanged -= new System.EventHandler(this.checkNumberTextBoxLeave);
                if (!string.IsNullOrEmpty(txtNumberOfPages.Text))
                {
                    int checksPerPage = !this.rd3UpPrinting.Checked ? 4 : 3;
                    int updatedNumberOfChecks = checksPerPage * int.Parse(txtNumberOfPages.Text);
                    txtCheckNo.Text = updatedNumberOfChecks.ToString();
                }
                this.txtCheckNo.TextChanged += new System.EventHandler(this.checkNumberTextBoxLeave);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(numberOfPagesLeave), nameof(TroyMICRDocGenerator));
            }
        }

        private void checkNumberTextBoxLeave(object sender, EventArgs e)
        {
            this.txtNumberOfPages.TextChanged -= new System.EventHandler(this.numberOfPagesLeave);
            calculateNumberOfPages();
            this.txtNumberOfPages.TextChanged += new System.EventHandler(this.numberOfPagesLeave);
        }

        private void calculateNumberOfPages()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCheckNo.Text) && int.Parse(txtCheckNo.Text) != 0)
                {
                    int numberOfChecks = int.Parse(txtCheckNo.Text);
                    int checksPerPage = !this.rd3UpPrinting.Checked ? 4 : 3;
                    decimal updatedNumberOfPagesDecimal = (decimal)numberOfChecks / (decimal)checksPerPage;
                    decimal updatedNumberOfPagesInt = (int)updatedNumberOfPagesDecimal;
                    if (!(Math.Truncate(updatedNumberOfPagesDecimal) == updatedNumberOfPagesDecimal))
                    {
                        updatedNumberOfPagesInt = updatedNumberOfPagesInt + 1;
                    }
                    txtNumberOfPages.Text = updatedNumberOfPagesInt.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(calculateNumberOfPages), nameof(TroyMICRDocGenerator));
            }

        }
        private void NumericTextBoxKeyDownEvent(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Delete)
                    return;
                TextBox textBox = (TextBox)sender;
                if (textBox == null)
                    return;
                int start = textBox.Text.Length - 1;
                if (start == -1)
                    return;
                if (textBox.SelectedText.Length != 0)
                {
                    textBox.SelectedText = string.Empty;
                }
                else
                {
                    textBox.Select(start, 1);
                    textBox.SelectedText = string.Empty;
                }
                this._jobChanged = true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(NumericTextBoxKeyDownEvent), nameof(TroyMICRDocGenerator));
            }
        }

        private void NumericTextBoxKeyPressEvent(object sender, KeyPressEventArgs e)
        {
            try
            {
                string str = e.KeyChar.ToString();
                NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
                string decimalSeparator = numberFormat.NumberDecimalSeparator;
                string numberGroupSeparator = numberFormat.NumberGroupSeparator;
                if (char.IsDigit(e.KeyChar) || str.Equals(decimalSeparator) || str.Equals(numberGroupSeparator) || e.KeyChar == '\b')
                {
                    e.Handled = false;
                    this._jobChanged = true;
                }
                else
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(NumericTextBoxKeyPressEvent), nameof(TroyMICRDocGenerator));
            }
        }

        private void InitPrinterTab()
        {
            try
            {
                this.InitPrinterTabControls();
                this.UpdatePaperSizeList();
                this.UpdatePaperOutputList();
                this.cbPrinterName.Items.Clear();
                this._initFlag = true;
                new Thread(new ParameterizedThreadStart(this.PrinterListUpdateThrdCallback)).Start((object)null);
                this._jobChanged = false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(InitPrinterTab), nameof(TroyMICRDocGenerator));
            }
        }
        private void PrinterListUpdateThrdCallback(object obj)
        {
            try
            {
                int count = PrinterSettings.InstalledPrinters.Count;
                for (int index = 0; index < count; ++index)
                {
                    string installedPrinter = PrinterSettings.InstalledPrinters[index];
                    //if (this.IsPCLPrinter(installedPrinter)) This does not work in Windows 10
                    this.DoUpdatePrinterList(installedPrinter);
                }
                this._jobChanged = false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "PrinterUpdateThrdCallback", nameof(TroyMICRDocGenerator));
            }
        }
        public void DoUpdateMouseCursor(bool wait) => this.Invoke((Delegate)new updateMouseCursorDelegate(this.UpdateMouseCursor), (object)wait);
        public void UpdateMouseCursor(bool wait)
        {
            this.waitValue = wait;
            this.UseWaitCursor = wait;
            this.ControlBox = !wait;
            this.toolBtnHelp.Enabled = !wait;
            this.toolBtnPrintTestPage.Enabled = !wait;
            this.toolBtnNew.Enabled = !wait;
            this.toolBtnOpen.Enabled = !wait;
            this.toolBtnSave.Enabled = !wait;
            this.toolBtnPrint.Enabled = !wait;
            this.Invalidate();
        }

        private void DoUpdatePrinterList(string printerName) => this.Invoke((Delegate)new updatePrinterDetailsDelegate(this.UpdatePrinterList), (object)printerName);

                private void UpdatePrinterList(string printerName)
        {
            try
            {
                this.cbPrinterName.Items.Add((object)printerName);
                if (this.cbPrinterName.Items.Count != 1)
                    return;
                this.cbPrinterName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdatePrinterList), nameof(TroyMICRDocGenerator));
            }
        }

        private void DoUpdatePaperSourceItems(string[] paperSources) => this.Invoke((Delegate)new UpdatePaperSourceDelegate(this.UpdatePaperSourceItems), (object)paperSources);

        private void UpdatePaperSourceItems(string[] paperSources)
        {
            try
            {
                for (int index = 0; index < paperSources.Length; ++index)
                    this.cbPaperSource.Items.Add((object)paperSources[index]);
                this.cbPaperSource.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdatePaperSourceItems), nameof(TroyMICRDocGenerator));
            }
        }

        private void DoUpdatePaperSourceList(string paperSource) => this.Invoke((Delegate)new updatePrinterDetailsDelegate(this.UpdatePaperSourceList), (object)paperSource);

        private void UpdatePaperSourceList(string paperSource)
        {
            try
            {
                this.cbPaperSource.Items.Add((object)paperSource);
                if (this.cbPaperSource.Items.Count != 1)
                    return;
                this.cbPaperSource.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdatePaperSourceList), nameof(TroyMICRDocGenerator));
            }
        }

        private void DoSetPaperSourceValue(string paperSource) => this.Invoke((Delegate)new updatePrinterDetailsDelegate(this.SetPaperSourceValue), (object)paperSource);

        private void SetPaperSourceValue(string paperSource)
        {
            try
            {
                int num1 = this.cbPaperSource.FindString(paperSource);
                if (num1 != -1)
                {
                    this.cbPaperSource.SelectedIndex = num1;
                }
                else
                {
                    if (this.cbPaperSource.Items.Count > 0)
                        this.cbPaperSource.SelectedIndex = 0;
                    this.tabMain.SelectedIndex = 0;
                    int num2 = (int)MessageBox.Show("The printer does not support this paper source \"" + paperSource + "\".", "Troy MICR Document Generator", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetPaperSourceValue), nameof(TroyMICRDocGenerator));
            }
        }

        private void DoUpdatePrinterModel(string printerModel) => this.Invoke((Delegate)new updatePrinterDetailsDelegate(this.UpdatePrinterModel), (object)printerModel);

        private void UpdatePrinterModel(string printerModel)
        {
            try
            {
                this.txtPrinterModel.Text = printerModel;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdatePrinterModel), nameof(TroyMICRDocGenerator));
            }
        }

        private void UpdatePaperSizeList()
        {
            try
            {
                for (int index = 0; index < this.PaperSizeValues.Length; ++index)
                    this.cbPaperSize.Items.Add((object)this.PaperSizeValues[index]);
                this.cbPaperSize.SelectedIndex = this.PaperSizeDefault;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdatePaperSizeList), nameof(TroyMICRDocGenerator));
            }
        }

        private void UpdatePaperOutputList()
        {
            try
            {
                for (int index = 0; index < this.PaperOutputValues.Length; ++index)
                    this.cbPaperOutput.Items.Add((object)this.PaperOutputValues[index]);
                this.cbPaperOutput.SelectedIndex = this.PaperOutputDefault;
                this.cbPaperOutput.Enabled = false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(UpdatePaperOutputList), nameof(TroyMICRDocGenerator));
            }
        }

        private void PrinterSelectionChangedEvent(object sender, EventArgs e)
        {
            try
            {
                string itemText = this.cbPrinterName.GetItemText(this.cbPrinterName.SelectedItem);
                this.cbPaperSource.Items.Clear();
                new Thread(new ParameterizedThreadStart(this.PrinterDetailsUpdateThrdCallback)).Start((object)new ThreadData()
                {
                    PrinterName = itemText,
                    initFlag = this._initFlag,
                    PaperSource = this._paperSource
                });
                if (this._initFlag)
                {
                    this._initFlag = false;
                    this._jobChanged = false;
                }
                else
                    this._jobChanged = true;
                this._paperSource = (string)null;
                Globals.SelectedPrinter = cbPrinterName.Text;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PrinterSelectionChangedEvent), nameof(TroyMICRDocGenerator));
            }
        }

        private void PrinterDetailsUpdateThrdCallback(object obj)
        {
            try
            {
                ThreadData threadData = (ThreadData)obj;
                if (threadData == null)
                    return;
                this.DoUpdateMouseCursor(true);
                this.DoUpdatePrinterModel(this.GetPrinterModelName(threadData.PrinterName));
                PrinterSettings printerSettings = new PrinterSettings();
                printerSettings.PrinterName = threadData.PrinterName;
                if (!printerSettings.IsValid)
                    return;
                ArrayList arrayList = new ArrayList();
                arrayList.Add((object)"Automatically Select");
                for (int index = 0; index < printerSettings.PaperSources.Count; ++index)
                {
                    string sourceName = printerSettings.PaperSources[index].SourceName;
                    if (sourceName.ToUpper().Contains("TRAY") || sourceName.Contains("MPT") || sourceName.Contains("MANUAL"))
                        arrayList.Add((object)sourceName.Trim());
                }
                this.DoUpdatePaperSourceItems((string[])arrayList.ToArray(typeof(string)));
                if (threadData.PaperSource != null && threadData.PaperSource.Length > 0)
                {
                    this.DoSetPaperSourceValue(threadData.PaperSource);
                    this._jobChanged = false;
                }
                if (threadData.initFlag)
                    this._jobChanged = false;
                this.DoUpdateMouseCursor(false);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PrinterDetailsUpdateThrdCallback), nameof(TroyMICRDocGenerator));
            }
        }
        private void InitPrinterTabControls()
        {
            try
            {
                this.txtDensity.Text = "";
                this.txtTonerType.Text = "";
                this.txtJobName.Text = "";
                this.txtBatchId.Text = "";
                this.txtPrinterSerialNo.Text = "";
                if (this.cbPrinterName.Items.Count > 0)
                    this.cbPrinterName.SelectedIndex = 0;
                if (this.cbPaperSize.Items.Count > 0)
                    this.cbPaperSize.SelectedIndex = this.PaperSizeDefault;
                this.rd3UpPrinting.Checked = true;
                this.rd4UpPrinting.Checked = false;
                this.txtHeight1.Text = "0.16";
                this.txtHeight2.Text = "3.88";
                this.txtHeight3.Text = "7.6";
                this.txtHeight4.Text = "";
                this.txtHeight4.Enabled = false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(InitPrinterTabControls), nameof(TroyMICRDocGenerator));
            }
        }
        private unsafe bool IsPCLPrinter(string printerName)
        {
            try
            {
                bool flag = false;
                IntPtr hPrinter = new IntPtr(0);
                if (!WIN32.OpenPrinter(printerName, out hPrinter, IntPtr.Zero) || hPrinter == IntPtr.Zero)
                    return false;
                uint pcbNeeded = 0;
                WIN32.GetPrinterDriver(hPrinter, (string)null, 2U, (IntPtr)(void*)null, 0U, out pcbNeeded);
                if (pcbNeeded == 0U)
                {
                    WIN32.ClosePrinter(hPrinter);
                    return false;
                }
                IntPtr num1 = Marshal.AllocHGlobal((int)pcbNeeded);
                if (WIN32.GetPrinterDriver(hPrinter, (string)null, 2U, num1, pcbNeeded, out pcbNeeded) == 0)
                {
                    Marshal.GetLastWin32Error();
                    Marshal.FreeHGlobal(num1);
                    WIN32.ClosePrinter(hPrinter);
                    return false;
                }
                int int32 = num1.ToInt32();
                WIN32.DriverInfo2 driverInfo2 = new WIN32.DriverInfo2();
                Marshal.PtrToStructure(new IntPtr(int32), (object)driverInfo2);
                if (!driverInfo2.pDataFile.EndsWith(".gpd", StringComparison.CurrentCultureIgnoreCase))
                {
                    Marshal.FreeHGlobal(num1);
                    WIN32.ClosePrinter(hPrinter);
                    return false;
                }
                FileStream fileStream = new FileStream(driverInfo2.pDataFile, FileMode.Open, FileAccess.Read);
                StreamReader streamReader = new StreamReader((Stream)fileStream);
                string end = streamReader.ReadToEnd();
                int num2 = 0;
                int num3 = 0;
                int num4 = end.IndexOf("Personality");
                if (num4 != -1)
                {
                    int num5 = end.IndexOf("PERSONALITY_HPGL2", num4 + 1, 60);
                    int num6 = end.IndexOf("HPGL2", num4 + 1, 60);
                    num2 = end.IndexOf("PCLXL", num4 + 1, 60);
                    int num7 = end.IndexOf("PERSONALITY_PCLXL", num4 + 1, 60);
                    if (num5 != -1 || num6 != -1 || num7 != -1 || num3 != -1)
                        flag = true;
                }
                streamReader.Close();
                fileStream.Close();
                Marshal.FreeHGlobal(num1);
                WIN32.ClosePrinter(hPrinter);
                return flag;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "ReadPrinterGPDFile", nameof(TroyMICRDocGenerator));
                return false;
            }
        }
        private bool GetPrinterStatus(
          string printerName,
          ref string printerStatus,
          ref string printerError)
        {
            string str1 = (string)null;
            string str2 = (string)null;
            bool flag = false;
            try
            {
                ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(new ObjectQuery("SELECT * FROM Win32_Printer"));
                ManagementObjectCollection objectCollection = managementObjectSearcher.Get();
                foreach (ManagementObject managementObject in objectCollection)
                {
                    PropertyDataCollection properties = managementObject.Properties;
                    if (string.Compare(((string)managementObject["Name"]).Trim(), printerName, true) == 0)
                    {
                        switch (Convert.ToUInt32(properties["PrinterStatus"].Value))
                        {
                            case 1:
                            case 2:
                            case 6:
                            case 7:
                                flag = true;
                                break;
                            default:
                                flag = false;
                                break;
                        }
                        if (flag)
                        {
                            switch (Convert.ToUInt32(properties["ExtendedPrinterStatus"].Value))
                            {
                                case 1:
                                    str1 = "Other";
                                    flag = true;
                                    break;
                                case 2:
                                    str1 = "Unknown";
                                    flag = true;
                                    break;
                                case 6:
                                    str1 = "Stopped Printing";
                                    flag = true;
                                    break;
                                case 7:
                                    str1 = "Offline";
                                    flag = true;
                                    break;
                                case 8:
                                    str1 = "Paused";
                                    flag = true;
                                    break;
                                case 9:
                                    str1 = "Error";
                                    flag = true;
                                    break;
                                case 10:
                                    str1 = "Busy";
                                    flag = true;
                                    break;
                                case 11:
                                    str1 = "Not Available";
                                    flag = true;
                                    break;
                                case 12:
                                    str1 = "Waiting";
                                    flag = true;
                                    break;
                                case 16:
                                    str1 = "Pending Deletion";
                                    flag = true;
                                    break;
                            }
                            if (flag)
                            {
                                switch (Convert.ToUInt32(properties["ExtendedDetectedErrorState"].Value))
                                {
                                    case 0:
                                        str2 = "Unknown";
                                        continue;
                                    case 1:
                                        str2 = "Other";
                                        continue;
                                    case 2:
                                        str2 = "No Error";
                                        continue;
                                    case 3:
                                        str2 = "Low Paper";
                                        continue;
                                    case 4:
                                        str2 = "No Paper";
                                        continue;
                                    case 5:
                                        str2 = "Low Toner";
                                        continue;
                                    case 6:
                                        str2 = "No Toner";
                                        continue;
                                    case 7:
                                        str2 = "Door Open";
                                        continue;
                                    case 8:
                                        str2 = "Jammed";
                                        continue;
                                    case 9:
                                        str2 = "Service Requested";
                                        continue;
                                    case 10:
                                        str2 = "Output Bin Full";
                                        continue;
                                    case 11:
                                        str2 = "Paper Problem";
                                        continue;
                                    case 12:
                                        str2 = "Cannot Print Page";
                                        continue;
                                    case 13:
                                        str2 = "User Intervention Required";
                                        continue;
                                    case 14:
                                        str2 = "Out of Memory";
                                        continue;
                                    case 15:
                                        str2 = "Server Unknown";
                                        continue;
                                    default:
                                        continue;
                                }
                            }
                        }
                        else
                            break;
                    }
                }
                managementObjectSearcher.Dispose();
                objectCollection.Dispose();
                printerStatus = str1;
                printerError = str2;
                return flag;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetPrinterStatus), nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private string GetPrinterModelName(string printerName)
        {
            string str = (string)null;
            try
            {
                ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(new ObjectQuery("SELECT * FROM Win32_Printer"));
                ManagementObjectCollection objectCollection = managementObjectSearcher.Get();
                foreach (ManagementObject managementObject in objectCollection)
                {
                    PropertyDataCollection properties = managementObject.Properties;
                    if (string.Compare(((string)managementObject["Name"]).Trim(), printerName, true) == 0)
                        str = (string)managementObject["DriverName"];
                }
                managementObjectSearcher.Dispose();
                objectCollection.Dispose();
                return str;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetPrinterModelName), nameof(TroyMICRDocGenerator));
                return (string)null;
            }
        }

        private void GetPrinterTabUserSettings(ref JobDetails jobDetail)
        {
            try
            {
                jobDetail.JobName = this.txtJobName.Text;
                jobDetail.BatchId = this.txtBatchId.Text;
                jobDetail.PrinterName = this.cbPrinterName.GetItemText(this.cbPrinterName.SelectedItem);
                jobDetail.PrinterSerialNo = this.txtPrinterSerialNo.Text;
                jobDetail.PrinterModel = this.txtPrinterModel.Text;
                jobDetail.Density = this.txtDensity.Text;
                jobDetail.PaperSize = this.cbPaperSize.GetItemText(this.cbPaperSize.SelectedItem);
                jobDetail.PaperSource = this.cbPaperSource.GetItemText(this.cbPaperSource.SelectedItem);
                jobDetail.PaperOutput = this.cbPaperOutput.GetItemText(this.cbPaperOutput.SelectedItem);
                jobDetail.TonerType = this.txtTonerType.Text;
                jobDetail.PrintOption = !this.rd3UpPrinting.Checked ? 1 : 0;
                jobDetail.YOffset1 = this.txtHeight1.Text.Length != 0 ? float.Parse(this.txtHeight1.Text) : -1f;
                jobDetail.YOffset2 = this.txtHeight2.Text.Length != 0 ? float.Parse(this.txtHeight2.Text) : -1f;
                jobDetail.YOffset3 = this.txtHeight3.Text.Length != 0 ? float.Parse(this.txtHeight3.Text) : -1f;
                if (this.txtHeight4.Text.Length == 0)
                    jobDetail.YOffset4 = -1f;
                else
                    jobDetail.YOffset4 = float.Parse(this.txtHeight4.Text);
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetPrinterTabUserSettings), nameof(TroyMICRDocGenerator));
            }
        }
        private bool ValidatePrinterTabValues(ArrayList errorList)
        {
            bool flag = true;
            try
            {
                if (this.txtJobName.Text == null || this.txtJobName.Text.Length == 0)
                {
                    errorList.Add((object)"Job Name");
                    flag = false;
                }
                if (this.txtBatchId.Text == null || this.txtBatchId.Text.Length == 0)
                {
                    errorList.Add((object)"Batch Id");
                    flag = false;
                }
                if (this.cbPrinterName.SelectedIndex == -1 || this.cbPrinterName.SelectedItem == null)
                {
                    errorList.Add((object)"Printer Name");
                    flag = false;
                }
                if (this.txtPrinterSerialNo.Text == null || this.txtPrinterSerialNo.Text.Length == 0)
                {
                    errorList.Add((object)"Printer Serial Number");
                    flag = false;
                }
                if (this.txtPrinterModel.Text == null || this.txtPrinterModel.Text.Length == 0)
                {
                    errorList.Add((object)"Printer Model");
                    flag = false;
                }
                if (this.txtDensity.Text == null || this.txtDensity.Text.Length == 0)
                {
                    errorList.Add((object)"BIAS / Density");
                    flag = false;
                }
                if (this.cbPaperSize.SelectedIndex == -1 || this.cbPaperSize.SelectedItem == null)
                {
                    errorList.Add((object)"Paper Size");
                    flag = false;
                }
                if (this.cbPaperSource.SelectedIndex == -1 || this.cbPaperSource.SelectedItem == null)
                {
                    errorList.Add((object)"Paper Source");
                    flag = false;
                }
                if (this.txtTonerType.Text == null || this.txtTonerType.Text.Length == 0)
                {
                    errorList.Add((object)"Toner Type");
                    flag = false;
                }
                if (!this.rd3UpPrinting.Checked && !this.rd4UpPrinting.Checked)
                {
                    errorList.Add((object)"Print Option");
                    flag = false;
                }
                if (this.txtHeight1.Text.Length != 0)
                {
                    if (this.txtHeight1.Text.Contains("-"))
                    {
                        errorList.Add((object)"Y Offset (Check1)");
                        flag = false;
                    }
                    else if (!this.IsValidDecimal(this.txtHeight1.Text))
                    {
                        errorList.Add((object)"Y Offset (Check1)");
                        flag = false;
                    }
                }
                if (this.txtHeight2.Text.Length != 0)
                {
                    if (this.txtHeight2.Text.Contains("-"))
                    {
                        errorList.Add((object)"Y Offset (Check2)");
                        flag = false;
                    }
                    else if (!this.IsValidDecimal(this.txtHeight2.Text))
                    {
                        errorList.Add((object)"Y Offset (Check2)");
                        flag = false;
                    }
                }
                if (this.txtHeight3.Text.Length != 0)
                {
                    if (this.txtHeight3.Text.Contains("-"))
                    {
                        errorList.Add((object)"Y Offset (Check3)");
                        flag = false;
                    }
                    if (!this.IsValidDecimal(this.txtHeight3.Text))
                    {
                        errorList.Add((object)"Y Offset (Check3)");
                        flag = false;
                    }
                }
                if (this.txtHeight4.Text.Length != 0)
                {
                    if (this.txtHeight4.Text.Contains("-"))
                    {
                        errorList.Add((object)"Y Offset (Check4)");
                        flag = false;
                    }
                    if (!this.IsValidDecimal(this.txtHeight4.Text))
                    {
                        errorList.Add((object)"Y Offset (Check4)");
                        flag = false;
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ValidatePrinterTabValues), nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private bool IsValidDecimal(string decNum)
        {
            try
            {
                double num = (double)float.Parse(decNum);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "IsValidDecimal for " + decNum, nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private void DisplayJobFileValuesOnPrinterTab(JobDetails jobDetails)
        {
            try
            {
                this.txtJobName.Text = jobDetails.JobName;
                this.txtBatchId.Text = jobDetails.BatchId;
                this._paperSource = jobDetails.PaperSource;
                int num1 = this.cbPrinterName.FindString(jobDetails.PrinterName);
                if (num1 != -1)
                {
                    this.cbPrinterName.SelectedIndex = num1;
                }
                else
                {
                    this.cbPrinterName.SelectedIndex = 0;
                    this.tabMain.SelectedIndex = 0;
                    int num2 = (int)MessageBox.Show("Could not find the printer \"" + jobDetails.PrinterName + "\" on the system.", "Troy MICR Document Generator", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                this.txtPrinterSerialNo.Text = jobDetails.PrinterSerialNo;
                this.txtDensity.Text = jobDetails.Density;
                int num3 = this.cbPaperSize.FindString(jobDetails.PaperSize);
                if (num3 != -1)
                    this.cbPaperSize.SelectedIndex = num3;
                this.txtTonerType.Text = jobDetails.TonerType;
                this.cbPaperOutput.SelectedIndex = this.PaperOutputDefault;
                this.cbPaperOutput.Enabled = false;
                this.txtCheckNo.Text = jobDetails.NoOfChecks.ToString();
                switch (jobDetails.PrintOption)
                {
                    case 0:
                        this.rd4UpPrinting.Checked = false;
                        this.rd3UpPrinting.Checked = true;
                        break;
                    case 1:
                        this.rd4UpPrinting.Checked = true;
                        this.rd3UpPrinting.Checked = false;
                        if ((double)jobDetails.YOffset4 != -1.0)
                        {
                            this.txtHeight4.Text = jobDetails.YOffset4.ToString();
                            break;
                        }
                        this.txtHeight4.Text = "";
                        break;
                }
                if ((double)jobDetails.YOffset1 != -1.0)
                    this.txtHeight1.Text = jobDetails.YOffset1.ToString();
                else
                    this.txtHeight1.Text = "";
                if ((double)jobDetails.YOffset2 != -1.0)
                    this.txtHeight2.Text = jobDetails.YOffset2.ToString();
                else
                    this.txtHeight2.Text = "";
                if ((double)jobDetails.YOffset3 != -1.0)
                    this.txtHeight3.Text = jobDetails.YOffset3.ToString();
                else
                    this.txtHeight3.Text = "";
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DisplayJobFileValuesOnPrinterTab), nameof(TroyMICRDocGenerator));
            }
        }

        private void PrintOptionChangedEvent(object sender, EventArgs e)
        {
            try
            {
                this._jobChanged = true;
                bool flag = false;
                if (string.Compare(this.cbPaperSize.GetItemText(this.cbPaperSize.SelectedItem), "Letter", true) == 0)
                    flag = true;
                if (this.rd4UpPrinting.Checked)
                {
                    if (flag)
                    {
                        this.txtHeight1.Text = "0.156";
                        this.txtHeight2.Text = "2.93";
                        this.txtHeight3.Text = "5.71";
                        this.txtHeight4.Text = "8.48";
                    }
                    else
                    {
                        this.txtHeight1.Text = "0.156";
                        this.txtHeight2.Text = "3.152";
                        this.txtHeight3.Text = "6.148";
                        this.txtHeight4.Text = "9.144";
                    }
                }
                else if (flag)
                {
                    this.txtHeight1.Text = "0.16";
                    this.txtHeight2.Text = "3.88";
                    this.txtHeight3.Text = "7.6";
                }
                else
                {
                    this.txtHeight1.Text = "0.16";
                    this.txtHeight2.Text = "4.214";
                    this.txtHeight3.Text = "8.27";
                }
                if (this.rd4UpPrinting.Checked)
                {
                    this.txtHeight4.Enabled = true;
                }
                else
                {
                    this.txtHeight4.Text = "";
                    this.txtHeight4.Enabled = false;
                }
                calculateNumberOfPages();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PrintOptionChangedEvent), nameof(TroyMICRDocGenerator));
            }
        }

        private void InitCheckTab()
        {
            try
            {
                this.InitCheckTabControls();
                this.PopulateMICRFonts();
                this._jobChanged = false;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(InitCheckTab), nameof(TroyMICRDocGenerator));
            }
        }

        private void PopulateInstalledFonts()
        {
            try
            {
                this.cbMICRSourceFont.Items.Clear();
                FontFamily[] families = new InstalledFontCollection().Families;
                int length = families.Length;
                for (int index = 0; index < length; ++index)
                    this.cbMICRSourceFont.Items.Add((object)families[index].Name);
                if (length <= 0)
                    return;
                this.cbMICRSourceFont.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PopulateInstalledFonts), nameof(TroyMICRDocGenerator));
            }
        }

        private void PopulateMICRFonts()
        {
            try
            {
                string[] files = Directory.GetFiles(TroyMICRDocGenerator._appPath + "\\MICRFONT");
                this.cbMICRSourceFont.Items.Clear();
                this._fontTable.Clear();
                for (int index = 0; index < files.Length; ++index)
                {
                    string fileName = Path.GetFileName(files[index]);
                    this._fontTable.Add((object)fileName, (object)files[index]);
                    this.cbMICRSourceFont.Items.Add((object)fileName);
                }
                if (files.Length <= 0)
                    return;
                this.cbMICRSourceFont.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(PopulateMICRFonts), nameof(TroyMICRDocGenerator));
            }
        }

        private void InitCheckTabControls()
        {
            try
            {
                this.txtCheckNo.Text = "";
                this.txtNumberOfPages.Text = "";
                this.micrIDCtrl.AmountField = "";
                this.micrIDCtrl.OnUSField = "";
                this.micrIDCtrl.AuxOnUSField = "";
                this.micrIDCtrl.TransitField = "";
                this.micrIDCtrl.StartAutoIncrAmountAt = 0;
                this.micrIDCtrl.StartAutoIncrAuxOnUSAt = 0;
                this.micrIDCtrl.StartAutoIncrOnUSAt = 0;
                this.micrIDCtrl.StartAutoIncrTransitAt = 0;
                this.rdSoftFont.Checked = true;
                this.rdResidentFont.Checked = false;
                this.rdInstalledFont.Checked = false;
                this.txtResidentFontID.Text = "";
                this.txtResidentFontID.Enabled = false;
                this.cbMICRSourceFont.Enabled = true;
                if (this.cbMICRSourceFont.Items.Count > 0)
                    this.cbMICRSourceFont.SelectedIndex = 0;
                this.numericUpDownLeftMargin.Value = 50M;
                this.numericUpDownTopMargin.Value = 1M;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "ClearCheckTabControls", nameof(TroyMICRDocGenerator));
            }
        }

        private void GetCheckTabUserSettings(ref JobDetails jobDetail)
        {
            try
            {
                if (this.rdResidentFont.Checked)
                {
                    jobDetail.FontOption = 0;
                    jobDetail.FontId = this.txtResidentFontID.Text;
                }
                else if (this.rdInstalledFont.Checked)
                {
                    jobDetail.FontOption = 2;
                    jobDetail.FontName = this.cbMICRSourceFont.GetItemText(this.cbMICRSourceFont.SelectedItem);
                }
                else
                {
                    jobDetail.FontOption = 1;
                    jobDetail.FontFile = (string)this._fontTable[(object)this.cbMICRSourceFont.GetItemText(this.cbMICRSourceFont.SelectedItem)];
                }
                jobDetail.AuxOnUSField = this.micrIDCtrl.AuxOnUSField;
                jobDetail.AmountField = this.micrIDCtrl.AmountField;
                jobDetail.TransitField = this.micrIDCtrl.TransitField;
                jobDetail.OnUSField = this.micrIDCtrl.OnUSField;
                jobDetail.NoOfChecks = int.Parse(this.txtCheckNo.Text);
                jobDetail.StartAtAmountField = this.micrIDCtrl.StartAutoIncrAmountAt;
                jobDetail.StartAtAuxOnUSField = this.micrIDCtrl.StartAutoIncrAuxOnUSAt;
                jobDetail.StartAtTransitField = this.micrIDCtrl.StartAutoIncrTransitAt;
                jobDetail.StartAtOnUSField = this.micrIDCtrl.StartAutoIncrOnUSAt;
                jobDetail.LeftMargin = (double)this.numericUpDownLeftMargin.Value;
                jobDetail.TopMargin = (double)this.numericUpDownTopMargin.Value;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetCheckTabUserSettings), nameof(TroyMICRDocGenerator));
            }
        }

        private bool ValidateCheckTabValues(ArrayList errorList)
        {
            bool flag = true;
            try
            {
                if (!this.rdResidentFont.Checked && !this.rdInstalledFont.Checked && !this.rdSoftFont.Checked && !this.rdEscapeSequence.Checked)
                {
                    errorList.Add((object)"MICR Source Font");
                    flag = false;
                }
                if (this.rdResidentFont.Checked && (this.txtResidentFontID.Text == null || this.txtResidentFontID.Text.Length == 0))
                {
                    errorList.Add((object)"Resident Font Id");
                    flag = false;
                }
                if ((this.rdInstalledFont.Checked || this.rdSoftFont.Checked) && (this.cbMICRSourceFont.SelectedIndex == -1 || this.cbMICRSourceFont.SelectedItem == null))
                {
                    errorList.Add((object)"Font Name");
                    flag = false;
                }
                if (this.micrIDCtrl.AuxOnUSField.IndexOf('#') != -1 && this.micrIDCtrl.StartAutoIncrAuxOnUSAt == -1)
                {
                    errorList.Add((object)"Aux On-Us Field (Auto Incr. start value)");
                    flag = false;
                }
                if (this.micrIDCtrl.TransitField.IndexOf('#') != -1 && this.micrIDCtrl.StartAutoIncrTransitAt == -1)
                {
                    errorList.Add((object)"Transit Field (Auto Incr. start value)");
                    flag = false;
                }
                if (this.micrIDCtrl.OnUSField.IndexOf('#') != -1 && this.micrIDCtrl.StartAutoIncrOnUSAt == -1)
                {
                    errorList.Add((object)"ON US Field (Auto Incr. start value)");
                    flag = false;
                }
                if (this.micrIDCtrl.AmountField.IndexOf('#') != -1 && this.micrIDCtrl.StartAutoIncrAmountAt == -1)
                {
                    errorList.Add((object)"Amount Field (Auto Incr. start value)");
                    flag = false;
                }
                if (this.txtCheckNo.Text == null || this.txtCheckNo.Text.Length == 0)
                {
                    errorList.Add((object)"No. Of Checks");
                    flag = false;
                }
                else if (this.txtCheckNo.Text.Contains("-"))
                {
                    errorList.Add((object)"No. Of Checks");
                    flag = false;
                }
                else if (!this.IsValidNumber(this.txtCheckNo.Text))
                {
                    errorList.Add((object)"No. Of Checks");
                    flag = false;
                }
                return flag;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "ValidatePrinterTabValues", nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private bool IsValidNumber(string userEntry)
        {
            try
            {
                int.Parse(userEntry);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, "IsValidNumber for " + userEntry, nameof(TroyMICRDocGenerator));
                return false;
            }
        }

        private void DisplayJobFileValuesOnCheckTab(JobDetails jobDetails)
        {
            try
            {
                switch (jobDetails.FontOption)
                {
                    case 0:
                        this.txtResidentFontID.Enabled = true;
                        this.txtResidentFontID.Text = jobDetails.FontId;
                        this.cbMICRSourceFont.Enabled = false;
                        this.rdInstalledFont.Checked = false;
                        this.rdResidentFont.Checked = true;
                        this.rdSoftFont.Checked = false;
                        break;
                    case 1:
                        this.txtResidentFontID.Enabled = false;
                        this.txtResidentFontID.Text = "";
                        this.cbMICRSourceFont.Enabled = true;
                        this.rdInstalledFont.Checked = false;
                        this.rdResidentFont.Checked = false;
                        this.rdSoftFont.Checked = true;
                        this.PopulateMICRFonts();
                        int num1 = this.cbMICRSourceFont.FindString(Path.GetFileName(jobDetails.FontFile));
                        if (num1 != -1)
                        {
                            this.cbMICRSourceFont.SelectedIndex = num1;
                            break;
                        }
                        this.cbMICRSourceFont.SelectedIndex = 0;
                        this.tabMain.SelectedIndex = 1;
                        int num2 = (int)MessageBox.Show("Could not find the soft font file \"" + jobDetails.FontFile + "\" on the system.", "Troy MICR Document Generator", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        break;
                    case 2:
                        this.txtResidentFontID.Enabled = false;
                        this.txtResidentFontID.Text = "";
                        this.cbMICRSourceFont.Enabled = true;
                        this.rdInstalledFont.Checked = true;
                        this.rdResidentFont.Checked = false;
                        this.rdSoftFont.Checked = false;
                        this.PopulateInstalledFonts();
                        int num3 = this.cbMICRSourceFont.FindString(jobDetails.FontName);
                        if (num3 != -1)
                        {
                            this.cbMICRSourceFont.SelectedIndex = num3;
                            break;
                        }
                        this.cbMICRSourceFont.SelectedIndex = 0;
                        this.tabMain.SelectedIndex = 1;
                        int num4 = (int)MessageBox.Show("Could not find the installed font \"" + jobDetails.FontName + "\" on the system.", "Troy MICR Document Generator", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        break;
                }
                this.micrIDCtrl.AmountField = jobDetails.AmountField;
                this.micrIDCtrl.AuxOnUSField = jobDetails.AuxOnUSField;
                this.micrIDCtrl.OnUSField = jobDetails.OnUSField;
                this.micrIDCtrl.TransitField = jobDetails.TransitField;
                this.micrIDCtrl.StartAutoIncrAmountAt = jobDetails.StartAtAmountField;
                this.micrIDCtrl.StartAutoIncrAuxOnUSAt = jobDetails.StartAtAuxOnUSField;
                this.micrIDCtrl.StartAutoIncrTransitAt = jobDetails.StartAtTransitField;
                this.micrIDCtrl.StartAutoIncrOnUSAt = jobDetails.StartAtOnUSField;
                this.numericUpDownLeftMargin.Value = (Decimal)jobDetails.LeftMargin;
                this.numericUpDownTopMargin.Value = (Decimal)jobDetails.TopMargin;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(DisplayJobFileValuesOnCheckTab), nameof(TroyMICRDocGenerator));
            }
        }
        private void FontOptionSelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this._jobChanged = true;
                if (this.rdInstalledFont.Checked)
                {
                    this.txtResidentFontID.Enabled = false;
                    this.cbMICRSourceFontEscapeSequence.Enabled = false;
                    this.cbMICRSourceFontEscapeSequence.SelectedIndex = -1;
                    this.cbMICRSourceFont.Enabled = true;
                    this.PopulateInstalledFonts();
                }
                else if (this.rdSoftFont.Checked)
                {
                    this.txtResidentFontID.Enabled = false;
                    this.cbMICRSourceFontEscapeSequence.Enabled = false;
                    this.cbMICRSourceFontEscapeSequence.SelectedIndex = -1;
                    this.cbMICRSourceFont.Enabled = true;
                    this.PopulateMICRFonts();
                }
                else if(this.rdEscapeSequence.Checked)
                {
                    this.txtResidentFontID.Enabled = false;
                    this.cbMICRSourceFont.Enabled = false;
                    this.cbMICRSourceFontEscapeSequence.Enabled = true;
                }
                else
                {
                    if (!this.rdResidentFont.Checked)
                        return;
                    this.cbMICRSourceFont.Enabled = false;
                    this.cbMICRSourceFontEscapeSequence.Enabled = false;
                    this.cbMICRSourceFontEscapeSequence.SelectedIndex = -1;
                    this.txtResidentFontID.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(FontOptionSelectionChanged), nameof(TroyMICRDocGenerator));
            }
        }

        public enum Enum_PaperSize
        {
            Letter,
            A4,
        }

        public enum Enum_PaperSource
        {
            Automatic,
            Tray1,
            Tray2,
        }

        public enum Enum_PaperOutput
        {
            Automatic,
            Tray1,
            Tray2,
        }

        public enum Enum_PrintOptions
        {
            Print_3_Up,
            Print_4_Up,
        }

        public enum Enum_FontOptions
        {
            ResidentFont,
            SoftFont,
            InstalledFont,
        }

        private void ignoreSourceCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(ignoreSourceCheckBox.Checked)
            {
                cbPaperSource.Enabled = false;
                cbPaperSource.SelectedIndex = -1;
            }
            else
            {
                cbPaperSource.Enabled = true;
                cbPaperSource.SelectedIndex = 0;
            }
        }

        private void printToryFontListButton_Click(object sender, EventArgs e)
        {
            PrintToSpooler.SendFileToPrinter(cbPrinterName.Text, "TroyPclFontList.prn", "PCL Font List");
        }
    }
}
