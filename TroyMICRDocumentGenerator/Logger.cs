﻿using System;
using System.IO;

namespace TroyMICRDocumentGenerator
{
    public class Logger
    {
        public static void LogMessage(Exception objException, string strFunction, string strClass)
        {
            try
            {
                string path = TroyMICRDocGenerator.ApplicationPath + "\\TroyMICRLog.txt";
                if (!File.Exists(path))
                    File.Create(path).Close();
                StreamWriter streamWriter = File.AppendText(path);
                streamWriter.WriteLine("----------------------------Exception-----------------------------");
                streamWriter.WriteLine("Class:      " + strClass);
                streamWriter.WriteLine("Function:   " + strFunction);
                streamWriter.WriteLine("Source:     " + objException.Source.ToString().Trim());
                streamWriter.WriteLine("Method:     " + objException.TargetSite.ToString());
                streamWriter.WriteLine("Date:       " + DateTime.Now.ToLongTimeString());
                streamWriter.WriteLine("Time:       " + DateTime.Now.ToShortDateString());
                streamWriter.WriteLine("Error:      " + objException.Message.Trim());
                streamWriter.WriteLine("StackTrace: " + objException.StackTrace.Trim());
                streamWriter.WriteLine("----------------------------Exception-----------------------------");
                streamWriter.WriteLine();
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public static void LogMessage(string Message, string strFunction, string strClass)
        {
            try
            {
                string path = TroyMICRDocGenerator.ApplicationPath + "\\TroyMICRLog.txt";
                if (!File.Exists(path))
                    File.Create(path).Close();
                StreamWriter streamWriter = File.AppendText(path);
                streamWriter.WriteLine("[" + (object)DateTime.Now + "]Class: " + strClass + "  Function: " + strFunction);
                streamWriter.WriteLine("Message: " + Message);
                streamWriter.WriteLine();
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public static void LogMessage(string Message)
        {
            try
            {
                string path = TroyMICRDocGenerator.ApplicationPath + "\\TroyMICRLog.txt";
                if (!File.Exists(path))
                    File.Create(path).Close();
                StreamWriter streamWriter = File.AppendText(path);
                streamWriter.WriteLine("[" + (object)DateTime.Now.Hour + "." + (object)DateTime.Now.Minute + "." + (object)DateTime.Now.Second + "." + (object)DateTime.Now.Millisecond + "]" + Message);
                streamWriter.WriteLine();
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch (Exception ex)
            {
            }
        }
    }
}
