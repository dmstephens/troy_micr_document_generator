﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace TroyMICRDocumentGenerator
{
    partial class JobList
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.lstJobList = new ListBox();
            this.btnOk = new Button();
            this.btnCancel = new Button();
            this.SuspendLayout();
            this.lstJobList.FormattingEnabled = true;
            this.lstJobList.Location = new Point(13, 13);
            this.lstJobList.Name = "lstJobList";
            this.lstJobList.Size = new Size(217, 251);
            this.lstJobList.TabIndex = 0;
            this.btnOk.Location = new Point(35, 270);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new EventHandler(this.OKButtonClickedEvent);
            this.btnCancel.Location = new Point(129, 270);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new EventHandler(this.CancelButtonClickedEvent);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(243, 303);
            this.Controls.Add((Control)this.btnCancel);
            this.Controls.Add((Control)this.btnOk);
            this.Controls.Add((Control)this.lstJobList);
            this.MaximizeBox = false;
            this.MaximumSize = new Size(251, 337);
            this.MinimizeBox = false;
            this.MinimumSize = new Size(251, 337);
            this.Name = "JobList";
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "JobList";
            this.ResumeLayout(false);
        }
    }
}
