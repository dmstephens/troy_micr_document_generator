﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace TroyMICRDocumentGenerator
{
    public partial class MICRIDControl : UserControl
    {
 

        public string AuxOnUSField
        {
            get => this.GetAuxOnUSFieldValue();
            set => this.SetAuxOnUSFieldValue(value);
        }

        public string TransitField
        {
            get => this.GetTransitFieldValue();
            set => this.SetTransitFieldValue(value);
        }

        public string OnUSField
        {
            get => this.GetOnUSFieldValue();
            set => this.SetOnUSFieldValue(value);
        }

        public string AmountField
        {
            get => this.GetAmountFieldValue();
            set => this.SetAmountFieldValue(value);
        }

        public int StartAutoIncrAuxOnUSAt
        {
            get => this.txtStartAuxOnUS.Text.Length == 0 ? -1 : int.Parse(this.txtStartAuxOnUS.Text);
            set
            {
                if (value == -1)
                    return;
                this.txtStartAuxOnUS.Text = value.ToString();
            }
        }

        public int StartAutoIncrTransitAt
        {
            get => this.txtStartTransit.Text.Length == 0 ? -1 : int.Parse(this.txtStartTransit.Text);
            set
            {
                if (value == -1)
                    return;
                this.txtStartTransit.Text = value.ToString();
            }
        }

        public int StartAutoIncrOnUSAt
        {
            get => this.txtStartOnUS.Text.Length == 0 ? -1 : int.Parse(this.txtStartOnUS.Text);
            set
            {
                if (value == -1)
                    return;
                this.txtStartOnUS.Text = value.ToString();
            }
        }

        public int StartAutoIncrAmountAt
        {
            get => this.txtStartAmount.Text.Length == 0 ? -1 : int.Parse(this.txtStartAmount.Text);
            set
            {
                if (value == -1)
                    return;
                this.txtStartAmount.Text = value.ToString();
            }
        }

        public MICRIDControl() => this.InitializeComponent();

        public event MicrIdControlChangedDelegate MICRIdChanged;

        public void OnMICRIdChanged()
        {
            if (this.MICRIdChanged == null)
                return;
            this.MICRIdChanged();
        }

        private string GetAmountFieldValue()
        {
            try
            {
                string str1 = "";
                string str2 = this.txt12.Text.Trim().Length != 0 ? str1 + this.txt12.Text.Trim() : str1 + " ";
                string str3 = this.txt11.Text.Trim().Length != 0 ? str2 + this.txt11.Text.Trim() : str2 + " ";
                string str4 = this.txt10.Text.Trim().Length != 0 ? str3 + this.txt10.Text.Trim() : str3 + " ";
                string str5 = this.txt9.Text.Trim().Length != 0 ? str4 + this.txt9.Text.Trim() : str4 + " ";
                string str6 = this.txt8.Text.Trim().Length != 0 ? str5 + this.txt8.Text.Trim() : str5 + " ";
                string str7 = this.txt7.Text.Trim().Length != 0 ? str6 + this.txt7.Text.Trim() : str6 + " ";
                string str8 = this.txt6.Text.Trim().Length != 0 ? str7 + this.txt6.Text.Trim() : str7 + " ";
                string str9 = this.txt5.Text.Trim().Length != 0 ? str8 + this.txt5.Text.Trim() : str8 + " ";
                string str10 = this.txt4.Text.Trim().Length != 0 ? str9 + this.txt4.Text.Trim() : str9 + " ";
                string str11 = this.txt3.Text.Trim().Length != 0 ? str10 + this.txt3.Text.Trim() : str10 + " ";
                string str12 = this.txt2.Text.Trim().Length != 0 ? str11 + this.txt2.Text.Trim() : str11 + " ";
                return this.txt1.Text.Trim().Length != 0 ? str12 + this.txt1.Text.Trim() : str12 + " ";
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetAmountFieldValue), nameof(MICRIDControl));
                return (string)null;
            }
        }

        private void SetAmountFieldValue(string AmountFieldVal)
        {
            try
            {
                int length = AmountFieldVal.Length;
                for (int index = 0; index < 12 - length; ++index)
                    AmountFieldVal = " " + AmountFieldVal;
                this.txt12.Text = AmountFieldVal[0].ToString();
                if (AmountFieldVal[0] == '#')
                    this.txt12.BackColor = Color.LightBlue;
                else
                    this.txt12.BackColor = Color.White;
                this.txt11.Text = AmountFieldVal[1].ToString();
                if (AmountFieldVal[1] == '#')
                    this.txt11.BackColor = Color.LightBlue;
                else
                    this.txt11.BackColor = Color.White;
                this.txt10.Text = AmountFieldVal[2].ToString();
                if (AmountFieldVal[2] == '#')
                    this.txt10.BackColor = Color.LightBlue;
                else
                    this.txt10.BackColor = Color.White;
                this.txt9.Text = AmountFieldVal[3].ToString();
                if (AmountFieldVal[3] == '#')
                    this.txt9.BackColor = Color.LightBlue;
                else
                    this.txt9.BackColor = Color.White;
                this.txt8.Text = AmountFieldVal[4].ToString();
                if (AmountFieldVal[4] == '#')
                    this.txt8.BackColor = Color.LightBlue;
                else
                    this.txt8.BackColor = Color.White;
                this.txt7.Text = AmountFieldVal[5].ToString();
                if (AmountFieldVal[5] == '#')
                    this.txt7.BackColor = Color.LightBlue;
                else
                    this.txt7.BackColor = Color.White;
                this.txt6.Text = AmountFieldVal[6].ToString();
                if (AmountFieldVal[6] == '#')
                    this.txt6.BackColor = Color.LightBlue;
                else
                    this.txt6.BackColor = Color.White;
                this.txt5.Text = AmountFieldVal[7].ToString();
                if (AmountFieldVal[7] == '#')
                    this.txt5.BackColor = Color.LightBlue;
                else
                    this.txt5.BackColor = Color.White;
                this.txt4.Text = AmountFieldVal[8].ToString();
                if (AmountFieldVal[8] == '#')
                    this.txt4.BackColor = Color.LightBlue;
                else
                    this.txt4.BackColor = Color.White;
                this.txt3.Text = AmountFieldVal[9].ToString();
                if (AmountFieldVal[9] == '#')
                    this.txt3.BackColor = Color.LightBlue;
                else
                    this.txt3.BackColor = Color.White;
                this.txt2.Text = AmountFieldVal[10].ToString();
                if (AmountFieldVal[10] == '#')
                    this.txt2.BackColor = Color.LightBlue;
                else
                    this.txt2.BackColor = Color.White;
                this.txt1.Text = AmountFieldVal[11].ToString();
                if (AmountFieldVal[11] == '#')
                    this.txt1.BackColor = Color.LightBlue;
                else
                    this.txt1.BackColor = Color.White;
                this.ConstraintStartAtValues();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetAmountFieldValue), nameof(MICRIDControl));
            }
        }

        private string GetTransitFieldValue()
        {
            try
            {
                string str1 = "";
                string str2 = this.txt43.Text.Trim().Length != 0 ? this.txt43.Text.Trim() : str1 + " ";
                string str3 = this.txt42.Text.Trim().Length != 0 ? str2 + this.txt42.Text.Trim() : str2 + " ";
                string str4 = this.txt41.Text.Trim().Length != 0 ? str3 + this.txt41.Text.Trim() : str3 + " ";
                string str5 = this.txt40.Text.Trim().Length != 0 ? str4 + this.txt40.Text.Trim() : str4 + " ";
                string str6 = this.txt39.Text.Trim().Length != 0 ? str5 + this.txt39.Text.Trim() : str5 + " ";
                string str7 = this.txt38.Text.Trim().Length != 0 ? str6 + this.txt38.Text.Trim() : str6 + " ";
                string str8 = this.txt37.Text.Trim().Length != 0 ? str7 + this.txt37.Text.Trim() : str7 + " ";
                string str9 = this.txt36.Text.Trim().Length != 0 ? str8 + this.txt36.Text.Trim() : str8 + " ";
                string str10 = this.txt35.Text.Trim().Length != 0 ? str9 + this.txt35.Text.Trim() : str9 + " ";
                string str11 = this.txt34.Text.Trim().Length != 0 ? str10 + this.txt34.Text.Trim() : str10 + " ";
                return this.txt33.Text.Trim().Length != 0 ? str11 + this.txt33.Text.Trim() : str11 + " ";
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetTransitFieldValue), nameof(MICRIDControl));
                return (string)null;
            }
        }

        private void SetTransitFieldValue(string TransitFieldVal)
        {
            try
            {
                int length = TransitFieldVal.Length;
                for (int index = 0; index < 11 - length; ++index)
                    TransitFieldVal = " " + TransitFieldVal;
                this.txt43.Text = TransitFieldVal[0].ToString();
                if (TransitFieldVal[0] == '#')
                    this.txt43.BackColor = Color.LightBlue;
                else
                    this.txt43.BackColor = Color.White;
                this.txt42.Text = TransitFieldVal[1].ToString();
                if (TransitFieldVal[1] == '#')
                    this.txt42.BackColor = Color.LightBlue;
                else
                    this.txt42.BackColor = Color.White;
                this.txt41.Text = TransitFieldVal[2].ToString();
                if (TransitFieldVal[2] == '#')
                    this.txt41.BackColor = Color.LightBlue;
                else
                    this.txt41.BackColor = Color.White;
                this.txt40.Text = TransitFieldVal[3].ToString();
                if (TransitFieldVal[3] == '#')
                    this.txt40.BackColor = Color.LightBlue;
                else
                    this.txt40.BackColor = Color.White;
                this.txt39.Text = TransitFieldVal[4].ToString();
                if (TransitFieldVal[4] == '#')
                    this.txt39.BackColor = Color.LightBlue;
                else
                    this.txt39.BackColor = Color.White;
                this.txt38.Text = TransitFieldVal[5].ToString();
                if (TransitFieldVal[5] == '#')
                    this.txt38.BackColor = Color.LightBlue;
                else
                    this.txt38.BackColor = Color.White;
                this.txt37.Text = TransitFieldVal[6].ToString();
                if (TransitFieldVal[6] == '#')
                    this.txt37.BackColor = Color.LightBlue;
                else
                    this.txt37.BackColor = Color.White;
                this.txt36.Text = TransitFieldVal[7].ToString();
                if (TransitFieldVal[7] == '#')
                    this.txt36.BackColor = Color.LightBlue;
                else
                    this.txt36.BackColor = Color.White;
                this.txt35.Text = TransitFieldVal[8].ToString();
                if (TransitFieldVal[8] == '#')
                    this.txt35.BackColor = Color.LightBlue;
                else
                    this.txt35.BackColor = Color.White;
                this.txt34.Text = TransitFieldVal[9].ToString();
                if (TransitFieldVal[9] == '#')
                    this.txt34.BackColor = Color.LightBlue;
                else
                    this.txt34.BackColor = Color.White;
                this.txt33.Text = TransitFieldVal[10].ToString();
                if (TransitFieldVal[10] == '#')
                    this.txt33.BackColor = Color.LightBlue;
                else
                    this.txt33.BackColor = Color.White;
                this.ConstraintStartAtValues();
            }
            catch (Exception ex)
            {
               Logger.LogMessage(ex, nameof(SetTransitFieldValue), nameof(MICRIDControl));
            }
        }

        private string GetOnUSFieldValue()
        {
            try
            {
                string str1 = "";
                string str2 = this.txt32.Text.Trim().Length != 0 ? str1 + this.txt32.Text.Trim() : str1 + " ";
                string str3 = this.txt31.Text.Trim().Length != 0 ? str2 + this.txt31.Text.Trim() : str2 + " ";
                string str4 = this.txt30.Text.Trim().Length != 0 ? str3 + this.txt30.Text.Trim() : str3 + " ";
                string str5 = this.txt29.Text.Trim().Length != 0 ? str4 + this.txt29.Text.Trim() : str4 + " ";
                string str6 = this.txt28.Text.Trim().Length != 0 ? str5 + this.txt28.Text.Trim() : str5 + " ";
                string str7 = this.txt27.Text.Trim().Length != 0 ? str6 + this.txt27.Text.Trim() : str6 + " ";
                string str8 = this.txt26.Text.Trim().Length != 0 ? str7 + this.txt26.Text.Trim() : str7 + " ";
                string str9 = this.txt25.Text.Trim().Length != 0 ? str8 + this.txt25.Text.Trim() : str8 + " ";
                string str10 = this.txt24.Text.Trim().Length != 0 ? str9 + this.txt24.Text.Trim() : str9 + " ";
                string str11 = this.txt23.Text.Trim().Length != 0 ? str10 + this.txt23.Text.Trim() : str10 + " ";
                string str12 = this.txt22.Text.Trim().Length != 0 ? str11 + this.txt22.Text.Trim() : str11 + " ";
                string str13 = this.txt21.Text.Trim().Length != 0 ? str12 + this.txt21.Text.Trim() : str12 + " ";
                string str14 = this.txt20.Text.Trim().Length != 0 ? str13 + this.txt20.Text.Trim() : str13 + " ";
                string str15 = this.txt19.Text.Trim().Length != 0 ? str14 + this.txt19.Text.Trim() : str14 + " ";
                string str16 = this.txt18.Text.Trim().Length != 0 ? str15 + this.txt18.Text.Trim() : str15 + " ";
                string str17 = this.txt17.Text.Trim().Length != 0 ? str16 + this.txt17.Text.Trim() : str16 + " ";
                string str18 = this.txt16.Text.Trim().Length != 0 ? str17 + this.txt16.Text.Trim() : str17 + " ";
                string str19 = this.txt15.Text.Trim().Length != 0 ? str18 + this.txt15.Text.Trim() : str18 + " ";
                string str20 = this.txt14.Text.Trim().Length != 0 ? str19 + this.txt14.Text.Trim() : str19 + " ";
                return this.txt13.Text.Trim().Length != 0 ? str20 + this.txt13.Text.Trim() : str20 + " ";
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetOnUSFieldValue), nameof(MICRIDControl));
                return (string)null;
            }
        }

        private void SetOnUSFieldValue(string OnUSFieldVal)
        {
            try
            {
                int length = OnUSFieldVal.Length;
                for (int index = 0; index < 20 - length; ++index)
                    OnUSFieldVal = " " + OnUSFieldVal;
                this.txt32.Text = OnUSFieldVal[0].ToString();
                if (OnUSFieldVal[0] == '#')
                    this.txt32.BackColor = Color.LightBlue;
                else
                    this.txt32.BackColor = Color.White;
                this.txt31.Text = OnUSFieldVal[1].ToString();
                if (OnUSFieldVal[1] == '#')
                    this.txt31.BackColor = Color.LightBlue;
                else
                    this.txt31.BackColor = Color.White;
                this.txt30.Text = OnUSFieldVal[2].ToString();
                if (OnUSFieldVal[2] == '#')
                    this.txt30.BackColor = Color.LightBlue;
                else
                    this.txt30.BackColor = Color.White;
                this.txt29.Text = OnUSFieldVal[3].ToString();
                if (OnUSFieldVal[3] == '#')
                    this.txt29.BackColor = Color.LightBlue;
                else
                    this.txt29.BackColor = Color.White;
                this.txt28.Text = OnUSFieldVal[4].ToString();
                if (OnUSFieldVal[4] == '#')
                    this.txt28.BackColor = Color.LightBlue;
                else
                    this.txt28.BackColor = Color.White;
                this.txt27.Text = OnUSFieldVal[5].ToString();
                if (OnUSFieldVal[5] == '#')
                    this.txt27.BackColor = Color.LightBlue;
                else
                    this.txt27.BackColor = Color.White;
                this.txt26.Text = OnUSFieldVal[6].ToString();
                if (OnUSFieldVal[6] == '#')
                    this.txt26.BackColor = Color.LightBlue;
                else
                    this.txt26.BackColor = Color.White;
                this.txt25.Text = OnUSFieldVal[7].ToString();
                if (OnUSFieldVal[7] == '#')
                    this.txt25.BackColor = Color.LightBlue;
                else
                    this.txt25.BackColor = Color.White;
                this.txt24.Text = OnUSFieldVal[8].ToString();
                if (OnUSFieldVal[8] == '#')
                    this.txt24.BackColor = Color.LightBlue;
                else
                    this.txt24.BackColor = Color.White;
                this.txt23.Text = OnUSFieldVal[9].ToString();
                if (OnUSFieldVal[9] == '#')
                    this.txt23.BackColor = Color.LightBlue;
                else
                    this.txt23.BackColor = Color.White;
                this.txt22.Text = OnUSFieldVal[10].ToString();
                if (OnUSFieldVal[10] == '#')
                    this.txt22.BackColor = Color.LightBlue;
                else
                    this.txt22.BackColor = Color.White;
                this.txt21.Text = OnUSFieldVal[11].ToString();
                if (OnUSFieldVal[11] == '#')
                    this.txt21.BackColor = Color.LightBlue;
                else
                    this.txt21.BackColor = Color.White;
                this.txt20.Text = OnUSFieldVal[12].ToString();
                if (OnUSFieldVal[12] == '#')
                    this.txt20.BackColor = Color.LightBlue;
                else
                    this.txt20.BackColor = Color.White;
                this.txt19.Text = OnUSFieldVal[13].ToString();
                if (OnUSFieldVal[13] == '#')
                    this.txt19.BackColor = Color.LightBlue;
                else
                    this.txt19.BackColor = Color.White;
                this.txt18.Text = OnUSFieldVal[14].ToString();
                if (OnUSFieldVal[14] == '#')
                    this.txt18.BackColor = Color.LightBlue;
                else
                    this.txt18.BackColor = Color.White;
                this.txt17.Text = OnUSFieldVal[15].ToString();
                if (OnUSFieldVal[15] == '#')
                    this.txt17.BackColor = Color.LightBlue;
                else
                    this.txt17.BackColor = Color.White;
                this.txt16.Text = OnUSFieldVal[16].ToString();
                if (OnUSFieldVal[16] == '#')
                    this.txt16.BackColor = Color.LightBlue;
                else
                    this.txt16.BackColor = Color.White;
                this.txt15.Text = OnUSFieldVal[17].ToString();
                if (OnUSFieldVal[17] == '#')
                    this.txt15.BackColor = Color.LightBlue;
                else
                    this.txt15.BackColor = Color.White;
                this.txt14.Text = OnUSFieldVal[18].ToString();
                if (OnUSFieldVal[18] == '#')
                    this.txt14.BackColor = Color.LightBlue;
                else
                    this.txt14.BackColor = Color.White;
                this.txt13.Text = OnUSFieldVal[19].ToString();
                if (OnUSFieldVal[19] == '#')
                    this.txt13.BackColor = Color.LightBlue;
                else
                    this.txt13.BackColor = Color.White;
                this.ConstraintStartAtValues();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetOnUSFieldValue), nameof(MICRIDControl));
            }
        }

        private string GetAuxOnUSFieldValue()
        {
            try
            {
                string str1 = "";
                string str2 = this.txt58.Text.Trim().Length != 0 ? str1 + this.txt58.Text.Trim() : str1 + " ";
                string str3 = this.txt57.Text.Trim().Length != 0 ? str2 + this.txt57.Text.Trim() : str2 + " ";
                string str4 = this.txt56.Text.Trim().Length != 0 ? str3 + this.txt56.Text.Trim() : str3 + " ";
                string str5 = this.txt55.Text.Trim().Length != 0 ? str4 + this.txt55.Text.Trim() : str4 + " ";
                string str6 = this.txt54.Text.Trim().Length != 0 ? str5 + this.txt54.Text.Trim() : str5 + " ";
                string str7 = this.txt53.Text.Trim().Length != 0 ? str6 + this.txt53.Text.Trim() : str6 + " ";
                string str8 = this.txt52.Text.Trim().Length != 0 ? str7 + this.txt52.Text.Trim() : str7 + " ";
                string str9 = this.txt51.Text.Trim().Length != 0 ? str8 + this.txt51.Text.Trim() : str8 + " ";
                string str10 = this.txt50.Text.Trim().Length != 0 ? str9 + this.txt50.Text.Trim() : str9 + " ";
                string str11 = this.txt49.Text.Trim().Length != 0 ? str10 + this.txt49.Text.Trim() : str10 + " ";
                string str12 = this.txt48.Text.Trim().Length != 0 ? str11 + this.txt48.Text.Trim() : str11 + " ";
                string str13 = this.txt47.Text.Trim().Length != 0 ? str12 + this.txt47.Text.Trim() : str12 + " ";
                string str14 = this.txt46.Text.Trim().Length != 0 ? str13 + this.txt46.Text.Trim() : str13 + " ";
                string str15 = this.txt45.Text.Trim().Length != 0 ? str14 + this.txt45.Text.Trim() : str14 + " ";
                return this.txt44.Text.Trim().Length != 0 ? str15 + this.txt44.Text.Trim() : str15 + " ";
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(GetAuxOnUSFieldValue), nameof(MICRIDControl));
                return (string)null;
            }
        }

        private void SetAuxOnUSFieldValue(string AuxOnUSFieldVal)
        {
            try
            {
                int length = AuxOnUSFieldVal.Length;
                for (int index = 0; index < 15 - length; ++index)
                    AuxOnUSFieldVal = " " + AuxOnUSFieldVal;
                this.txt58.Text = AuxOnUSFieldVal[0].ToString();
                if (AuxOnUSFieldVal[0] == '#')
                    this.txt58.BackColor = Color.LightBlue;
                else
                    this.txt58.BackColor = Color.White;
                this.txt57.Text = AuxOnUSFieldVal[1].ToString();
                if (AuxOnUSFieldVal[1] == '#')
                    this.txt57.BackColor = Color.LightBlue;
                else
                    this.txt57.BackColor = Color.White;
                this.txt56.Text = AuxOnUSFieldVal[2].ToString();
                if (AuxOnUSFieldVal[2] == '#')
                    this.txt56.BackColor = Color.LightBlue;
                else
                    this.txt56.BackColor = Color.White;
                this.txt55.Text = AuxOnUSFieldVal[3].ToString();
                if (AuxOnUSFieldVal[3] == '#')
                    this.txt55.BackColor = Color.LightBlue;
                else
                    this.txt55.BackColor = Color.White;
                this.txt54.Text = AuxOnUSFieldVal[4].ToString();
                if (AuxOnUSFieldVal[4] == '#')
                    this.txt54.BackColor = Color.LightBlue;
                else
                    this.txt54.BackColor = Color.White;
                this.txt53.Text = AuxOnUSFieldVal[5].ToString();
                if (AuxOnUSFieldVal[5] == '#')
                    this.txt53.BackColor = Color.LightBlue;
                else
                    this.txt53.BackColor = Color.White;
                this.txt52.Text = AuxOnUSFieldVal[6].ToString();
                if (AuxOnUSFieldVal[6] == '#')
                    this.txt52.BackColor = Color.LightBlue;
                else
                    this.txt52.BackColor = Color.White;
                this.txt51.Text = AuxOnUSFieldVal[7].ToString();
                if (AuxOnUSFieldVal[7] == '#')
                    this.txt51.BackColor = Color.LightBlue;
                else
                    this.txt51.BackColor = Color.White;
                this.txt50.Text = AuxOnUSFieldVal[8].ToString();
                if (AuxOnUSFieldVal[8] == '#')
                    this.txt50.BackColor = Color.LightBlue;
                else
                    this.txt50.BackColor = Color.White;
                this.txt49.Text = AuxOnUSFieldVal[9].ToString();
                if (AuxOnUSFieldVal[9] == '#')
                    this.txt49.BackColor = Color.LightBlue;
                else
                    this.txt49.BackColor = Color.White;
                this.txt48.Text = AuxOnUSFieldVal[10].ToString();
                if (AuxOnUSFieldVal[10] == '#')
                    this.txt48.BackColor = Color.LightBlue;
                else
                    this.txt48.BackColor = Color.White;
                this.txt47.Text = AuxOnUSFieldVal[11].ToString();
                if (AuxOnUSFieldVal[11] == '#')
                    this.txt47.BackColor = Color.LightBlue;
                else
                    this.txt47.BackColor = Color.White;
                this.txt46.Text = AuxOnUSFieldVal[12].ToString();
                if (AuxOnUSFieldVal[12] == '#')
                    this.txt46.BackColor = Color.LightBlue;
                else
                    this.txt46.BackColor = Color.White;
                this.txt45.Text = AuxOnUSFieldVal[13].ToString();
                if (AuxOnUSFieldVal[13] == '#')
                    this.txt45.BackColor = Color.LightBlue;
                else
                    this.txt45.BackColor = Color.White;
                this.txt44.Text = AuxOnUSFieldVal[14].ToString();
                if (AuxOnUSFieldVal[14] == '#')
                    this.txt44.BackColor = Color.LightBlue;
                else
                    this.txt44.BackColor = Color.White;
                this.ConstraintStartAtValues();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(SetAuxOnUSFieldValue), nameof(MICRIDControl));
            }
        }

        private void ConstraintStartAtValues()
        {
            try
            {
                string amountField = this.AmountField;
                int num1 = 0;
                int num2 = amountField.IndexOf('#');
                if (num2 == -1)
                {
                    this.txtStartAmount.Enabled = false;
                    this.txtStartAmount.MaxLength = 0;
                    this.txtStartAmount.Text = "";
                }
                else
                {
                    int num3 = amountField.LastIndexOf('#');
                    int num4 = num3 - num2 + 1;
                    for (int index = 12 - num2 - 1; index > 12 - num3; --index)
                    {
                        TextBox control = (TextBox)this.Controls["txt" + index.ToString()];
                        control.Text = "#";
                        control.BackColor = Color.LightBlue;
                    }
                    this.txtStartAmount.Enabled = true;
                    this.txtStartAmount.MaxLength = num4;
                    string text = this.txtStartAmount.Text;
                    if (text.Length > num4)
                    {
                        int num5 = text.Length - num4;
                        this.txtStartAmount.Text = text.Remove(text.Length - num5);
                    }
                }
                string onUsField = this.OnUSField;
                num1 = 0;
                int num6 = 0;
                int num7 = onUsField.IndexOf('#');
                if (num7 == -1)
                {
                    this.txtStartOnUS.Enabled = false;
                    this.txtStartOnUS.MaxLength = 0;
                    this.txtStartOnUS.Text = "";
                }
                else
                {
                    int num8 = onUsField.LastIndexOf('#');
                    int num9 = num8 - num7 + 1;
                    for (int index = 32 - num7 - 1; index > 32 - num8; --index)
                    {
                        TextBox control = (TextBox)this.Controls["txt" + index.ToString()];
                        control.Text = "#";
                        control.BackColor = Color.LightBlue;
                    }
                    this.txtStartOnUS.Enabled = true;
                    this.txtStartOnUS.MaxLength = num9;
                    string text = this.txtStartOnUS.Text;
                    if (text.Length > num9)
                    {
                        int num10 = text.Length - num9;
                        this.txtStartOnUS.Text = text.Remove(text.Length - num10);
                    }
                }
                string transitField = this.TransitField;
                num1 = 0;
                num6 = 0;
                int num11 = transitField.IndexOf('#');
                if (num11 == -1)
                {
                    this.txtStartTransit.Enabled = false;
                    this.txtStartTransit.MaxLength = 0;
                    this.txtStartTransit.Text = "";
                }
                else
                {
                    int num12 = transitField.LastIndexOf('#');
                    int num13 = num12 - num11 + 1;
                    for (int index = 43 - num11 - 1; index > 43 - num12; --index)
                    {
                        TextBox control = (TextBox)this.Controls["txt" + index.ToString()];
                        control.Text = "#";
                        control.BackColor = Color.LightBlue;
                    }
                    this.txtStartTransit.Enabled = true;
                    this.txtStartTransit.MaxLength = num13;
                    string text = this.txtStartTransit.Text;
                    if (text.Length > num13)
                    {
                        int num14 = text.Length - num13;
                        this.txtStartTransit.Text = text.Remove(text.Length - num14);
                    }
                }
                string auxOnUsField = this.AuxOnUSField;
                num1 = 0;
                num6 = 0;
                int num15 = auxOnUsField.IndexOf('#');
                if (num15 == -1)
                {
                    this.txtStartAuxOnUS.Enabled = false;
                    this.txtStartAuxOnUS.MaxLength = 0;
                    this.txtStartAuxOnUS.Text = "";
                }
                else
                {
                    int num16 = auxOnUsField.LastIndexOf('#');
                    int num17 = num16 - num15 + 1;
                    for (int index = 58 - num15 - 1; index > 58 - num16; --index)
                    {
                        TextBox control = (TextBox)this.Controls["txt" + index.ToString()];
                        control.Text = "#";
                        control.BackColor = Color.LightBlue;
                    }
                    this.txtStartAuxOnUS.Enabled = true;
                    this.txtStartAuxOnUS.MaxLength = num17;
                    string text = this.txtStartAuxOnUS.Text;
                    if (text.Length <= num17)
                        return;
                    int num18 = text.Length - num17;
                    this.txtStartAuxOnUS.Text = text.Remove(text.Length - num18);
                }
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(ConstraintStartAtValues), nameof(MICRIDControl));
            }
        }

        private void MicrIdKeyPressedEvent(object sender, KeyPressEventArgs e)
        {
            try
            {
                bool flag1 = false;
                bool flag2 = false;
                TextBox textBox = (TextBox)sender;
                if (textBox == null)
                    return;
                textBox.Text = "";
                string name = textBox.Name;
                int num1 = 0;
                if (char.IsDigit(name[name.Length - 1]))
                    num1 = !char.IsDigit(name[name.Length - 2]) ? int.Parse(name.Substring(name.Length - 1)) : int.Parse(name.Substring(name.Length - 2));
                int num2 = num1 != 0 ? num1 - 1 : 0;
                int num3 = num1 != 58 ? num1 + 1 : 58;
                string key = "txt" + num2.ToString();
                TextBox control1 = (TextBox)this.Controls["txt" + num3.ToString()];
                TextBox control2 = (TextBox)this.Controls[key];
                Convert.ToInt32(e.KeyChar);
                bool flag3;
                if (char.IsDigit(e.KeyChar))
                    flag3 = true;
                else if (e.KeyChar == 'A' || e.KeyChar == 'a')
                    flag3 = true;
                else if (e.KeyChar == '&')
                    flag3 = true;
                else if (e.KeyChar == 'B' || e.KeyChar == 'b')
                    flag3 = true;
                else if (e.KeyChar == '$')
                    flag3 = true;
                else if (e.KeyChar == 'C' || e.KeyChar == 'c')
                    flag3 = true;
                else if (e.KeyChar == '/')
                    flag3 = true;
                else if (e.KeyChar == 'D' || e.KeyChar == 'd')
                    flag3 = true;
                else if (e.KeyChar == 'E' || e.KeyChar == 'e')
                    flag3 = true;
                else if (e.KeyChar == '-')
                    flag3 = true;
                else if (e.KeyChar == '\b')
                {
                    flag3 = true;
                    flag1 = true;
                }
                else if (e.KeyChar == '#')
                {
                    flag3 = true;
                    flag2 = true;
                }
                else
                    flag3 = false;
                if (flag3)
                {
                    if (flag1)
                    {
                        if (control1 != null)
                        {
                            control1.Focus();
                            control1.Select(control1.Text.Length, 0);
                        }
                    }
                    else if (control2 != null)
                    {
                        control2.Focus();
                        control2.Select(control2.Text.Length, 0);
                    }
                    if (flag2)
                        textBox.BackColor = Color.LightBlue;
                    else
                        textBox.BackColor = Color.White;
                    this.OnMICRIdChanged();
                    e.Handled = false;
                }
                else
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(MicrIdKeyPressedEvent), nameof(MICRIDControl));
            }
        }

        private void MicrIdKeyDownEvent(object sender, KeyEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            if (textBox == null)
                return;
            string name = textBox.Name;
            int num1 = 0;
            int num2 = 0;

            if (char.IsDigit(name[name.Length - 1]))
                num1 = !char.IsDigit(name[name.Length - 2]) ? int.Parse(name.Substring(name.Length - 1)) : int.Parse(name.Substring(name.Length - 2));
            if (e.KeyCode == Keys.Delete)
                textBox.Text = "";
            else if (e.KeyCode == Keys.Left)
                num2 = num1 != 58 ? num1 + 1 : 58;
            else if (e.KeyCode == Keys.Right)
                num2 = num1 != 1 ? num1 - 1 : 1;
            else if (e.KeyCode == Keys.Space)
            {
                num2 = num1 != 1 ? num1 - 1 : 1;
                textBox.BackColor = Color.White;
            }
            TextBox control = (TextBox)this.Controls["txt" + num2.ToString()];
            if (control == null)
                return;
            control.Focus();
            control.Select(control.Text.Length, 0);
        }

        private void AutoIncrStartAtChangedEvent(object sender, EventArgs e) => this.OnMICRIdChanged();

        private void AutoIncrStartValueKeyDownEvent(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Delete)
                    return;
                TextBox textBox = (TextBox)sender;
                if (textBox == null)
                    return;
                int start = textBox.Text.Length - 1;
                if (start == -1)
                    return;
                if (textBox.SelectedText.Length != 0)
                {
                    textBox.SelectedText = string.Empty;
                }
                else
                {
                    textBox.Select(start, 1);
                    textBox.SelectedText = string.Empty;
                }
                this.OnMICRIdChanged();
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(AutoIncrStartValueKeyDownEvent), nameof(MICRIDControl));
            }
        }

        private void AutoIncrStartValueKeyPressedEvent(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (char.IsDigit(e.KeyChar) || e.KeyChar == '\b')
                {
                    e.Handled = false;
                    this.OnMICRIdChanged();
                }
                else
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex, nameof(AutoIncrStartValueKeyPressedEvent), nameof(MICRIDControl));
            }
        }

        private void MicrIdKeyUpEvent(object sender, KeyEventArgs e) => this.ConstraintStartAtValues();

        private void CMC7RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 1; i < 59; i++)
            {
                string controlName = "txt" + i;
                TextBox control = (TextBox)this.Controls["txt" + i.ToString()];
                if (E13BRadioButton.Checked == true)
                {
                    control.Font = new System.Drawing.Font("TROY E-13B ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2))); ;
                }
                else
                {
                    control.Font =  new System.Drawing.Font("TROY CMC-7", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
                }
            }            
        }

        public object GetInstance(string strFullyQualifiedName)
        {
            Type t = Type.GetType(strFullyQualifiedName);
            return Activator.CreateInstance(t);
        }
    }
}
