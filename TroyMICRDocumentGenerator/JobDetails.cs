﻿namespace TroyMICRDocumentGenerator
{
    public class JobDetails
    {
        public string JobName;
        public string BatchId;
        public int NoOfChecks;
        public int TotalChecks;
        public string PrinterName;
        public string PrinterModel;
        public string PrinterSerialNo;
        public string PaperSize;
        public string PaperSource;
        public string PaperOutput;
        public string TonerType;
        public string Density;
        public int PrintOption;
        public float YOffset1;
        public float YOffset2;
        public float YOffset3;
        public float YOffset4;
        public int FontOption;
        public string FontId;
        public string FontName;
        public string FontFile;
        public string AuxOnUSField;
        public string TransitField;
        public string OnUSField;
        public string AmountField;
        public int StartAtAuxOnUSField;
        public int StartAtTransitField;
        public int StartAtOnUSField;
        public int StartAtAmountField;
        public double LeftMargin;
        public double TopMargin;
    }
}
